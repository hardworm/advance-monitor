<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Первичный визит
$lang['h1']                    = 'Бронирование';
$lang['h2']                    = 'Ваша бронь';
$lang['booking_add']           = 'Забронировать';
$lang['fistVisit']             = 'Первичный визит - отчет родителей';
$lang['returnVisit']           = 'Повторный визит - отчет родителей';
$lang['exportExcell']          = 'Выгрузить в Excell';
$lang['date']                  = 'Дата';
$lang['group']                 = 'Группа';
$lang['number_of_seats']       = 'Кол. мест';
$lang['start_time']            = 'Время начала';
$lang['end_time']              = 'Время окончания';
$lang['reservation_confirmed'] = 'Бронирование подтверждено';
$lang['yes']                   = 'Да';
$lang['no']                    = 'Нет';
$lang['h4']                    = 'К вашему вниманию';
$lang['warring_TST']           = 'Стандартный курс TST состоит из 5 занятий в течении недели. ( вы можете забронировать места в нескольких группах в течении одного дня)';
$lang['warring_HDOT']          = 'Стандартный курс HDOT состоит из 5 сессий (1 сессия в день)';
$lang['warring_HDOT&TST']      = 'TST и HDOT бронируются последовательно по 1 сессии';
$lang['warring_hyperactive']   = 'Если у вас младенец или гиперактивный ребенок, пожалуйста бронируйте малую камеру «Родитель и ребенок» HDOT ( без шлема)';