<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// 1 шаг дигноза
$lang['cerebral_palsy']              = 'ДЦП';
$lang['post_traumatic_brain_injury'] = 'Посттравматическое поражение мозга';
$lang['spinal_injury']               = 'Спинальная травма';
$lang['retardation']                 = 'Задержка развития';
$lang['epilepsy']                    = 'Эпилепсия';
$lang['autism']                      = 'Аутизм';
$lang['asperger_syndrome']           = 'С-м Аспергера';
$lang['autistopodobnoe_disease']     = 'Аутистоподобное заболевание';
$lang['microcephaly']                = 'Микроцефалия';
$lang['hydrocephalus']               = 'Гидроцефалия';
$lang['genetic_disease']             = 'Генетическое заболевание';
$lang['other']                       = 'Другое';


$lang['back'] = 'Назад';
$lang['send'] = 'Отправить';
$lang['step'] = 'Шаг';
$lang['next'] = 'Следущий';
$lang['h2']   = 'Пожалуйста уточните';


//2 шаг
$lang['high']   = 'тяжелая';
$lang['middle'] = 'средняя';
$lang['low']    = 'легкая';


$lang['spastic_type']      = 'Спастический церебральный паралич';
$lang['tetraplegia']       = 'Тетраплегия';
$lang['paraplegia']        = 'Параплегия';
$lang['hemiplegia']        = 'Гемиплегия';
$lang['spastic_diplegia']  = 'Спастическая диплегия (синдром/болезнь Литтля)';
$lang['double_hemiplegia'] = 'Двойная диплегия';

$lang['dyskineticl_type'] = 'Дискинетический  церебральный паралич';
$lang['hyperkinetic']     = 'Гиперкинетический';
$lang['dystonic']         = 'Дистонический';

$lang['atonic_type']                  = 'Атонический церебральный паралич';
$lang['atonic-astatic_form']          = 'Атонически-астатическая форма';
$lang['congenital_cerebellar_ataxia'] = 'Врожденная мозжечковая атаксия';


$lang['multiform'] = 'Смешанная форма';

$lang['unspecified'] = 'ДЦП Неуточненный';

$lang['tetraparesis'] = 'Тетрапарез';

$lang['hemiparesis'] = 'Гемипарез';

$lang['diplegia'] = 'Диплегия';

$lang['tetraparesis'] = 'Тетрапарез';

$lang['hemiparesis'] = 'Гемипарез';
$lang['diplegia']    = 'Диплегия';

$lang['motor']     = 'Моторного';
$lang['cognitive'] = 'Интеллектуального';
$lang['speech']    = 'Речевого';


$lang['day']   = 'Дневная';
$lang['night'] = 'Ночная';
$lang['mixed'] = 'Смешанная';

$lang['focal'] = 'Джексоновская';

$lang['west_syndrome']  = 'С-м Уэста';
$lang['lennox-gastaut'] = 'Леннокса-Гасто';

$lang['decompensated']     = 'Декомпенсированная';
$lang['subcompensated']    = 'Субкомпенсированная';
$lang['compensated']       = 'Компенсированная';
$lang['amgelman_syndrome'] = 'С-м Ангелмана';

$lang['downs_syndrome'] = 'С-м Дауна';

$lang['diagnosis_add']          = 'Заполнить диагноз';
$lang['formation_of_diagnosis'] = 'Формирование диагноза';
$lang['send_and']                   = "Данные отправлены";
