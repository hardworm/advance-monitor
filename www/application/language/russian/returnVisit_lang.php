<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Повторный визит
$lang['h1']                           = 'Повторный визит';
$lang['h2']                           = 'Повторный визит - отчет родителей';
$lang['contact_with_advance']         = 'Были ли вы в контакте с Advance относительно восстановительной программы  с момента вашего последнего визита?';
$lang['hours_recommended']            = 'Сколько лечебных часов было рекомендовано?';
$lang['only_numeric_values']          = 'только цифровые значения';
$lang['hours_paid_daily']             = 'Сколько лечебных часов вы уделяли в среднем ежедневно?';
$lang['reason_reduce_operating_time'] = 'По какой причине вы вынуждены были (если она была) сокращать время работы с ребенком. Пожалуйста отметьте';
$lang['lack_of_time']                 = 'недостаток времени';
$lang['difficulties_build_schedule']  = 'затруднения выстроить график';
$lang['resistance_of_child']          = 'сопротивление ребенка';
$lang['disease']                      = 'заболевания';
$lang['holidays']                     = 'праздники';
$lang['technical_difficulties']       = 'технические трудности выполнения упражнений';
$lang['other']                        = 'другое';
$lang['ims']                          = 'Программа IMS(индивидуальной метаболической поддержки)';
$lang['yes']                          = 'да';
$lang['no']                           = 'нет';
$lang['main_target']                  = 'Что было вашей главной целью этого периода?';
$lang['step']                         = 'Шаг';
$lang['step2']                        = 'Шаг 2';
$lang['step3']                        = 'Шаг 3';
$lang['step4']                        = 'Шаг 4';
$lang['step5']                        = 'Шаг 5';
$lang['step6']                        = 'Шаг 6';
$lang['step7']                        = 'Шаг 7';
$lang['example_menu']                 = 'Пожалуйста приведите пример ежедневного меню вашего ребенка, и укажите время';
$lang['day1_breakfast']               = 'День 1, Завтрак';
$lang['day1_lunch']                   = 'День 1, Обед';
$lang['day1_afternoon_snack']         = 'День 1, Полдник';
$lang['day1_dinner']         = 'День 1, Ужин';
$lang['day1_between_meals']           = 'День 1, промежуточные приемы пищи';
$lang['day2_breakfast']               = 'День 2, Завтрак';
$lang['day2_lunch']                   = 'День 2, Обед';
$lang['day2_afternoon_snack']         = 'День 2, Полдник';
$lang['day2_dinner']                  = 'День 2, Ужин';
$lang['day2_between_meals']           = 'День 2, промежуточные приемы пищи';
$lang['day3_breakfast']               = 'День 3, Завтрак';
$lang['day3_lunch']                   = 'День 3, Обед';
$lang['day3_afternoon_snack']         = 'День 3, Полдник';
$lang['day3_dinner']                  = 'День 3, Ужин';
$lang['day3_between_meals']           = 'День 3, промежуточные приемы пищи';
$lang['end_between_meals']            = 'последнее время приема пищи или жидкости вечером:';
$lang['structural_disorders']         = 'Структурные нарушения';
$lang['thorax']                       = 'Грудная клетка';
$lang['any_changes']                  = 'Пожалуйста, отметьте  если вы отмечаете любые изменения';
$lang['significant_changes']          = 'Пожалуйста, отметьте если вы отмечаете значительные изменения';


$lang['severe']   = 'Очень тяжелые';
$lang['poor']     = 'Тяжелые';
$lang['moderate'] = 'Выраженные';
$lang['mild']     = 'Легкие';
$lang['normal']   = 'Норма';

$lang['not_at_all']  = 'Нет';
$lang['poorly']      = 'Очень плохо';
$lang['moderately']  = 'Плохо';
$lang['fairly_well'] = 'Удовлетворительно';
$lang['well']        = 'Хорошо';

$lang['very_high']                    = 'очень тяжелые';
$lang['high']                         = 'тяжелые';
$lang['expressed']                    = 'выраженные';
$lang['norm']                         = 'норма';
$lang['low']                          = 'легкие';


$lang['scale']                        = 'Пожалуйста, отметьте, если  имеются незначительные изменения в рамках предыдущего статуса используя шкалу от 1 до 9';
$lang['cervical_spine']               = 'Шейный отдел позвоночника (Верхний)';
$lang['thoracic_spine']               = 'Грудной отдел позвоночника (Средний)';
$lang['lumbar_spine']                 = 'Поясничный отдел позвоночника (Нижний)';
$lang['belly']                        = 'Живот';
$lang['pelvis']                       = 'Таз';
$lang['hip']                          = 'Бедра';
$lang['legs']                         = 'Ноги';
$lang['foot']                         = 'Стопы';
$lang['shoulder-blade']               = 'Лопатки';
$lang['upper_arm']                    = 'Плечи';
$lang['hands']                        = 'Руки';
$lang['head']                         = 'Голова';
$lang['funtsionalnost_sphere']        = 'Фунциональная сфера';
$lang['holding_head']                 = 'Удерживание головы';
$lang['very_bad']                     = 'очень плохо';
$lang['bad']                          = 'плохо';
$lang['satisfactory']                 = 'удовлетворительно';
$lang['good']                         = 'хорошо';
$lang['coordinated_limb_movements']   = 'Координированные движения конечностей';
$lang['hand_movements']               = 'Движения рук';
$lang['leg_movements']                = 'Движения ног';
$lang['trunk_movements']              = 'Движения туловища';
$lang['turn']                         = 'Переворачиваться';
$lang['sit_up_unaided']               = 'Самостоятельно сидеть';
$lang['keep_balance']                 = 'Удерживать равновесие';
$lang['creep']                        = 'Ползать';
$lang['stand']                        = 'Стоять';
$lang['walk']                         = 'Ходить';
$lang['run']                          = 'Бегать';
$lang['vocalize']                     = 'Издавать звуки';
$lang['speak']                        = 'Говорить';
$lang['gape']                         = 'Зевать';
$lang['laugh']                        = 'Смеяться';
$lang['sing']                         = 'Петь';
$lang['articulation_and_sound']       = 'Артикуляция и звукопроизношение';
$lang['sight']                        = 'Зрение';
$lang['hearing']                      = 'Слух';
$lang['chew']                         = 'Жевать';
$lang['swallow_solid_food']           = 'Глотать твердую пищу';
$lang['swallow_liquid_food']          = 'Глотать жидкую пищу';
$lang['independently_drink']          = 'Самостоятельно пить';
$lang['independently_there']          = 'Самостоятельно есть';
$lang['stool']                        = 'Стул';
$lang['bladder_control']              = 'Контроль мочеиспускания';
$lang['change_sphere']                = 'Изменение когнитивной сферы';
$lang['react_to_their_environment']   = 'Реагировать на окружающую обстановку';
$lang['stare']                        = 'Фиксировать взгляд на лицах, предметах';
$lang['recognize_people']             = 'Узнавать людей';
$lang['communicate_nonverbally']      = 'Общаться  невербально (без помощи слов)';
$lang['communicate_verbally']         = 'Общаться вербально (с помощью отдельных слов)';
$lang['say_phrases']                  = 'Произносить фразы';
$lang['understand_speech']            = 'Понимать речь';
$lang['perform_simple_requests']      = 'Выполнять простые просьбы';
$lang['allocate_favorite_toy']        = 'Выделять любимую игрушку';
$lang['play_with_other']              = 'Играть с другими';
$lang['independently_play']           = 'Самостоятельно играть';
$lang['follow_social_norms']          = 'Следовать социальным нормам';
$lang['concentrate_on_tasks']         = 'Концентрироваться на поставленном задании';
$lang['listen_to_storiess']           = 'Слушать истории и сказки';
$lang['read']                         = 'Читать';
$lang['write']                        = 'Писать';
$lang['respond_emotionally']          = 'Адекватно эмоционально реагировать';
$lang['perform_complex_tasks']        = 'Выполнять сложные задачи';
$lang['tutor']                        = 'Заниматься самостоятельно';
$lang['work_in_group']                = 'Работать в группе';
$lang['eye_contact']                  = 'Глазной контакт';
$lang['pointing_gesture']             = 'Указательный жест';
$lang['to_be_quiet_for_request']      = 'Вести себя тихо, когда просят';
$lang['concentrate_on_conversation']  = 'Концентрироваться на предмете разговора';
$lang['change_sphere2']               = 'Изменение физиологической сферы';
$lang['appetite']                     = 'Аппетит';
$lang['digestion']                    = 'Переваривание пищи';
$lang['bowel_movement']               = 'Опорожнение кишечника';
$lang['breathing']                    = 'Дыхание';
$lang['dream']                        = 'Сон';
$lang['symptoms']                     = 'Заболевание и симптомы';
$lang['convulsions']                  = 'Судороги';
$lang['bronchitis']                   = 'Бронхит';
$lang['sars']                         = 'ОРВИ';
$lang['allergy']                      = 'Аллергия';
$lang['bronchial_asthma']             = 'Бронхиальная астма';
$lang['skin_diseases']                = 'Кожные заболевания';
$lang['intestinal_colic']             = 'Кишечные колики';
$lang['other']                        = 'Другое (вписанное в певичной анкете)';
$lang['new_other']                    = 'Другие новые заболевания в этот период';
$lang['send']                        = 'Отправить';
$lang['sending_completed']           = 'Благодарим вас за заполнение анкеты';
$lang['ATTENTION']                   = '<strong>Внимание:</strong><br><u>Фотосъемка во время консультации.</u><br>Для нас очень важен осмотр туловища, а также внешний вид детей. Пожалуйста, возьмите  с собой красивые шорты или купальные трусики для мальчиков и девочек. Для девочек старше 11 лет также необходима и верхняя часть купальника';
$lang['complete_all_lines']          = 'Все поля обязательны для заполнения';
