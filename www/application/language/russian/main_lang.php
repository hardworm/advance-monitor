<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// меню
$lang['diagnosis']     = 'Диагноз';
$lang['information']   = 'Информация';
$lang['methods']       = 'Методы';
$lang['alarm_form']    = 'Аларм Форма';
$lang['book_a_course'] = 'Бронирование';
$lang['users']         = 'Пользователи';
$lang['data_base']     = 'Управление базой';
$lang['log_out']       = 'выход';
$lang['add']       = 'Добавить';
$lang['read']       = 'Читать';