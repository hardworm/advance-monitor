<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// меню
$lang['diagnosis']     = 'DIAGNOSIS';
$lang['information']   = 'INFORMATION';
$lang['methods']       = 'METHODS';
$lang['alarm_form']    = 'ALARM FORM';
$lang['book_a_course'] = 'BOOK A COURSE';
$lang['users']         = 'USERS';
$lang['data_base']     = 'DATA BASE';
$lang['log_out']       = 'Log out';
$lang['add']       = 'Add';
$lang['read']       = 'Read';
