<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Первичный визит
$lang['h1']                    = 'BOOK A COURSE';
$lang['h1']                    = 'Your booking';
$lang['booking_add']           = 'Make a reservation';
$lang['fistVisit']             = 'First visit - parenta report';
$lang['returnVisit']           = 'Return visit - parenta report';
$lang['exportExcell']          = 'Export to Excell';
$lang['date']                  = 'Date';
$lang['group']                 = 'Group';
$lang['number_of_seats']       = 'Number of seats';
$lang['start_time']            = 'Start time';
$lang['end_time']              = 'End time';
$lang['reservation_confirmed'] = 'Reservation confirmed';
$lang['yes']                   = 'Yes';
$lang['no']                    = 'No';
$lang['h4']                    = 'For your attention';
$lang['warring_TST']           = 'A standard course of TST consists of 5 sessions during the week. (You can book a place in more than 1 group on the same day)';
$lang['warring_HDOT']          = 'A standard course of HDOT consists  5 sessions (1 session per day)';
$lang['warring_HDOT&TST']      = 'TST and HDOT should be book step by step basis, according the spaces available';
$lang['warring_hyperactive']   = 'If you have an infant or a hyperactive child please  book the small “Parent and Child” chamber (no hood necessary)';
