<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Первичный визит
$lang['h1']                          = 'First visit ';
$lang['h2']                          = 'Parents’ Information prior to the First Evaluation';
$lang['name_of_mother']              = 'Mother’s name';
$lang['name_of_father']              = 'Father’s name';
$lang['address']                     = 'Address';
$lang['home_phone']                  = 'Telephone no. home';
$lang['mobile_phone_mother']         = 'Mother’s Mobile telelephone  no. ';
$lang['work_phone_mother']           = 'Mother’s Telephone no. work';
$lang['email_mother']                = 'Mother’s E-mail address ';
$lang['mobile_phone_father']         = 'Father’s Mobile telelephone  no.';
$lang['work_phone_father']           = 'Father’s Telephone no. work';
$lang['email_father']                = 'Father’s E-mail address';
$lang['position_father']             = 'Occupation father';
$lang['position_mother']             = 'Occupation mother';
$lang['spoken_english']              = 'Do you own spoken English?';
$lang['was_baby_preterm']            = 'Was your child premature?';
$lang['respiratory_care']            = 'Was your child ever on a ventilator?';
$lang['harm_during_pregnancy']       = 'Did the mother had any illness or stress during pregnancy?';
$lang['school_or_kindergarten']      = 'Does your child attend school?';
$lang['hours_sitting']               = 'How long does your child spend daily:';
$lang['hours']                       = 'How long does your child spend daily:';
$lang['sitting']                     = 'sitting';
$lang['lying_back']                  = 'lying on the back';
$lang['lying_stomach']               = 'lying on the stomach';
$lang['hours_other']                 = 'other (please state)';
$lang['first_improvements']          = 'What is the first change you would like to see in your child?';
$lang['yes']                         = 'yes';
$lang['no']                          = 'no';
$lang['step']                        = 'Step';
$lang['step2']                       = 'Step 2';
$lang['step3']                       = 'Step 3';
$lang['step4']                       = 'Step 4';
$lang['step5']                       = 'Step 5';
$lang['step6']                       = 'Step 6';
$lang['step7']                       = 'Step 7';
$lang['step8']                       = 'Step 8';
$lang['step9']                       = 'Step 9';
$lang['what_therapies']              = 'What therapies have you used so far?';
$lang['medication']                  = 'Medicines';
$lang['muscle_relaxants']            = 'Muscle relaxants';
$lang['anticonvulsants']             = 'Anticonvulsants';
$lang['antipsychotics']              = 'Neuroleptics';
$lang['antidepressants']             = 'Antidepressants';
$lang['tranquilizers']               = 'Tranquilizers';
$lang['enzymes']                     = 'Enzymes';
$lang['antihistamines']              = 'Antihistamine';
$lang['laxatives']                   = 'Laxatives';
$lang['hormones']                    = 'Hormones';
$lang['physiotherapy']               = 'Physiotherapy';
$lang['physiotherapy_details']       = 'If “YES”, please state which:';
$lang['reflexology']                 = 'Reflexology';
$lang['massage']                     = 'Massage';
$lang['gemeopatiya']                 = 'Homeopathy';
$lang['baa']                         = 'Food Supplements';
$lang['baa_details']                 = 'If “YES”, please state which:';
$lang['other_treatments']            = 'Other therapies :';
$lang['orthopedic_products']         = 'Does your child use any special equipment or supports?';
$lang['orthopedic_products_details'] = 'If “YES”, please state which:';
$lang['corrective_surgery']          = 'Has your child had any corrective surgery?';
$lang['corrective_surgery_details']  = 'If “YES”, please state which:';
$lang['hypersensitivity_to_sound']   = 'Is your child easily startled by sounds?';
$lang['pregnancy']                   = 'Did the mother had any illness or stress during pregnancy';
$lang['infection']                   = 'Infection';


$lang['severe']   = 'Severe';
$lang['poor']     = 'Poor';
$lang['moderate'] = 'Moderate';
$lang['mild']     = 'Mild';
$lang['normal']   = 'Normal';

$lang['not_at_all']  = 'Not at all';
$lang['poorly']      = 'Poorly';
$lang['moderately']  = 'Moderately';
$lang['fairly_well'] = 'Fairly Well';
$lang['well']        = 'Well';



$lang['slight']                      = 'Slight';
$lang['first_of_pregnancy']          = 'first trimester of pregnancy';
$lang['second_of_pregnancy']         = 'second trimester of pregnancy';
$lang['both_trimesters']             = 'Both trimesters of preganancy';
$lang['stress']                      = 'Stress';
$lang['threat_miscarriage']          = 'Risk of miscarriage';
$lang['toxemia']                     = 'Toxicosis';
$lang['menu']                        = 'Please give an example of your child’s daily menu and what time he or she eats?';
$lang['day1_breakfast']              = 'Day 1, Breakfast';
$lang['day1_lunch']                  = 'Day 1, Lunch';
$lang['day1_afternoon_snack']        = 'Day 1, Afternoon snack';
$lang['day1_dinner']                 = 'Day 1, Supper';
$lang['day1_between_meals']          = 'Day 1, Snacks';
$lang['day2_breakfast']              = 'Day 2, Breakfast';
$lang['day2_lunch']                  = 'Day 2, Lunch';
$lang['day2_afternoon_snack']        = 'Day 2, Afternoon snack';
$lang['day2_dinner']                 = 'Day 2, Supper';
$lang['day2_between_meals']          = 'Day 2, Snacks';
$lang['day3_breakfast']              = 'Day 3, Breakfast';
$lang['day3_lunch']                  = 'Day 3, Lunch';
$lang['day3_afternoon_snack']        = 'Day 3, Afternoon snack';
$lang['day3_dinner']                 = 'Day 3, Supper';
$lang['day3_between_meals']          = 'Day 3, Snacks';
$lang['end_between_meals']           = 'What time in the evening is the last food or drink usually taken?';
$lang['structural_damage']           = 'Structural abnormality';
$lang['chest']                       = 'Chest';
$lang['very_high']                   = 'Very high';
$lang['high']                        = 'Severe';
$lang['expressed']                   = 'Poor';
$lang['norm']                        = 'Normal';
$lang['low']                         = 'Mild';
$lang['cervical_spine']              = 'Cervical Spine  (Upper)';
$lang['thoracic_spine']              = 'Thoracic Spine  (Middle)';
$lang['lumbar_spine']                = 'Lumbar Spine  (Lower)';
$lang['belly']                       = 'Abdomen';
$lang['pelvis']                      = 'Pelvis';
$lang['hip']                         = 'Hips';
$lang['legs']                        = 'Legs';
$lang['foot']                        = 'Foot';
$lang['shoulder-blade']              = 'Shoulder Blades';
$lang['upper_arm']                   = 'Shoulders';
$lang['hands']                       = 'Arms';
$lang['head']                        = 'Head';
$lang['funtsionalnost_sphere']       = 'Functional ability';
$lang['holding_head']                = 'Hold up head';
$lang['very_bad']                    = 'Poorly';
$lang['bad']                         = 'Moderately';
$lang['satisfactory']                = 'Fairly Well';
$lang['good']                        = 'Well';
$lang['coordinated_limb_movements']  = 'Move limbs';
$lang['hand_movements']              = 'Range of upper limbs movenent';
$lang['leg_movements']               = 'Range of lower limbs movenent';
$lang['trunk_movements']             = 'Range of trunk movement';
$lang['turn']                        = 'Roll';
$lang['sit_up_unaided']              = 'Sit unsupported';
$lang['keep_balance']                = 'Balance';
$lang['creep']                       = 'Crawl';
$lang['stand']                       = 'Stand';
$lang['walk']                        = 'Walk';
$lang['run']                         = 'Run';
$lang['vocalize']                    = 'Vocalize';
$lang['speak']                       = 'Speak';
$lang['gape']                        = 'Yawn';
$lang['laugh']                       = 'Laugh';
$lang['sing']                        = 'Sing';
$lang['articulation_and_sound']      = 'Articulate and  Vocalise';
$lang['sight']                       = 'See';
$lang['hearing']                     = 'Hear';
$lang['chew']                        = 'Chew';
$lang['swallow_solid_food']          = 'Can swallow solid food';
$lang['swallow_liquid_food']         = 'Can swallow a liqids';
$lang['independently_drink']         = 'Drink independently';
$lang['independently_there']         = 'Eat independently';
$lang['stool']                       = 'Bowel movements';
$lang['bladder_control']             = 'Bladder Control';
$lang['change_sphere']               = 'Cognitive';
$lang['react_to_their_environment']  = 'Respond to surroundings';
$lang['stare']                       = 'Focusing on people, objects';
$lang['recognize_people']            = 'Recognize people';
$lang['communicate_nonverbally']     = 'Non verbal communicate ( without words)';
$lang['communicate_verbally']        = 'Verbal communicate ( using same words)';
$lang['say_phrases']                 = 'Use a language';
$lang['understand_speech']           = 'Understand speech';
$lang['perform_simple_requests']     = 'Carry out simple request';
$lang['allocate_favorite_toy']       = 'Choose his\her favorite toy';
$lang['play_with_other']             = 'Play with others';
$lang['independently_play']          = 'Playing by him\her self';
$lang['follow_social_norms']         = 'Behave socially acceptable';
$lang['concentrate_on_tasks']        = 'Carry out simple request';
$lang['listen_to_storiess']          = 'Listen to stories';
$lang['read']                        = 'Read';
$lang['write']                       = 'Write';
$lang['respond_emotionally']         = 'Show normal emotions';
$lang['perform_complex_tasks']       = 'Carry out complex requests';
$lang['tutor']                       = 'Work independently';
$lang['work_in_group']               = 'Work in a group';
$lang['eye_contact']                 = 'Give eye contact';
$lang['pointing_gesture']            = 'Point to indicate need';
$lang['to_be_quiet_for_request']     = 'Be quite when requested';
$lang['concentrate_on_conversation'] = 'Concentrate on conversational subject';
$lang['change_sphere2']              = 'Physiological changes';
$lang['appetite']                    = 'Appetite';
$lang['digestion']                   = 'Digestion';
$lang['bowel_movement']              = 'Bowels movement';
$lang['breathing']                   = 'Respiration';
$lang['dream']                       = 'Sleep';
$lang['symptoms']                    = 'Diseases and symptoms';
$lang['convulsions']                 = 'Seizures';
$lang['bronchitis']                  = 'Chest infections';
$lang['sars']                        = 'Colds';
$lang['allergy']                     = 'Allergies';
$lang['bronchial_asthma']            = 'Asthma';
$lang['skin_diseases']               = 'Eczema';
$lang['intestinal_colic']            = 'Abdominal pain';
$lang['other']                       = 'Other';
$lang['new_other']                   = 'Other new pathologies';
$lang['send']                        = 'Send';
$lang['find_us']                     = 'How did you find us?';
$lang['word_of_mouth']               = 'Word of mouth';
$lang['tv_radio']                    = 'TV/radio';
$lang['newspaper']                   = 'Newspaper';
$lang['magazine']                    = 'Magazine';
$lang['from_family_of_child']        = 'From family of child';
$lang['internet']                    = 'Internet';
$lang['medical']                     = 'Medical';
$lang['add_info']                    = 'Before coming to Advance,<br>what was the most helpful thing you were told about<br>your child’s condition by a member<br> of your child’s therapy team:';
$lang['sending_completed']           = 'Thank you very much for filling in this form';
$lang['ATTENTION']                   = '<strong>ATTENTION</strong><br><u>Assessment photographs</u><br>We do need to look at the trunk structure but it is also lovely to see the children looking smart. Please could you bring smart shorts or bathing costume for boys and girls (bottom part of bikini for girls). If the girls are over 11 please bring crop top and shorts or bikini for them.';
$lang['complete_all_lines']          = 'Please complete all lines';