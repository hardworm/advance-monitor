<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// 1 шаг дигноза
$lang['cerebral_palsy']              = 'Cerebral palsy';
$lang['post_traumatic_brain_injury'] = 'Brain injury';
$lang['spinal_injury']               = 'Spinal Injury';
$lang['retardation']                 = 'Developmental Delay';
$lang['epilepsy']                    = 'Epilepsy';
$lang['autism']                      = 'Autism';
$lang['asperger_syndrome']           = 'Asperger’s Syndrome';
$lang['autistopodobnoe_disease']     = 'Autism related disorder';
$lang['microcephaly']                = 'Microcephaly';
$lang['hydrocephalus']               = 'Hydrocephaly';
$lang['genetic_disease']             = 'Genetic disorder';
$lang['other']                       = 'Other';


$lang['back'] = 'Back';
$lang['send'] = 'Send';
$lang['step'] = 'Step';
$lang['next'] = 'Next';
$lang['h2']   = 'Please specify';

//2 шаг
$lang['high']   = 'severe';
$lang['middle'] = 'moderate';
$lang['low']    = 'slight';


$lang['spastic_type']      = 'Spastic type';
$lang['tetraplegia']       = 'Quadriplegia';
$lang['paraplegia']        = 'Paraplegia';
$lang['hemiplegia']        = 'Hemiplegia';
$lang['spastic_diplegia']  = 'Spastic Diplegia\Little’s Disease';
$lang['double_hemiplegia'] = 'Double Hemiplegia';

$lang['dyskineticl_type'] = 'Dyskineticl type';
$lang['hyperkinetic']     = 'Hyperkinetic\ Athetoid';
$lang['dystonic']         = 'Dystonic';

$lang['atonic_type']                  = 'Atonic type';
$lang['atonic-astatic_form']          = 'Atonic type';
$lang['congenital_cerebellar_ataxia'] = 'Ataxia \Cerebellar Ataxia';

$lang['multiform'] = 'Multiform';

$lang['unspecified'] = 'Unspecified';

$lang['tetraparesis'] = 'Tetraparesis';

$lang['hemiparesis'] = 'Hemiparesis';

$lang['diplegia'] = 'Diplegia';

$lang['tetraparesis'] = 'Tetraparesis';

$lang['hemiparesis'] = 'Hemiparesis';
$lang['diplegia']    = 'Diplegia';

$lang['motor']     = 'Motor';
$lang['cognitive'] = 'Cognitive';
$lang['speech']    = 'Speech';

$lang['day']   = 'Day time';
$lang['night'] = 'Night time';
$lang['mixed'] = 'At any time';

$lang['focal'] = 'Focal';

$lang['west_syndrome']  = 'West Syndrome';
$lang['lennox-gastaut'] = 'Lennox-Gastaut';

$lang['decompensated']     = 'Decompensated \ progressive';
$lang['subcompensated']    = 'Subcompensated';
$lang['compensated']       = 'Compensated\ non progressive';
$lang['amgelman_syndrome'] = 'Amgelman Syndrome';

$lang['downs_syndrome'] = "Down's Syndrome";

$lang['diagnosis_add']          = "Please fill in your child's diagnosis";
$lang['formation_of_diagnosis'] = "Please give us a break down of your child's diagnosis";
$lang['send_and'] = "your data has been sen";