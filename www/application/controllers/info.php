<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Info extends My_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library(array('parser', 'ion_auth', 'form_validation'));
        $this->load->model('info_model', 'InfoModel');
    }

    public function index() {
        $data['title']    = "Advance MONITOR: ".$this->lang->line('information');
        $data['isAdmin'] = $this->ion_auth->is_admin();
        $data['arrInfo']  = $this->InfoModel->getInfo();
        $data['notice']   = $this->InfoModel->getNotice();
        $data['is_admin'] = $this->ion_auth->is_admin();

        $this->parser->parse("info.tpl", $data);
    }

    public function article($id) {
        $data['article'] = $this->InfoModel->get($id);
        $this->parser->parse("article.tpl", $data);
    }
    
    public function add() {
        if (!$this->ion_auth->is_admin()) {
            return show_error('You must be an administrator to view this page.');
        } else {
            //validate form input
            $this->form_validation->set_rules('title', 'title', 'required');
            $this->form_validation->set_rules('short_desc', 'short_desc', 'required');
            $this->form_validation->set_rules('desc', 'desc', 'required');
            $this->form_validation->set_rules('is_notice', 'is_notice', 'required');
            $this->form_validation->set_rules('visible', 'visible', 'required');
            
            if ($this->form_validation->run() == true) {
                $data = array(
                    'type_info_id' => 1,
                    'title'        => $this->input->post('title'),
                    'short_desc'   => $this->input->post('short_desc'),
                    'desc'         => $this->input->post('desc'),
                    'is_notice'    => $this->input->post('is_notice'),
                    'visible'      => $this->input->post('visible'),
                );
                $this->InfoModel->insert($data);
                redirect('info', 'refresh');
            } else {
                $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                echo validation_errors();
                
                $data['title']    = "Advance MONITOR: Adding information";
                $this->parser->parse("info_edit.tpl", $data);
            }
        }
    }
    
    public function delete($id) {
        if (!$this->ion_auth->is_admin()) {
            return show_error('You must be an administrator to view this page.');
        } else {
            $this->InfoModel->delete($id);
            redirect('info', 'refresh');
        }
    }
}