<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Alarm extends My_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library(array('parser', 'ion_auth', 'form_validation','email'));
        $this->load->model('alarm_model', 'AlarmModel');
        $this->load->model('report_model', 'ReportModel');
        $this->load->model('diagnosis_model');
        if($this->input->cookie('lang') == "russian"){
            $this->lang->load('alarm','russian');
        } else {
            $this->lang->load('alarm','english');
        }
    }

    public function index() {
        $user = $this->ion_auth->user()->row();
        $data['isAdmin'] = $this->ion_auth->is_admin();
        $data['title']    = "Advance MONITOR: Alarm Form";
        $data['isReportThisDay'] = $this->ReportModel->isReportThisDay($user->id);
        $this->parser->parse("alarm.tpl", $data);
    }
    
    public function exportExcell() {
        $user = $this->ion_auth->user()->row();
        
        $this->load->library('excel');
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="alarm.xls"');
        header('Cache-Control: max-age=0');
        
        // Assign cell values
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('AlarmForm');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Date');
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
        $this->excel->getActiveSheet()->setCellValue('B1', 'behavior');
        $this->excel->getActiveSheet()->setCellValue('C1', 'neurologic');
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('D1', 'vegetative');
        $this->excel->getActiveSheet()->setCellValue('E1', 'respiratory');
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('F1', 'alimentary');
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('G1', 'urinary');
        $this->excel->getActiveSheet()->setCellValue('H1', 'total score');
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('I1', 'weaser score');
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(11);
        
        $allReport = $this->ReportModel->getAllIndividualReport($user->id);
        $i=2;
        foreach ($allReport as $val) {
            $this->excel->getActiveSheet()->setCellValue("A$i", $val->date);
            $this->excel->getActiveSheet()->setCellValue("B$i", $val->behavior);
            $this->excel->getActiveSheet()->setCellValue("C$i", $val->neurologic);
            $this->excel->getActiveSheet()->setCellValue("D$i", $val->vegetative);
            $this->excel->getActiveSheet()->setCellValue("E$i", $val->respiratory);
            $this->excel->getActiveSheet()->setCellValue("F$i", $val->alimentary);
            $this->excel->getActiveSheet()->setCellValue("G$i", $val->urinary);
            $this->excel->getActiveSheet()->setCellValue("H$i", $val->total_score);
            $this->excel->getActiveSheet()->setCellValue("I$i", $val->weaser_score);
            $i++;
        }

        // Save it as an excel 2003 file
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    public function add() {
        $user = $this->ion_auth->user()->row();
        if(!$this->diagnosis_model->isDiagnosis($user->id)){
            redirect('diagnosis/add/', 'refresh');
        }
        
        $isReportThisDay = $this->ReportModel->isReportThisDay($user->id);
        if (!$isReportThisDay){
            $data['title']    = "Advance MONITOR: Alarm Form";
            $this->parser->parse("alarm_add.tpl", $data);
        } else {
            redirect('alarm', 'refresh');
        }
    }
    
    public function save() {
        $user = $this->ion_auth->user()->row();
        $isReportThisDay = $this->ReportModel->isReportThisDay($user->id);
        
        if(!$isReportThisDay){
            $dataReport = array(
                'users_id'    => $user->id,
                'json_text'   => $this->input->post('textInfo'),
                'json_params' => $this->input->post('setup'),
                'json_weaser' => $this->input->post('pogoda'),
            );
            $report_id = $this->ReportModel->insert($dataReport);
            $dataForm = json_decode($this->input->post('setup'));
            $textForm = json_decode($this->input->post('textInfo'));
            $weaserForm = json_decode($this->input->post('pogoda'));

            if (!empty($dataForm) AND is_int($report_id)) {
//                $this->AlarmModel->recordAlarm($report_id, $dataForm);
                $this->AlarmModel->calculateAndRecord($report_id, $dataForm, $this->config);
                $this->sendReport($textForm);
            }
            if (!empty($weaserForm) AND is_int($report_id)) {
                $this->ReportModel->calculateWeaser($report_id, $weaserForm);
            }
        }
    }
    
    /**
     * Функция отправки данных 
     * @param array $textForm
     */
    private function sendReport($textForm) {
        $user = $this->ion_auth->user()->row();
        
        if(!empty($textForm)){
            //собираем ответы польователя
            $str = "$user->first_name $user->last_name $user->DOB <hr> $user->diagnosis <hr>";
            foreach ($textForm as $val) {
                foreach (explode("|", $val) as $value) {
                    if($value != 'undefined'){
                        $str .= " $value ";
                    }
                }
                $str .= "<br>";
            }
            
            //TODO: вынести в конфиг
            $this->email->from('offpoly@robul.ve.dol.ru', 'AlarmForm');
            $this->email->to('klimenov_alexand@mail.ru');
            $this->email->subject('AlarmForm');
            $this->email->message($str);
            $this->email->send();
        }
    }
    
    /*
     * Функция для получения данных для индивидульного
     * графика
     */
    public function getIndividualReport() {
        $user = $this->ion_auth->user()->row();
        $rep = $this->ReportModel->getIndividualReport($user->id);
        header('Content-Type: application/json');
        echo json_encode($rep, JSON_NUMERIC_CHECK);
    }
    
    public function getWeaserIndividualReport() {
        $user = $this->ion_auth->user()->row();
        $rep = $this->ReportModel->getWeaserIndividualReport($user->id);
        header('Content-Type: application/json');
        echo json_encode($rep, JSON_NUMERIC_CHECK);
    }
    
    /*
     * Функция для получения данных для общего графика
     */
    public function getGeneralReport() {
        $user   = $this->ion_auth->user()->row();
        $report = $this->ReportModel->getGeneralReport($user->id);
        header('Content-Type: application/json');
        echo json_encode($report, JSON_NUMERIC_CHECK);
    }
}