<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Metods extends My_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library(array('parser', 'ion_auth', 'form_validation'));
        $this->load->model('info_model', 'InfoModel');
    }

    public function index() {
        $data['title']   = "Advance MONITOR: ".$this->lang->line('methods');
        $data['isAdmin'] = $this->ion_auth->is_admin();
        $data['arrMetods'] = $this->InfoModel->getMetods();
        $this->parser->parse("metods.tpl", $data);
    }
    
    public function article($id) {
        $data['article'] = $this->InfoModel->get($id);
        $this->parser->parse("article.tpl", $data);
    }
    
    public function add() {
        if (!$this->ion_auth->is_admin()) {
            return show_error('You must be an administrator to view this page.');
        } else {
            //validate form input
            $this->form_validation->set_rules('title', 'title', 'required');
            $this->form_validation->set_rules('short_desc', 'short_desc', 'required');
            $this->form_validation->set_rules('desc', 'desc', 'required');
            $this->form_validation->set_rules('visible', 'visible', 'required');
            
            if ($this->form_validation->run() == true) {
                $data = array(
                    'type_info_id' => 2,
                    'title'        => $this->input->post('title'),
                    'short_desc'   => $this->input->post('short_desc'),
                    'desc'         => $this->input->post('desc'),
                    'is_notice'    => 0,
                    'visible'      => $this->input->post('visible'),
                );
                $this->InfoModel->insert($data);
                redirect('metods', 'refresh');
            } else {
                $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                echo validation_errors();
                
                $data['title']    = "Advance MONITOR: Adding information";
                $this->parser->parse("metods_edit.tpl", $data);
            }
        }
    }
    
    public function delete($id) {
        if (!$this->ion_auth->is_admin()) {
            return show_error('You must be an administrator to view this page.');
        } else {
            $this->InfoModel->delete($id);
            redirect('metods', 'refresh');
        }
    }
}
