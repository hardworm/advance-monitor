<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Diagnosis extends My_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library(array('parser', 'ion_auth', 'form_validation'));
        $this->load->model('diagnosis_model', 'DiagnosisModel');
        
        if($this->input->cookie('lang') == "russian"){
            $this->lang->load('diagnosis','russian');
        } else {
            $this->lang->load('diagnosis','english');
        }
    }
    
    public function index() {
        $user = $this->ion_auth->user()->row();
        $data['isAdmin'] = $this->ion_auth->is_admin();
        $data['title']       = "Advance MONITOR: ".$this->lang->line('diagnosis');
        $data['isDiagnosis'] = $this->DiagnosisModel->isDiagnosis($user->id);
        $data['diagnosis']   = $this->DiagnosisModel->diagnosis($user->id);

        $this->parser->parse("diagnosis.tpl", $data);
    }
    
    public function add() {
        $user = $this->ion_auth->user()->row();
        $isDiagnosis = $this->DiagnosisModel->isDiagnosis($user->id);
        if (!$isDiagnosis){
            $data['title']    = "Advance MONITOR: ".$this->lang->line('formation_of_diagnosis');
            $this->parser->parse("diagnosis_add.tpl", $data);
        } else {
            redirect('diagnosis', 'refresh');
        }
    }
    public function save() {
        $user = $this->ion_auth->user()->row();
        $isDiagnosis = $this->DiagnosisModel->isDiagnosis($user->id);
        if (!$isDiagnosis){
            $dataForm = json_decode($this->input->post('diagnos'));
            $this->DiagnosisModel->recDiagnosis($user->id, $dataForm);
        } else {
            redirect('diagnosis', 'refresh');
        }
    }
}