<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Database extends My_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library(array('parser', 'ion_auth'));
        $this->load->model('database_model');
        $this->load->model('report_model');
        $this->load->library('pagination');
    }
    
    public function index($page = 0) {
        if(!$this->ion_auth->is_admin()) {
            return show_error('You must be an administrator to view this page.');
        } else {
            $data['isAdmin'] = $this->ion_auth->is_admin();
            $data['title'] = "Advance MONITOR: Data Base";
            $data['users'] = $this->database_model->getUsers($page);
            
            $config['base_url'] = base_url().'database/index/';
            $config['total_rows'] = $this->database_model->getCountUser();
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();
            
            $this->parser->parse("database.tpl", $data);
        }
    }
    
    public function alarms($user_id, $page = 0) {
        if(!$this->ion_auth->is_admin() OR empty($user_id)) {
            return show_error('You must be an administrator to view this page.');
        } else {
            $data['isAdmin'] = $this->ion_auth->is_admin();
            $data['title']   = "Advance MONITOR: Data Base Alarms";
            $data['alarms']  = $this->database_model->getAlarms($user_id, $page);

            $config['base_url']   = base_url() . "database/alarm/$user_id/";
            $config['total_rows'] = $this->database_model->countGetAlarms($user_id);
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();
            
            $this->parser->parse("database_alarms.tpl", $data);
        }
    }
    
    public function bookings($user_id, $page = 0) {
        if(!$this->ion_auth->is_admin() OR empty($user_id)) {
            return show_error('You must be an administrator to view this page.');
        } else {
            $data['isAdmin'] = $this->ion_auth->is_admin();
            $data['title']   = "Advance MONITOR: Data Base Booking";
            $data['bookings']  = $this->database_model->getBooking($user_id);

            $this->parser->parse("database_bookings.tpl", $data);
        }
    }
    
    public function alarm($alarm_id) {
        if(!$this->ion_auth->is_admin() OR empty($alarm_id)) {
            return show_error('You must be an administrator to view this page.');
        } else {
            $data['isAdmin'] = $this->ion_auth->is_admin();
            $data['title']   = "Advance MONITOR: Data Base Alarm";
            $data['alarm']  = $this->report_model->getReport($alarm_id);
            $this->report_model->update($data['alarm']['id'], array('is_read'=>1));
            
            $data['alarm']['json_text'] = json_decode($data['alarm']['json_text']);
            $data['alarm']['json_params'] = json_decode($data['alarm']['json_params']);
            $data['alarm']['json_weaser'] = json_decode($data['alarm']['json_weaser']);

            $this->parser->parse("database_alarm.tpl", $data);
        }
    }
    
    public function return_visits($user_id, $page = 0) {
         if(!$this->ion_auth->is_admin() OR empty($user_id)) {
            return show_error('You must be an administrator to view this page.');
        } else {
            $data['isAdmin'] = $this->ion_auth->is_admin();
            $data['title']   = "Advance MONITOR: Data Base QS";
            $data['return_visits']  = $this->database_model->getQSs($user_id, $page);

            $config['base_url']   = base_url() . "database/return_visits/$user_id/";
            $config['total_rows'] = $this->database_model->countGetQSs($user_id);
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();
            
            $this->parser->parse("database_return_visits.tpl", $data);
        }
    }
    
    public function return_visit($qs_id) {
        if(!$this->ion_auth->is_admin() OR empty($qs_id)) {
            return show_error('You must be an administrator to view this page.');
        } else {
            $data['isAdmin'] = $this->ion_auth->is_admin();
            $data['title']   = "Advance MONITOR: Data Base QS";
            $data['return_visit']  = $this->database_model->getQS($qs_id);
            $this->database_model->setQSRead($qs_id);
            
            $data['return_visit']['json_general'] = json_decode($data['return_visit']['json_general']);
            $data['return_visit']['json_rv'] = json_decode($data['return_visit']['json_rv']);

            $this->parser->parse("database_return_visit.tpl", $data);
        }
    }
}