<?php
class Tools extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('file');
        $this->load->model('tools_model');
    }
    
	public function updateGS() {
        if($this->input->is_cli_request()){
            $string = file_get_contents('http://services.swpc.noaa.gov/text/27-day-outlook.txt');
            $arr = preg_split('/\n/', $string);
            $date = date("Y M d", time());
            foreach ($arr as $value) {
                if(mb_stripos($value, $date) === 0){
                    $arr2 = explode('         ', str_replace($date, '', $value));
                    preg_match('/[0-9]{1,2}/u', $arr2[2], $matches);
                    if($this->tools_model->setGS($matches[0])){
                        echo "ok!".PHP_EOL;
                    } else {
                        echo "error".PHP_EOL;
                    }
                    break;
                } 
            }
        }
	}
    
    public function updateMoon() {
        if($this->input->is_cli_request()){
            $arr = json_decode(file_get_contents('http://cerridwen.viridian-project.de/api/v1/moon'), true);
            
            if ($this->tools_model->setMoon($arr)) {
                echo "ok!" . PHP_EOL;
            } else {
                echo "error" . PHP_EOL;
            }
        }
    }
}