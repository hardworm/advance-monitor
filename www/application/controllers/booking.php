<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Booking extends My_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library(array('parser', 'ion_auth'));
        $this->load->model('booking_model', 'BookingModel');
        $this->load->model('return_visit_model');
        $this->load->model('diagnosis_model');
        if($this->input->cookie('lang') == "russian"){
            $this->lang->load('booking','russian');
        } else {
            $this->lang->load('booking','english');
        }
    }
    
    public function index() {
        $user = $this->ion_auth->user()->row();
        $data['isAdmin'] = $this->ion_auth->is_admin();
        $data['title']       = "Advance MONITOR: ".$this->lang->line('h1');
        $data['reservation'] = $this->BookingModel->getReservationForUser($user->id);
        $data['showUserReport'] = $this->BookingModel->showUserReportReturVisit($user->id);
        $data['showUserFistReport'] = $this->BookingModel->showUserReportFistVisit($user->id);
        
        $this->parser->parse("booking.tpl", $data);
    }
    
    public function returnVisit() {
        if($this->input->cookie('lang') == "russian"){
            $this->lang->load('returnVisit','russian');
        } else {
            $this->lang->load('returnVisit','english');
        }
        $user = $this->ion_auth->user()->row();
        if(!$this->diagnosis_model->isDiagnosis($user->id)){
            redirect('diagnosis/add/', 'refresh');
        }
        $data['title'] = "Advance MONITOR: ".$this->lang->line('h2');
        $data['last'] = $this->return_visit_model->getLastReportUser($user->id);
//        print_r($data['last']);
        
        $this->parser->parse("booking_returnVisit.tpl", $data);
    }
    
    public function fistVisit() {
        if($this->input->cookie('lang') == "russian"){
            $this->lang->load('firstVisit','russian');
        } else {
            $this->lang->load('firstVisit','english');
        }
        $user = $this->ion_auth->user()->row();
        if(!$this->diagnosis_model->isDiagnosis($user->id)){
            redirect('diagnosis/add/', 'refresh');
        }
        $data['title'] = "Advance MONITOR: ".$this->lang->line('h2');
        $data['lang'] = $this->input->cookie('lang');
        
        $this->parser->parse("booking_fistVisit.tpl", $data);
    }
    
    public function exportExcell() {
        $user = $this->ion_auth->user()->row();
        
        $this->load->library('excel');
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="returnVisit.xls"');
        header('Cache-Control: max-age=0');
        
        // Assign cell values
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('ReturnVisit');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Date');
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
        $this->excel->getActiveSheet()->setCellValue('B1', 'Structural');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Functional');
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('D1', 'Cognitive');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Physiological');
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('F1', 'Diseases');
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('G1', 'total score');
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        
        $allReport = $this->return_visit_model->getAllIndividualReport($user->id);
        $i=2;
        foreach ($allReport as $val) {
            $this->excel->getActiveSheet()->setCellValue("A$i", $val->date);
            $this->excel->getActiveSheet()->setCellValue("B$i", $val->structural_defects);
            $this->excel->getActiveSheet()->setCellValue("C$i", $val->funtsionalnost_sphere);
            $this->excel->getActiveSheet()->setCellValue("D$i", $val->cognitive);
            $this->excel->getActiveSheet()->setCellValue("E$i", $val->physiological_services);
            $this->excel->getActiveSheet()->setCellValue("F$i", $val->disease);
            $this->excel->getActiveSheet()->setCellValue("G$i", $val->total_score);
            $i++;
        }

        // Save it as an excel 2003 file
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    public function saveFistVisit() {
        $user = $this->ion_auth->user()->row();
        $this->config->load('return_visit', TRUE);
//        $isReportThisDay = $this->ReportModel->isReportThisDay($user->id);
        
        if(true){
            $dataReport = array(
                'users_id'          => $user->id,
                'date'              => date("Y-m-d H:i:s", time()),
                'json_general'      => $this->input->post('general_otchet'),
                'json_general_vals' => $this->input->post('general_otchet_vals'),
                'json_rv'           => $this->input->post('fv'),
                'json_rv_vals'      => $this->input->post('fv_vals'),
            );
            $report_id = $this->return_visit_model->insert($dataReport);
            $dataForm = json_decode($this->input->post('fv_vals'));
            $general_otchet = json_decode($this->input->post('general_otchet'));
            $rv = json_decode($this->input->post('fv'));
          
            if (!empty($dataForm) AND is_int($report_id)) {
                $this->return_visit_model->calculateAndRecordFistVisit($report_id, $dataForm, $this->config->item('return_visit'));
                $this->sendReportFist($general_otchet, $rv);
            }
        }
    }
    
    public function saveReturnVisit() {
        $user = $this->ion_auth->user()->row();
        $this->config->load('return_visit', TRUE);
//        $isReportThisDay = $this->ReportModel->isReportThisDay($user->id);
//        file_put_contents('/tmp/json.txt',print_r($this->input->post(), true));
        if(true){
            $dataReport = array(
                'users_id'          => $user->id,
                'date'              => date("Y-m-d H:i:s", time()),
                'json_general'      => $this->input->post('general_otchet'),
                'json_general_vals' => $this->input->post('general_otchet_vals'),
                'json_rv'           => $this->input->post('rv'),
                'json_rv_vals'      => $this->input->post('rv_vals'),
            );
            $report_id = $this->return_visit_model->insert($dataReport);
            $dataForm = json_decode($this->input->post('rv_vals'));
            $general_otchet = json_decode($this->input->post('general_otchet'));
            $rv = json_decode($this->input->post('rv'));
//            file_put_contents('/tmp/dataForm.txt',print_r($dataReport, true));
          
            if (!empty($dataForm) AND is_int($report_id)) {
                $this->return_visit_model->calculateAndRecordReturnVisit($report_id, $dataForm, $this->config->item('return_visit'));
                $this->sendReportReturn($general_otchet, $rv);
            }
        }
    }
    
    
    /**
     * Функция для сбора данных массивов в удобочетаемый вид 
     * @param array $general_otchet
     * @param array $rv
     * @return string
     */
    private function constructReportForSend($general_otchet, $rv) {
        $user = $this->ion_auth->user()->row();
        
        if(!empty($general_otchet) AND !empty($rv)){
            //собираем ответы польователя
            $str = "$user->first_name $user->last_name $user->DOB <hr> $user->diagnosis <hr>";
            foreach ($general_otchet as $val) {
                foreach (explode("|", $val) as $value) {
                    if($value != 'undefined'){
                        $str .= " $value ";
                    }
                }
                $str .= "<br>";
            }
            $str .= "<hr>";
            foreach ($rv as $val) {
                foreach (explode("|", $val) as $value) {
                    if($value != 'undefined'){
                        $str .= " $value ";
                    }
                }
                $str .= "<br>";
            }
        }
        return $str;
    }
    
    /**
     * Функция отправки данных для первичного отчета
     * @param array $general_otchet
     * @param array $rv
     */
    private function sendReportFist($general_otchet, $rv) {
        if (!empty($general_otchet) AND ! empty($rv)) {
            $text = $this->constructReportForSend($general_otchet, $rv);

            $this->email->from('offpoly@robul.ve.dol.ru', 'AdvanceMonitor');
            $this->email->to('klimenov_alexand@mail.ru');
            $this->email->subject('First visit');
            $this->email->message($text);
            $this->email->send();
        }
    }
    
    /**
     * Функция для отправки данных повторного визита
     * @param array $general_otchet
     * @param array $rv
     */
    private function sendReportReturn($general_otchet, $rv) {
        $user = $this->ion_auth->user()->row();
        $count = $this->return_visit_model->getCountAllIndividualReport($user->id);
        if (!empty($general_otchet) AND ! empty($rv)) {
            $text = $this->constructReportForSend($general_otchet, $rv);

            $this->email->from('offpoly@robul.ve.dol.ru', 'AdvanceMonitor');
            $this->email->to('klimenov_alexand@mail.ru');
            $this->email->subject("Return visit $count");
            $this->email->message($text);
            $this->email->send();
        }
    }
    
    public function add() {
        $user = $this->ion_auth->user()->row();
        if(!$this->input->get('calendar_id') AND !$this->input->get('category_id')) {
            //get default category and default calendar of the category
            $category_id = $this->BookingModel->getDefaultCategoryID();
            $calendar_id = $this->BookingModel->getDefaultCalendarId($category_id);
        } else if($this->input->get('calendar_id')) {
            $calendar_id = $this->BookingModel->getCalendar('calendar_id',$this->input->get('calendar_id'));
            $category_id = $this->BookingModel->getCalendar('category_id',$this->input->get('calendar_id'));
        } else if($this->input->get('category_id')) {
            $category_id = $this->BookingModel->getCategoryID($this->input->get('category_id'));
            $calendar_id = $this->BookingModel->getDefaultCalendarId($category_id);
        }
        
        $data['arrFields'] = $this->BookingModel->getVisibleFields();
        $data['mandatoryFields'] = $this->BookingModel->getMandatoryFields();
        $data['reservFieldType'] = $this->BookingModel->getReservationFieldType();
        $data['arrayCategories'] = $this->BookingModel->getCategoriesList();
        $data['arrayCalendars'] = $this->BookingModel->getCalendarsList($category_id);
        $data['Labels'] = $this->BookingModel->getLabels();
        
        $data['title']     = "Advance MONITOR: ".$this->lang->line('h1');
        $data['setting']   = $this->BookingModel->getSetting();
        $data['publickey'] = '';
        $data['calendar_title'] = $this->BookingModel->getCalendar('calendar_title', $calendar_id);
        $data['calendar_id'] = $calendar_id;
        $data['category_id'] = $category_id;
        $data['GET'] = $this->input->get();
        

        $this->parser->parse("booking_add.tpl", $data);
    }
    
    public function getCalendarsList() {
        $user = $this->ion_auth->user()->row();
        $data['arrayCalendars'] = $this->BookingModel->getCalendarsList($this->input->get('category_id'));
        $data['default'] = 0;
        foreach($data['arrayCalendars'] as $calendarId => $calendar) {
            if($calendar["calendar_order"] == 0) {
                $data['default'] = $calendarId;
                $arrCalendar = $this->BookingModel->getCalendar('calendar_id', $data['default']);
                $calendar_id = $arrCalendar->calendar_id;
            }
        }
        $data['date'] = $this->BookingModel->getFirstFilledMonth(@$calendar_id);
        echo $this->parser->parse("/booking/calendarsList.tpl", $data, TRUE);
    }
    
    public function getCalendar() {
        $user = $this->ion_auth->user()->row();
        $arrCalendar = $this->BookingModel->getCalendar('calendar_id', $this->input->get('calendar_id'));
        echo $this->BookingModel->getFirstFilledMonth($arrCalendar->calendar_id);
    }
    
    public function getMonthCalendar() {
        $user = $this->ion_auth->user()->row();
        $setting = $this->BookingModel->getSetting();
        
        if ($setting['date_format'] == "UK" || $setting['date_format'] == "EU") {
            $data['startDay']       = 1;
            $data['weekday_format'] = "N";
            $data['lastWeekDay']    = 7;
        } else {
            $data['startDay']       = 0;
            $data['weekday_format'] = "w";
            $data['lastWeekDay']    = 6;
        }
        
        $data['arrayMonth'] = $this->BookingModel->getMonthCalendar($this->input->get("month"),$this->input->get("year"),$data['weekday_format']);
        
        if ($this->input->get('calendar_id') > 0) {
            $data['calendar_id'] = $this->input->get('calendar_id');
            
            $data['numslots'] = $this->BookingModel->getSlotsPerDay($this->input->get('year'), $this->input->get("month"), $data['arrayMonth'], $this->input->get("calendar_id"), $setting);
            $date = date_create(date('Y-m-d'));

            if(function_exists("date_add")) {
                date_add($date, date_interval_create_from_date_string($setting['book_from'].' days'));
            } else {
                date_modify($date, '+'.$setting['book_from'].' day');
            }
            $data['bookfromdate'] = date_format($date, 'Ymd');

            if($setting['book_to'] > 0) {
                $date = date_create(date('Y-m-d'));
                if(function_exists("date_add")) {
                    date_add($date, date_interval_create_from_date_string($setting['book_to'].' days'));
                } else {
                    date_modify($date, '+'.$setting['book_to'].' day');
                }

                $data['booktodate'] = date_format($date, 'Ymd');
            } else {
                $data['booktodate'] = '30001010';
            }
        } else {
            
        }
        
        $data['Labels'] = $this->BookingModel->getLabels();
        $data['GET'] = $this->input->get();
        $data['setting'] = $setting;
        $data['slots_popup_enabled'] = $setting['slots_popup_enabled'];
        
        echo $this->parser->parse("/booking/getMonthCalendar.tpl", $data, TRUE);
    }
    
    public function fillSlotsPopup() {
        $user = $this->ion_auth->user()->row();
        $data['setting'] = $this->BookingModel->getSetting();
        $data['arraySlots'] = $this->BookingModel->getSlotsPerDayList($this->input->get("year"),  $this->input->get("month"),
             $this->input->get("day"),  $this->input->get("calendar_id"),$data['setting']);
        $arrCalendar = $this->BookingModel->getCalendar('calendar_title', $this->input->get('calendar_id'));
        $data['calendar_title'] = $arrCalendar->calendar_title;
        
        $columns = ceil(count($data['arraySlots']) / 6);
        if ($data['setting']['time_format'] == "12") {
            $maxColumn = 7;
        } else {
            $maxColumn = 9;
        }
        $lines = 6;
        if ($columns > $maxColumn) {
            $columns = $maxColumn;
            $lines   = 7;
            do {
                $lines++;
            } while (ceil(count($data['arraySlots']) / $lines) > $maxColumn);
        }
        $data['lines'] = $lines;
        $data['columns'] = $columns;

        echo $this->parser->parse("/booking/fillSlotsPopup.tpl", $data, TRUE);
    }
    
    public function getBookingForm() {
        $user               = $this->ion_auth->user()->row();
        $data['setting']    = $this->BookingModel->getSetting();
        $data['Labels']     = $this->BookingModel->getLabels();
        $data['maxColumn']  = 0;
        $data['arraySlots'] = $this->BookingModel->getSlotsPerDayList($this->input->get("year"), $this->input->get("month"), $this->input->get("day"), $this->input->get("calendar_id"), $data['setting']);
        $data['GET'] = $this->input->get();
        $arrCalendar = $this->BookingModel->getCalendar('*', $this->input->get('calendar_id'));
        $data['calendar_id'] = $arrCalendar->calendar_id;
        $data['calendar_title'] = $arrCalendar->calendar_title;
        
        $datenext = date_create($this->input->get("year")."-".$this->input->get("month")."-".$this->input->get("day"));
        date_add($datenext, date_interval_create_from_date_string('1 days'));

        if ($this->BookingModel->checkFutureSlots(date_format($datenext, 'Y'), date_format($datenext, 'm'), date_format($datenext, 'd'), $this->input->get("calendar_id"))) {
            $data['next']           = strtotime(date("Y-m-d", mktime(0, 0, 0, $this->input->get("month"), $this->input->get("day"), $this->input->get("year"))) . "+ 1 day");
            $data['nextDay']        = date("d", $data['next']);
            $data['nextMonth']      = date("m", $data['next']);
            $data['nextYear']       = date("Y", $data['next']);
            $arraySlotsNext = $this->BookingModel->getSlotsPerDayList($data['nextYear'], $data['nextMonth'], $data['nextDay'], $this->input->get("calendar_id"), $data['setting']);
            if (count($arraySlotsNext) == 0) {
                do {
                    $data['next']           = strtotime(date("Y-m-d", mktime(0, 0, 0, $data['nextMonth'], $data['nextDay'], $data['nextYear'])) . "+ 1 day");
                    $data['nextDay']        = date("d", $data['next']);
                    $data['nextMonth']      = date("m", $data['next']);
                    $data['nextYear']       = date("Y", $data['next']);
                    $arraySlotsNext = $this->BookingModel->getSlotsPerDayList($data['nextYear'], $data['nextMonth'], $data['nextDay'], $this->input->get("calendar_id"), $data['setting']);
                } while (count($arraySlotsNext) == 0);
            }
        } else {
            $data['next'] = '';
        }
        $dateprev = date_create($this->input->get("year")."-". $this->input->get("month")."-".$this->input->get("day"));
        date_sub($dateprev, date_interval_create_from_date_string('1 days'));
        if ($this->BookingModel->checkPastSlots(date_format($dateprev, 'Y'), date_format($dateprev, 'm'), date_format($dateprev, 'd'), $this->input->get("calendar_id"))) {
            $data['prev']           = strtotime(date("Y-m-d", mktime(0, 0, 0, $this->input->get("month"), $this->input->get("day"), $this->input->get("year"))) . "- 1 day");
            $data['prevDay']        = date("d", $data['prev']);
            $data['prevMonth']      = date("m", $data['prev']);
            $data['prevYear']       = date("Y", $data['prev']);
            $arraySlotsPrev = $this->BookingModel->getSlotsPerDayList($data['prevYear'], $data['prevMonth'], $data['prevDay'], $this->input->get("calendar_id"), $data['setting']);
            if (count($arraySlotsPrev) == 0) {
                do {
                    $data['prev']           = strtotime(date("Y-m-d", mktime(0, 0, 0, $data['prevMonth'], $data['prevDay'], $data['prevYear'])) . "- 1 day");
                    $data['prevDay']        = date("d", $data['prev']);
                    $data['prevMonth']      = date("m", $data['prev']);
                    $data['prevYear']       = date("Y", $data['prev']);
                    $arraySlotsPrev = $this->BookingModel->getSlotsPerDayList($data['prevYear'], $data['prevMonth'], $data['prevDay'], $this->input->get("calendar_id"), $data['setting']);
                } while (count($arraySlotsPrev) == 0);
            }
        } else {
            $data['prev'] = '';
        }
        $date = date_create(date('Y-m-d'));

        if (function_exists("date_add")) {
            date_add($date, date_interval_create_from_date_string($data['setting']['book_from'] . ' days'));
        } else {
            date_modify($date, '+' . $data['setting']['book_from'] . ' day');
        }
        $data['bookfromdate'] = date_format($date, 'Ymd');
        $date = date_create(date('Y-m-d'));
        if (function_exists("date_add")) {
            date_add($date, date_interval_create_from_date_string($data['setting']['book_to'] . ' days'));
        } else {
            date_modify($date, '+' . $data['setting']['book_to'] . ' day');
        }
        $data['booktodate'] = date_format($date, 'Ymd');
        $data['wd'] = intval(date('w',mktime(0,0,0,  $this->input->get("month"),  $this->input->get("day"), $this->input->get("year"))));

        echo $this->parser->parse("/booking/getBookingForm.tpl", $data, TRUE);
    }

    public function doReservation() {
        $user          = $this->ion_auth->user()->row();
        $setting       = $this->BookingModel->getSetting();
        $Labels        = $this->BookingModel->getLabels();
        $visibleFields = $this->BookingModel->getVisibleFields();
        $arrCalendar   = $this->BookingModel->getCalendar('*', $this->input->post('calendar_id'));

        $confirm          = 0;
        $POST = $this->input->post();
        $POST['reservation_name']    = $user->first_name;
        $POST['reservation_surname'] = $user->last_name;
        $POST['reservation_email']   = $user->email;

        $listReservations = $this->BookingModel->insertReservation($setting, $POST, $user->id);
        if ($listReservations != '') {
            if ($setting['reservation_confirmation_mode'] == 1 && $this->input->post('with_paypal') == FALSE) {
                $this->BookingModel->confirmReservations($listReservations);
            }
            $confirm = 1;
        } else {
            $confirm = 0;
        }
        if ($confirm == 1) {
            //send reservation email to admin
            //check first if the current calendar has a custom email address
            $to = $setting['email_reservation'];

            $subject = $Labels["DORESERVATION_MAIL_ADMIN_SUBJECT"];
            $message = $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE1"] . "<br>";

            if (in_array("reservation_name", $visibleFields)) {
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE2"] . "</strong>: " . $user->first_name . "<br>";
            }
            if (in_array("reservation_surname", $visibleFields)) {
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE3"] . "</strong>: " . $user->last_name . "<br>";
            }
            if (in_array("reservation_email", $visibleFields)) {
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE4"] . "</strong>: " . $user->email . "<br>";
            }
            if (in_array("reservation_phone", $visibleFields)) {
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE5"] . "</strong>: " . $this->input->post("reservation_phone") . "<br>";
            }
            if (in_array("reservation_message", $visibleFields)) {
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE6"] . "</strong>: " . $this->input->post("reservation_message") . "<br>";
            }
            if (in_array("reservation_field1", $visibleFields)) {
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE10"] . "</strong>: " . $this->input->post("reservation_field1") . "<br>";
            }
            if (in_array("reservation_field2", $visibleFields)) {
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE11"] . "</strong>: " . $this->input->post("reservation_field2") . "<br>";
            }
            if (in_array("reservation_field3", $visibleFields)) {
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE12"] . "</strong>: " . $this->input->post("reservation_field3") . "<br>";
            }
            if (in_array("reservation_field4", $visibleFields)) {
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE13"] . "</strong>: " . $this->input->post("reservation_field4") . "<br>";
            }
            $message.="<br><strong>" . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE7"] . "</strong>:<br>";
            $message.="<ul type='disc'>";
            //loop through slots
            for ($i = 0; $i < count($this->input->post("reservation_slot")); $i++) {
                $reservation_slot = $this->input->post("reservation_slot");
                $slotsObj         = $this->BookingModel->getSlot($reservation_slot[$i]);

                $message.="<li>";
                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_CALENDAR"] . "</strong>: " . $arrCalendar->calendar_title . "<br>";
                $dateToSend = strftime('%Y/%m/%d', strtotime($slotsObj->slot_date));

                $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_DATE"] . "</strong>: " . $dateToSend . "<br>";
                if ($setting['time_format'] == "12") {
                    $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_TIME"] . "</strong>: " . $slotsObj->slot_time_from . "-" . $slotsObj->slot_time_to . "<br>";
                } else {
                    $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_TIME"] . "</strong>: " . $slotsObj->slot_time_from . "-" . $slotsObj->slot_time_to . "<br>";
                }
                if ($setting['slots_unlimited'] == 2) {
                    $message.="<strong>" . $Labels["DORESERVATION_MAIL_ADMIN_SEATS"] . "</strong>: " . $this->input->post("reservation_seats_" . $reservation_slot[$i]) . "<br>";
                }
                $message.="</li>";
            }
            $message.="</ul>";
            if ($setting['reservation_confirmation_mode'] == 3) {
                $message.= $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE8"] . '<a href="' . $setting['site_domain'] . '/admin/">' . $Labels["DORESERVATION_MAIL_ADMIN_MESSAGE9"] . '</a>';
            }

            //для админа
            $this->email->from('offpoly@robul.ve.dol.ru', 'Reservation');
//            $this->email->mailtype('html');
            $this->email->to('klimenov_alexand@mail.ru');
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();

            if (in_array("reservation_email", $visibleFields)) {
                //send reservation email to user
                $to = $user->email;

                //WARNING!! static mail record ids, if deleted/changed, must be changed here also
                switch ($setting['reservation_confirmation_mode']) {
                    case "1":
                        $mailObj = $this->BookingModel->getMail(1);
                        break;
                    case "2":
                        $mailObj = $this->BookingModel->getMail(2);
                        break;
                    case "3":
                        $mailObj = $this->BookingModel->getMail(3);
                        break;
                }
                $subject = $mailObj->email_subject;
                //setting username in message
                $message = str_replace("[customer-name]", $this->input->post("reservation_name"), $mailObj->email_text);
                //check if cancellation is enabled id email is 1
                if ($mailObj->email_id == 1 && $setting['reservation_cancel'] == "1") {
                    $message .= $mailObj->email_cancel_text;
                }
                //setting reservation detail in message
                //loop through slots
                $res_details = "";
                for ($i = 0; $i < count($this->input->post("reservation_slot")); $i++) {
                    $reservation_slot = $this->input->post("reservation_slot");
                    $slotsObj         = $this->BookingModel->getSlot($reservation_slot[$i]);
                    $res_details.="<strong>" . $Labels["DORESERVATION_MAIL_USER_VENUE"] . "</strong>: " . $arrCalendar->calendar_title . "<br>";
                    $dateToSend       = strftime('%Y/%m/%d', strtotime($slotsObj->slot_date));
                    $res_details.="<strong>" . $Labels["DORESERVATION_MAIL_USER_DATE"] . "</strong>: " . $dateToSend . "<br>";
                    if ($slotsObj->slot_special_mode == 1) {
                        if ($setting['time_format'] == "12") {
                            $res_details.="<strong>" . $Labels["DORESERVATION_MAIL_USER_TIME"] . "</strong>: " . $slotsObj->slot_time_from . "-" . $slotsObj->slot_time_to;
                        } else {
                            $res_details.="<strong>" . $Labels["DORESERVATION_MAIL_USER_TIME"] . "</strong>: " . $slotsObj->slot_time_from . "-" . $slotsObj->slot_time_to;
                        }
                        if ($slotsObj->slot_special_text != '') {
                            $res_details.=" - " . $slotsObj->slot_special_text;
                        }
                        $res_details.="<br>";
                    } else if ($slotsObj->slot_special_mode == 0 && $slotsObj->slot_special_text != '') {
                        $res_details.="<strong>" . $Labels["DORESERVATION_MAIL_USER_TIME"] . "</strong>:" . $slotsObj->slot_special_text . "<br>";
                    } else {
                        if ($setting['time_format'] == "12") {
                            $res_details.="<strong>" . $Labels["DORESERVATION_MAIL_USER_TIME"] . "</strong>: " . $slotsObj->slot_time_from . "-" . $slotsObj->slot_time_to . "<br>";
                        } else {
                            $res_details.="<strong>" . $Labels["DORESERVATION_MAIL_USER_TIME"] . "</strong>: " . $slotsObj->slot_time_from . "-" . $slotsObj->slot_time_to . "<br>";
                        }
                    }
                    if ($setting['slots_unlimited'] == 2) {
                        $res_details.="<strong>" . $Labels["DORESERVATION_MAIL_USER_SEATS"] . "</strong>: " . $this->input->post("reservation_seats_" . $reservation_slot[$i]) . "<br>";
                    }
                    $res_details.="<br><br>";
                }
                $message = str_replace("[reservation-details]", $res_details, $message);


                if ($mailObj->email_id == 2) {
                    //setting reservation confirmation link in message
                    //if he must confirm it via mail, I send the link
                    $message = str_replace("[confirmation-link]", "<a href='" . base_url() . "/booking/confirm/?reservations=" . $listReservations . "'>" . $Labels["DORESERVATION_MAIL_USER_MESSAGE3"] . "</a>", $message);
                    $message = str_replace("[confirmation-link-url]", base_url() . "/booking/confirm/?reservations=" . $listReservations, $message);
                }

                if ($mailObj->email_id == 1 && $setting['reservation_cancel'] == "1") {
                    $message = str_replace("[cancellation-link]", "<a href='" . base_url() . "/booking/confirm/?reservations=" . $listReservations . "'>" . $Labels["DORESERVATION_MAIL_USER_MESSAGE4"] . "</a>", $message);
                    $message = str_replace("[cancellation-link-url]", base_url() . "/booking/confirm/?reservations=" . $listReservations, $message);
                }
                $message.="<br><br>" . $mailObj->email_signature;

                //для пользователя
                $this->email->from('offpoly@robul.ve.dol.ru', 'Reservation');
                //$this->email->mailtype('html');
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($message);
                $this->email->send();
            }

            $arrReservations = explode(",", $listReservations);
            $htmlToAppend    = "";
            for ($i = 0; $i < count($arrReservations); $i++) {
                $htmlToAppend.='<input type="hidden" name="item_number_' . ($i + 1) . '" value="' . $arrReservations[$i] . '" />';
            }
            echo "<script>window.parent.showResponse($arrCalendar->calendar_id)</script>";
        }
    }
}