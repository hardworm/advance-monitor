<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Конфигурационный фал для хранения баллов alarm form и их принадлежности к разделам
 */

//плачет
$config['cry'] = array(
    'high'   => array(3, 'behavior'),
    'middle' => array(2, 'behavior'),
    'low'    => array(1, 'behavior'),
);

//возбуждение
$config['hyperactivated'] = array(
    'high'   => array(4, 'behavior'),
    'middle' => array(3, 'behavior'),
    'low'    => array(1, 'behavior'),
);

//вялось
$config['floppy'] = array(
    'high'   => array(3, 'behavior'),
    'middle' => array(2, 'behavior'),
    'low'    => array(1, 'behavior'),
);

//Мышечный гипертонус
$config['muscular_hypertonus'] = array(
    'hands' => array(
        'high'   => array(4, 'neurologic'),
        'middle' => array(3, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
    'feet' => array(
        'high'   => array(4, 'neurologic'),
        'middle' => array(3, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
    'general' => array(
        'high'   => array(4, 'neurologic'),
        'middle' => array(3, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
);

//Нарушение координации
$config['incoordination'] = array(
    'high'   => array(4, 'neurologic'),
    'middle' => array(3, 'neurologic'),
    'low'    => array(1, 'neurologic'),
);

//Мышечная гипотония
$config['hypotonia'] = array(
    'high'   => array(3, 'neurologic'),
    'middle' => array(2, 'neurologic'),
    'low'    => array(1, 'neurologic'),
);

//Гиперсенситивность
$config['hypersensibity'] = array(
    'to_sound' => array(
        'high'   => array(4, 'neurologic'),
        'middle' => array(3, 'neurologic'),
        'low'    => array(2, 'neurologic'),
    ),
    'to_light' => array(
        'high'   => array(4, 'neurologic'),
        'middle' => array(3, 'neurologic'),
        'low'    => array(2, 'neurologic'),
    ),
    'to_touch' => array(
        'high'   => array(4, 'neurologic'),
        'middle' => array(3, 'neurologic'),
        'low'    => array(2, 'neurologic'),
    ),
);

//Судороги
$config['seizures'] = array(
    'increased'  => array(
        'high'   => array(6, 'neurologic'),
        'middle' => array(5, 'neurologic'),
        'low'    => array(3, 'neurologic'),
    ),
    'first_time' => array(
        '1attack'     => array(4, 'neurologic'),
        '2of10attack' => array(6, 'neurologic'),
        '10attack'    => array(8, 'neurologic'),
    ),
    'new_type'   => array(
        '1attack'     => array(3, 'neurologic'),
        '2of10attack' => array(4, 'neurologic'),
        '10attack'    => array(5, 'neurologic'),
    ),
);

//Гиперкинезы
$config['hyperkynesis'] = array(
    'increased'  => array(
        'high'   => array(3, 'neurologic'),
        'middle' => array(2, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
    'first_time' => array(
        'high'   => array(6, 'neurologic'),
        'middle' => array(4, 'neurologic'),
        'low'    => array(2, 'neurologic'),
    ),
    'new_type'   => array(
        'high'   => array(3, 'neurologic'),
        'middle' => array(2, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
);

//Стереотипии или эхолалия
$config['stereotypes'] = array(
    'increased'  => array(
        'high'   => array(3, 'neurologic'),
        'middle' => array(2, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
    'first_time' => array(
        'high'   => array(3, 'neurologic'),
        'middle' => array(2, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
    'new_type'   => array(
        'high'   => array(3, 'neurologic'),
        'middle' => array(2, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
);

//Головная боль
$config['headache'] = array(
    'increased'  => array(
        'high'   => array(3, 'neurologic'),
        'middle' => array(2, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
    'first_time' => array(
        'high'   => array(3, 'neurologic'),
        'middle' => array(2, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
    'new_type'   => array(
        'high'   => array(3, 'neurologic'),
        'middle' => array(2, 'neurologic'),
        'low'    => array(1, 'neurologic'),
    ),
);

//Сон
$config['sleep'] = array(
    'day_dream'  => array(
        'shortened' => array(2, 'neurologic'),
        'missing'   => array(1, 'neurologic'),
    ),
    'night_dream' => array(
        'sleep_short'               => array(5, 'neurologic'),
        'often_wakes_up'            => array(4, 'neurologic'),
        'wakes_up12'                => array(2, 'neurologic'),
        'wakes_up_early'            => array(3, 'neurologic'),
        'difficulty_falling_asleep' => array(1, 'neurologic'),
    ),
    'inversion_sleep'    => array(6, 'neurologic'),
);

//Температура
$config['temperature'] = array(
    'down38' => array(2, 'vegetative'),
    'up38'   => array(3, 'vegetative'),
);

//Потливость
$config['swelling'] = array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
);

//Слюнотечение
$config['hypersalivation'] = array(
    'quickened'  => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'first_time' => array(
        'high'   => array(4, 'vegetative'),
        'middle' => array(3, 'vegetative'),
        'low'    => array(2, 'vegetative'),
    ),
    'decreased'   => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
);

//Похолодание конечностей
$config['cool_limbs'] = array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
);

//Бледность
$config['pale'] = array(
    'hands'  => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'feet' => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'face'   => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'general'   => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
);

//Покраснение
$config['errhitena'] = array(
    'hands'  => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'feet' => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'face'   => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'general'   => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
);

//Мраморная кожа
$config['marple_skin'] = array(
    'hands'  => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'feet' => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'general'   => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
);

//Цианоз
$config['cyanos'] = array(
    'limbs'  => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
    'nasolabial_triangle' => array(
        'high'   => array(3, 'vegetative'),
        'middle' => array(2, 'vegetative'),
        'low'    => array(1, 'vegetative'),
    ),
);

//Нерегулярное дыхание
$config['irregular_breathing'] = array(
    'fist_time'  => array(
        'high'   => array(6, 'respiratory'),
        'middle' => array(4, 'respiratory'),
        'low'    => array(2, 'respiratory'),
    ),
    'quickened' => array(
        'high'   => array(3, 'respiratory'),
        'middle' => array(2, 'respiratory'),
        'low'    => array(1, 'respiratory'),
    ),
);

//Периоды апноэ
$config['periods_of_apnea'] = array(
    'fist_time'  => array(
        'high'   => array(6, 'respiratory'),
        'middle' => array(4, 'respiratory'),
        'low'    => array(2, 'respiratory'),
    ),
    'quickened' => array(
        'high'   => array(3, 'respiratory'),
        'middle' => array(2, 'respiratory'),
        'low'    => array(1, 'respiratory'),
    ),
);

//Частое дыхание
$config['tachypnea'] = array(
    'fist_time'  => array(
        'high'   => array(6, 'respiratory'),
        'middle' => array(4, 'respiratory'),
        'low'    => array(2, 'respiratory'),
    ),
    'quickened' => array(
        'high'   => array(3, 'respiratory'),
        'middle' => array(2, 'respiratory'),
        'low'    => array(1, 'respiratory'),
    ),
);

//Ослабленное дыхание
$config['weak'] = array(
    'fist_time'  => array(
        'high'   => array(6, 'respiratory'),
        'middle' => array(4, 'respiratory'),
        'low'    => array(2, 'respiratory'),
    ),
    'quickened' => array(
        'high'   => array(3, 'respiratory'),
        'middle' => array(2, 'respiratory'),
        'low'    => array(1, 'respiratory'),
    ),
);

//Затрудненный выдох
$config['difficult_to_exhail'] = array(
    'fist_time'  => array(
        'high'   => array(6, 'respiratory'),
        'middle' => array(4, 'respiratory'),
        'low'    => array(2, 'respiratory'),
    ),
    'quickened' => array(
        'high'   => array(3, 'respiratory'),
        'middle' => array(2, 'respiratory'),
        'low'    => array(1, 'respiratory'),
    ),
);

//Затрудненный вдох
$config['difficult_to_inhail'] = array(
    'fist_time'  => array(
        'high'   => array(6, 'respiratory'),
        'middle' => array(4, 'respiratory'),
        'low'    => array(2, 'respiratory'),
    ),
    'quickened' => array(
        'high'   => array(3, 'respiratory'),
        'middle' => array(2, 'respiratory'),
        'low'    => array(1, 'respiratory'),
    ),
);

//Кашель
$config['cough'] = array(
    'dry_cough'  => array(
        'high'   => array(3, 'respiratory'),
        'middle' => array(2, 'respiratory'),
        'low'    => array(1, 'respiratory'),
    ),
    'wet_cough' => array(
        'high'   => array(3, 'respiratory'),
        'middle' => array(2, 'respiratory'),
        'low'    => array(1, 'respiratory'),
    ),
);

//насморк
$config['snuffle'] = array(
        'high'   => array(3, 'respiratory'),
        'middle' => array(2, 'respiratory'),
        'low'    => array(1, 'respiratory'),
);

//Аппетит
$config['appetite'] = array(
    'increased'  => array(
        'high'   => array(3, 'alimentary'),
        'middle' => array(2, 'alimentary'),
        'low'    => array(1, 'alimentary'),
    ),
    'down' => array(
        'high'   => array(3, 'alimentary'),
        'middle' => array(2, 'alimentary'),
        'low'    => array(1, 'alimentary'),
    ),
    'unchanged' => array(
        'high'   => array(3, 'alimentary'),
        'middle' => array(2, 'alimentary'),
        'low'    => array(1, 'alimentary'),
    )
);

//Питье
$config['drinking'] = array(
    'failure' => array(4, 'alimentary'),
    'down'    => array(3, 'alimentary'),
);

//Стул
$config['appetite'] = array(
    'lock'  => array(3, 'alimentary'),
    'diarrhea' => array(
        'high'   => array(5, 'alimentary'),
        'middle' => array(3, 'alimentary'),
        'low'    => array(1, 'alimentary'),
    ),
);

//Cрыгивания
$config['crygivaniya'] = array(
    'spontaneous'  => array(
        'frequent' => array(3, 'alimentary'),
        'few'      => array(2, 'alimentary'),
        'single'   => array(1, 'alimentary'),
    ),
    'after_eating' => array(
        'frequent' => array(3, 'alimentary'),
        'few'      => array(2, 'alimentary'),
        'single'   => array(1, 'alimentary'),
    ),
);

//Рвота
$config['vomiting'] = array(
    'spontaneous'  => array(
        'indomitable' => array(3, 'alimentary'),
        'strong'      => array(2, 'alimentary'),
        'single'      => array(1, 'alimentary'),
    ),
    'after_eating' => array(
        'indomitable' => array(3, 'alimentary'),
        'strong'      => array(2, 'alimentary'),
        'single'      => array(1, 'alimentary'),
    ),
);

//Стул
$config['reflux'] = array(
    'fist_time' => array(2, 'alimentary'),
    'quickened' => array(1, 'alimentary'),
);

//Мочеиспускание
$config['urinary'] = array(
    'quickened' => array(
        'low'  => array(2, 'urinary'),
        'high' => array(3, 'urinary'),
    ),
    'few' => array(
        'low'  => array(5, 'urinary'),
        'high' => array(4, 'urinary'),
    ),
    'is_not'    => array(6, 'urinary'),
);
