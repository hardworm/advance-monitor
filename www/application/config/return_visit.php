<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Конфигурационный файл для хранения баллов "Повторный визит" и их принадлежности к разделам
 */

//Грудная клетка
$config['thorax'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Шейный отдел позвоночника
$config['cervical_spine'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Грудной отдел позвоночника
$config['thoracic_spine'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Поясничный отдел позвоночника
$config['lumbar_spine'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Живот
$config['belly'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Таз
$config['pelvis'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Бедра
$config['hip'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Ноги
$config['legs'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Стопы
$config['foot'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Лопатки
$config['shoulder-blade'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Плечи
$config['upper_arm'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Руки
$config['hands'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);
//Голова
$config['head'] = array(
    'severe'   => array(-40, 'structural_defects'),
    'poor'     => array(-30, 'structural_defects'),
    'moderate' => array(-20, 'structural_defects'),
    'mild'     => array(-10, 'structural_defects'),
    'normal'   => array(0, 'structural_defects'),
);



//Удерживание головы
$config['holding_head'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Координированные движения конечностей
$config['coordinated_limb_movements'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Движения рук
$config['hand_movements'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Движения ног
$config['leg_movements'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Движения туловища
$config['trunk_movements'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Переворачиваться
$config['turn'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Самостоятельно сидеть
$config['sit_up_unaided'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Удерживать равновесие
$config['keep_balance'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Ползать
$config['creep'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Стоять
$config['stand'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Ходить
$config['walk'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Бегать
$config['run'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Издавать звуки
$config['vocalize'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Говорить
$config['speak'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Зевать
$config['gape'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Смеяться
$config['laugh'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Петь
$config['sing'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Артикуляция и звукопроизношение
$config['articulation_and_sound'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Зрение
$config['sight'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Слух
$config['hearing'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Жевать
$config['chew'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Глотать твердую пищу
$config['swallow_solid_food'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Глотать жидкую пищу
$config['swallow_liquid_food'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Самостоятельно пить
$config['independently_drink'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Самостоятельно есть
$config['independently_there'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Стул
$config['stool'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);
//Контроль мочеиспускания
$config['bladder_control'] = array(
    'not_at_all'  => array(0, 'funtsionalnost_sphere'),
    'poorly'      => array(10, 'funtsionalnost_sphere'),
    'moderately'  => array(20, 'funtsionalnost_sphere'),
    'fairly_well' => array(30, 'funtsionalnost_sphere'),
    'well'        => array(40, 'funtsionalnost_sphere'),
);


//Реагировать на окружающую обстановку
$config['react_to_their_environment'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);

//Фиксировать взгляд на лицах, предметах
$config['stare'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Узнавать людей
$config['recognize_people'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Общаться  невербально
$config['communicate_nonverbally'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Общаться вербально
$config['communicate_verbally'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Произносить фразы
$config['say_phrases'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Понимать речь
$config['understand_speech'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Выполнять простые просьбы
$config['perform_simple_requests'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Выделять любимую игрушку
$config['allocate_favorite_toy'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Играть с другими
$config['play_with_other'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Самостоятельно играть
$config['independently_play'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Следовать социальным нормам
$config['follow_social_norms'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Концентрироваться на поставленном задании
$config['concentrate_on_tasks'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Слушать истории и сказки
$config['listen_to_storiess'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Читать
$config['read'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Писать
$config['write'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Адекватно эмоционально реагировать
$config['adequately_respond_emotionally'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Выполнять сложные задачи
$config['perform_complex_tasks'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Заниматься самостоятельно
$config['tutor'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Работать в группе
$config['work_in_group'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Глазной контакт
$config['eye_contact'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Указательный жест
$config['pointing_gesture'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Вести себя тихо, когда просят
$config['to_be_quiet_for_request'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);
//Концентрироваться на предмете разговора
$config['concentrate_on_conversation'] = array(
    'not_at_all'  => array(0, 'cognitive'),
    'poorly'      => array(10, 'cognitive'),
    'moderately'  => array(20, 'cognitive'),
    'fairly_well' => array(30, 'cognitive'),
    'well'        => array(40, 'cognitive'),
);



//Аппетит
$config['appetite']  = array(
    'poorly'      => array(0, 'physiological_services'),
    'moderately'  => array(10, 'physiological_services'),
    'fairly_well' => array(20, 'physiological_services'),
    'well'        => array(30, 'physiological_services'),
);
//Переваривание пищи
$config['digestion'] = array(
    'poorly'      => array(0, 'physiological_services'),
    'moderately'  => array(10, 'physiological_services'),
    'fairly_well' => array(20, 'physiological_services'),
    'well'        => array(30, 'physiological_services'),
);
//Опорожнение кишечника
$config['bowel_movement'] = array(
    'poorly'      => array(0, 'physiological_services'),
    'moderately'  => array(10, 'physiological_services'),
    'fairly_well' => array(20, 'physiological_services'),
    'well'        => array(30, 'physiological_services'),
);
//Дыхание
$config['breathing'] = array(
    'poorly'      => array(0, 'physiological_services'),
    'moderately'  => array(10, 'physiological_services'),
    'fairly_well' => array(20, 'physiological_services'),
    'well'        => array(30, 'physiological_services'),
);
//Сон
$config['dream'] = array(
    'poorly'      => array(0, 'physiological_services'),
    'moderately'  => array(10, 'physiological_services'),
    'fairly_well' => array(20, 'physiological_services'),
    'well'        => array(30, 'physiological_services'),
);



//Судороги
$config['convulsions'] = array(-10, 'disease');
//Бронхит
$config['bronchitis'] = array(-10, 'disease');
//ОРВИ
$config['sars'] = array(-10, 'disease');
//Аллергия
$config['allergy'] = array(-10, 'disease');
//Бронхиальная астма
$config['bronchial_asthma'] = array(-10, 'disease');
//Кожные заболевания
$config['skin_diseases'] = array(-10, 'disease');
//Кишечные колики
$config['intestinal_colic'] = array(-10, 'disease');
//Другое
$config['other'] = array(-10, 'disease');