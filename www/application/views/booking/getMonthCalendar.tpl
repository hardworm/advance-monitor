{if $GET.calendar_id > 0}
	{counter name='i' start=0 print=FALSE assign='i'}
    
    {foreach $arrayMonth as $daynum => $daydata}
		{if $i == 0}
            {for $j=$startDay to ($daydata["dayofweek"]-1)}
                {if $j == $lastWeekDay}
                    <div class="day_container day_grey" style="margin-right: 0px;"><a style="background-color:{$settingObj.day_grey_bg}"></a></div>
                {else}
					<div class="day_container day_grey"><a style="background-color:{$settingObj.day_grey_bg}"></a></div>
				{/if}
            {/for}
		{/if}
		
		{$background = "day_white"}
		{$background_color = $setting.day_white_bg}
		{$newstyle=''}
        {$newstyle1 = 'style="color:+"'|replace:'+':$setting.day_white_line1_color}
		{$newstyle2 = 'style="color:+"'|replace:'+':$setting.day_white_line2_color}
		{$over=1}
        
        {*  FixME      *}
        {*{if $daydata['Ymd'] < $bookfromdate || $numslots.$daynum == -1 || $daydata['Ymd'] > $bookfromdate}
			{$background_color = $setting.day_white_bg_disabled}
			{$newstyle='style="color:#CCC"'}
			{$newstyle1 = 'style="color:+"'|replace:'+':$setting.day_white_line1_disabled_color}
			{$newstyle2 = 'style="color:+"'|replace:'+':$setting.day_white_line2_disabled_color}
			{$over=0}
        {/if}*}
        
        {if $numslots.$daynum == 0 && $daydata['Ymd'] >= $smarty.now|date_format:"%Y%m%d"}
			{$background="day_red"}
			{$background_color = $setting.day_red_bg}
			{$newstyle1 = 'style="color:+"'|replace:'+':$setting.day_red_line1_color}
			{$newstyle2 = 'style="color:+"'|replace:'+':$setting.day_red_line2_color}
		{elseif $daydata['Ymd'] == $smarty.now|date_format:"%Y%m%d"}
			{$background="day_black"}
			{$background_color = $setting.day_black_bg}
			{$newstyle1 = 'style="color:+"'|replace:'+':$setting.day_black_line1_color}
			{$newstyle2 = 'style="color:+"'|replace:'+':$setting.day_black_line2_color}
		{elseif $numslots.$daynum == -1}
			{$background="day_white"}
			{$background_color = $setting.day_white_bg_disabled}
			{$newstyle1 = 'style="color:+"'|replace:'+':$setting.day_white_line1_disabled_color}
			{$newstyle2 = 'style="color:+"'|replace:'+':$setting.day_white_line2_disabled_color}
        {/if}

        {if $daydata["dayofweek"] == $lastWeekDay}
			<div class="day_container {$background}" style="margin-right: 0px;"><a style="cursor:pointer;background-color:{$background_color}"  year="{$GET["year"]}" month="{$GET["month"]}" day="{$daynum}" popup="{$slots_popup_enabled}" over="{$over}">
				<div class="day_number" {$newstyle1}>{$daynum}</div>
                <div class="day_book" {$newstyle1}>
                    {if $numslots.$daynum > 0 AND $daydata['Ymd'] >= $bookfromdate AND $daydata['Ymd'] <= $booktodate}
						{$Labels.GETMONTHCALENDAR_BOOK_NOW}
					{elseif $numslots.$daynum == 0}
						{$Labels.GETMONTHCALENDAR_SOLDOUT}
					{/if}
				</div>
                <div class="cleardiv"></div>
				<div class="day_slots" {$newstyle2}>
                    {if $numslots.$daynum > 0}
						{$Labels.GETMONTHCALENDAR_SLOTS_AVAILABLE}{": "}{$numslots[$daynum]}
					{else}
						{$Labels.GETMONTHCALENDAR_NO_SLOTS_AVAILABLE}
					{/if}
				</div>
			</a></div>
        {else}
			<div class="day_container {$background}"><a style="cursor:pointer;background-color:{$background_color}" year="{$GET["year"]}" month="{$GET["month"]}" day="{$daynum}" popup="{$slots_popup_enabled}" over="{$over}">
                <div class="day_number" {$newstyle1}>{$daynum}</div>
                <div class="day_book" {$newstyle1}>
					{if $numslots.$daynum > 0 AND $daydata["Ymd"] >= $bookfromdate AND $daydata["Ymd"] <= $booktodate}
						{$Labels.GETMONTHCALENDAR_BOOK_NOW}
					{elseif $numslots.$daynum == 0}
						{$Labels.GETMONTHCALENDAR_SOLDOUT}
					{/if}
				</div>
                <div class="cleardiv"></div>
				<div class="day_slots" {$newstyle2}>
                    {if $numslots.$daynum > 0} 
                        {$Labels.GETMONTHCALENDAR_SLOTS_AVAILABLE}{": "}{$numslots[$daynum]}
					{else}
						{$Labels.GETMONTHCALENDAR_NO_SLOTS_AVAILABLE}
					{/if}
				</div>
			</a></div>
		{/if}
		
        {counter name='i' print=FALSE}
        {if $i == count($arrayMonth)}
			{$lastDay=$daydata["dayofweek"]}
        {/if}
    {/foreach}
{*	for($j=$lastWeekDay;$j>$lastDay;$j--) {*}
    {for $j=$lastDay to ($lastWeekDay-1)}
        {if $j == ($lastDay+1)}
			<div class="day_container day_grey" style="margin-right: 0px;"><a style="background-color:{$setting.day_grey_bg}"></a></div>
		{else}
			<div class="day_container day_grey"><a style="background-color:{$setting.day_grey_bg}"></a></div>
		{/if}
	{/for}
	<script>
		$(function() {
			$('#month_nav_prev').html("<a href=\"javascript:getPreviousMonth({$calendar_id},'{$GET["publickey"]}',{$setting.calendar_month_limit_past});\" class=\"month_nav_button month_navigation_button_custom\"><img src=\"{ci helper="url" function="base_url"}dist/images/prev.png\" /></a>");
			$('#month_nav_next').html("<a href=\"javascript:getNextMonth({$calendar_id},'{$GET["publickey"]}',{$setting.calendar_month_limit_future});\" class=\"month_nav_button month_navigation_button_custom\"><img src=\"{ci helper="url" function="base_url"}dist/images/next.png\" /></a>");
		});
	</script>
{else}
    {counter name=k start=0 print=FALSE assign='i'}
    {foreach $arrayMonth as $daynum => $daydata}
        {if $i == 0}
            {for $j=$startDay to ($daydata["dayofweek"]-1)}
                {if $j == $lastWeekDay}
                    <div class="day_container day_grey" style="margin-right: 0px;"><a style="background-color:{$setting.day_grey_bg};"></a></div>
                {else}			
                    <div class="day_container day_grey"><a style="background-color:{$setting.day_grey_bg}"></a></div>
                {/if}
            {/for}
        {/if}
		
        {$background = "day_white"}
        {$background_color = $setting.day_white_bg_disabled}
        {$newstyle=''}
        {$newstyle1=''}
        {$newstyle2=''}
        {$over=0}
        {if $daydata["dayofweek"] == $lastWeekDay}
            <div class="day_container {$background}" style="margin-right: 0px;"><a style="cursor:pointer;background-color:{$background_color}" year="{$GET["year"]}" month="{$GET["month"]}" day="{$daynum}" popup="{$slots_popup_enabled}" over="{$over}">
                <div class="day_number" {$newstyle}>{$daynum}</div>
                <div class="day_slots" {$newstyle}>
                </div>
                <div class="day_book">
                </div>
            </a></div>
        {else}
            <div class="day_container {$background}"><a style="cursor:pointer;background-color:{$background_color}" year="{$GET["year"]}" month="{$GET["month"]}" day="{$daynum}" over="{$over}">
                <div class="day_number" {$newstyle}>{$daynum}</div>
                <div class="day_slots" {$newstyle}>
                </div>
                <div class="day_book">
                </div>
            </a></div>
        {/if}
        {counter name=k print=FALSE}
        {if $i == count($arrayMonth)}
            {$lastDay=$daydata["dayofweek"]}
        {/if}
    {/foreach}
    {for $j=$lastDay to ($lastWeekDay-1)}
        {if $j == ($lastDay+1)}
            <div class="day_container day_grey" style="margin-right: 0px;"><a style="background-color:{$setting.day_grey_bg}"></a></div>
        {else}
            <div class="day_container day_grey"><a style="background-color:{$setting.day_grey_bg}"></a></div>
        {/if}
    {/for}
    <script>
        $(function() {
            $('#month_nav_prev').html("<a href=\"javascript:getPreviousMonth('{$calendar_id}','{$GET["publickey"]}',{$setting.calendar_month_limit_past});\" class=\"month_nav_button month_navigation_button_custom\"><img src=\"{ci helper="url" function="base_url"}dist/images/prev.png\" /></a></a>");
            $('#month_nav_next').html("<a href=\"javascript:getNextMonth('{$calendar_id}','{$GET["publickey"]}',{$setting.calendar_month_limit_future});\" class=\"month_nav_button month_navigation_button_custom\"><img src=\"{ci helper="url" function="base_url"}dist/images/next.png\" /></a></a>");
        });
    </script>
{/if}