{$weekdays[0] = $Labels.SUNDAY}
{$weekdays[1] = $Labels.MONDAY}
{$weekdays[2] = $Labels.TUESDAY}
{$weekdays[3] = $Labels.WEDNESDAY}
{$weekdays[4] = $Labels.THURSDAY}
{$weekdays[5] = $Labels.FRIDAY}
{$weekdays[6] = $Labels.SATURDAY}

{$maxColumn = 0}
{foreach $arraySlots as $slot}
	{if trim($slot["slot_special_text"])!=''}
		{$maxColumn = 1}
	{/if}
{/foreach}
{$columns=ceil(count($arraySlots)/6)}

{*max number columns is 9, so if there are too many slots, have to add lines instead of columns*}
{if $setting.paypal_display_price == 1 && $setting.slots_unlimited != 2 && $maxColumn == 0}
	{$maxColumn = 2}
{elseif $setting.slots_unlimited == 2 && $maxColumn == 0}
	{$maxColumn=1}
{elseif $maxColumn == 0}
	{if $setting.time_format == "12"}
		{$maxColumn = 3}
	{else}
		{$maxColumn = 4}
	{/if}
{/if}

{$lines=6}
{if $columns > $maxColumn}
	{$columns=$maxColumn}
	{$lines=7}
	{*do {
		$lines++;
	} while(ceil(count($arraySlots)/$lines)>$maxColumn && $lines < 13);*}
    {while ceil(count($arraySlots)/$lines)>$maxColumn && $lines < 13}
        {$lines++}
    {/while}
{/if}

{counter name='totCols' start=0 print=FALSE assign='totCols'}
{counter name='page' start=1 print=FALSE assign='page'}
{*get the next and prev dates with available slots*}
{*first I check if there are slots in the future*}
{if $prev == '' || $GET.month > $prevMonth || date("Ymd",$prev) < $bookfromdate}
    <div class="prev_day" style="float:left;color:#CCC;margin-right:20px">{$Labels.GETBOOKINGFORM_PREV_DAY}</div>
{else}
	<div class="prev_day" style="float:left;margin-right:20px"><a href="javascript:getBookingForm({$prevYear},{$prevMonth},{$prevDay}, {$GET["calendar_id"]},'{$setting.recaptcha_public_key}');" style="color:#000">{$Labels.GETBOOKINGFORM_PREV_DAY}</a></div>
{/if}
{*//only current month is navigable, so I stop navigation at the end of month*}
{if $next == '' || $GET["month"] < $nextMonth || date("Ymd",$next) > $booktodate}
    <div class="next_day" style="float:left;color:#CCC">{$Labels.GETBOOKINGFORM_NEXT_DAY}</div>
{else}  
	<div class="next_day" style="float:left"><a href="javascript:getBookingForm({$nextYear},{$nextMonth},{$nextDay}, {$GET["calendar_id"]},'{$setting.recaptcha_public_key}');" style="color:#000">{$Labels.GETBOOKINGFORM_NEXT_DAY}</a></div>
{/if}
{*close*}
<div class="font_custom close_booking"><a href="javascript:closeBooking({$calendar_id},'{$setting.recaptcha_public_key}',{$GET["year"]},{$GET["month"]});">{$Labels.GETBOOKINGFORM_CLOSE}&nbsp; X</a></div>
<div class="cleardiv"></div>
<script type="text/javascript">
    {if $setting.slot_selection == "1"}
		$(function() {
			$('#booking_slots').find('input').each(function() {
				  $(this).click(function() {
					  $('#booking_slots').find('input').removeAttr('checked');
					  $(this).attr("checked","checked");
				  });
			  });
		});
    {/if}
</script>
{*leftside*}
<div class="booking_left">
{*    title*}
    <div class="font_custom booking_title"><span id="booking_day">{$GET["day"]}{" "}{$weekdays.$wd}</span> - <span style="color:{$setting.form_calendar_name_color};" id="calendar_name">{$calendar_title}</span><span style="float:right;width:30px;cursor:pointer" id="next">&nbsp;</span><span style="float:right;width:30px;cursor:pointer" id="prev">&nbsp;</span></div>
    <div class="cleardiv"></div>
	{if $setting.form_text !=''}
        <div class="form_text">{$setting.form_text}</div>
    {/if}
{*    slots available*}
    <div class="booking_slots_container" id="booking_slots">
        <div id="slideshow">
            <div id="page{$page}">
				{$onclick=""}
				{if $setting.paypal == 1 && $setting.paypal_account !='' && $setting.paypal_currency !='' && $setting.paypal_locale != ''}
					{$onclick="javascript:addToPaypalForm();"}
				{/if}
                 <div class="booking_slots_column">
                    {counter name='z' start=1 print=FALSE assign='z'}
                    {foreach $arraySlots as $slotId => $slot}
						{$disabled = ""}
						{if isset($slot["slot_av"]) && $slot["slot_av"] == 0 && $setting.slots_unlimited == 2}
							{$disabled = "disabled"}
						{/if}
						{if $slot["booked"] == 1}
							{$disabled="disabled"}
						{/if}
                        <div class="booking_slots_row">
					    {if $slot["booked"] == 1}
							<div class="booked_slot">
						{/if}
                        <div class="booking_check"><input type="checkbox" name="reservation_slot[]" value="{$slotId}" tmt:minchecked="1" tmt:message="{$Labels.GETBOOKINGFORM_SLOT_ALERT}" {$disabled} onclick="{$onclick}"/></div>
                        <div class="booking_slot">
                        {if $slot["slot_special_mode"] == 1}
                            {if $setting.time_format == "12"}
                                {strtotime($slot["slot_time_from"])|date_format:"%h:%i %a"}{" - "}{strtotime($slot["slot_time_to"])|date_format:"%h:%i %a"}
                            {else}
                                {$slot["slot_time_from"]|substr:0:5}{" - "}{$slot["slot_time_to"]|substr:0:5}
                            {/if}
					    {elseif $slot["slot_special_mode"] == 0 && $slot["slot_special_text"] != ''}
{*                            //echo $slot["slot_special_text"]; *}
                        {else}
                            {if $settingObj->getTimeFormat() == "12"}
                                {strtotime($slot["slot_time_from"])|date_format:"%h:%i %a"}{" - "}{strtotime($slot["slot_time_to"])|date_format:"%h:%i %a"}
                            {else}
                                {$slot["slot_time_from"]|substr:0:5}{" - "}{$slot["slot_time_to"]|substr:0:5}
                            {/if}
                        {/if}
                        </div>
                        {* //add seats num if set*}
						{if $setting.slots_unlimited == 2}
                            <div class="booking_seats">
                            	{$Labels.SELECT_SEATS}:&nbsp;
                                <select name="reservation_seats_{$slotId}" id="seats_{$slotId}" onchange="{$onclick}" style="width: 40px;" {$disabled}>
									{for $u=1 to $slot["slot_av_max"]}
                                        <option value="{$u}">{$u}</option>
									{/for}
                                </select>
                            </div>
                        {/if}
                        
						{if $setting.paypal_display_price == 1}
                            <div class="booking_price">
								{if $slot["slot_price"]>0}
									{$slot["slot_price"]}&nbsp;{$setting.paypal_currency}
								{else}
									{$Labels.FREE}
								{/if}
                            </div>
                        {/if}
						
						{if $slot["slot_special_text"] != '' && ($slot["slot_special_mode"] == 1 || $slot["slot_special_mode"] == 0)}
                            <div class="booking_text">
                                {$slot["slot_special_text"]}
                            </div>
						{/if}
                            
						{if $slot["booked"] == 1}
							</div>
						{/if}
                        
                        <div class="cleardiv"></div>
                    </div>
                        {if $z % $lines == 0}
                           {counter name='totCols' print=FALSE}
                            </div>
                            {$display=""}
                            {if $totCols % $maxColumn == 0 && $z < count($arraySlots)}
                                {counter name='page' print=FALSE}  
                                </div>
                                <div id="page{$page}">
                            {/if}
                            <div class="booking_slots_column">
                        {/if}
                        {counter name='z' print=FALSE}
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(function() {
		$('#calendar_id').val({$calendar_id});
        {if $page > 1}
		  var slider = $('#slideshow').bxSlider({
			infiniteLoop: false,
			controls: false,
			onAfterSlide: function(currentSlideNumber, totalSlideQty, currentSlideHtmlObject){
							$('#prev').html('<a href="#"></a>');
							 $('#next').html('<a href="#"></a>');
						  if(currentSlideNumber+1 == totalSlideQty) {
							  $('#next').html("");
							  $('#prev').html('<a href="#"></a>');
						  }
						  if(currentSlideNumber == 0) {
							  $('#prev').html('');
							  $('#next').html('<a href="#"></a>');
						  } 
						}
		  });
		  $('#prev').click(function(){
			slider.goToPreviousSlide();
			return false;
		  });
		
		  $('#next').click(function(){
			slider.goToNextSlide();
			return false;
		  });
        {/if}
	});
</script>
|
{if $page>1}
	{"1"}
{else}
	{"0"}
{/if}
|{$Labels.GETBOOKINGFORM_CAPTCHA_ALERT}