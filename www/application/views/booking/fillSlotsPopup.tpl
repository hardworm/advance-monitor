{$styleAttr = 'style="margin-left: 20px;"'}
{counter name='totCols' start=0 print=FALSE assign='totCols'}
<div class="box_preview_column" {$styleAttr}>
    {counter name='z' start=1 print=FALSE assign='z'}
    {if count($arraySlots) == 1}
        <div class="slot_special_container">
            {foreach $arraySlots as $slotId => $slot}
                {if $slot["booked"] == 1}
                    <span class="booked_slot">
                    {/if}
                    {if $slot["slot_special_mode"] == 1}
                        {if $setting.time_format == "12"}
                            {strtotime($slot["slot_time_from"])|date_format:"%h:%i %a"}{" - "}{strtotime($slot["slot_time_to"])|date_format:"%h:%i %a"}
                        {else}
                            {$slot["slot_time_from"]|substr:0:5}{" - "}{$slot["slot_time_to"]|substr:0:5}
                        {/if}
                        {if $slot["slot_special_text"] != ''}
                            {" - "}{$slot["slot_special_text"]} 
                        {/if}
                    {elseif $slot["slot_special_mode"] == 0 && $slot["slot_special_text"] != ''}
                        {$slot["slot_special_text"]} 
                    {else}
                        {if $setting.time_format == "12"}
                            {strtotime($slot["slot_time_from"])|date_format:"%h:%i %a"}{" - "}{strtotime($slot["slot_time_to"])|date_format:"%h:%i %a"}
                        {else}
                            {$slot["slot_time_from"]|substr:0:5}{" - "}{$slot["slot_time_to"]|substr:0:5}
                        {/if}
                    {/if}
                    {if $slot["booked"] == 1}
                    </span>
                {/if}
            {/foreach}
        </div>
    {else}
        {foreach $arraySlots as $slotId => $slot}
            <div class="box_preview_row">
                {if $slot["booked"] == 1}
                    <span class="booked_slot">
                    {/if}
                    {if $slot["slot_special_mode"] == 1}	
                        {if $setting.time_format == "12"}
                            {strtotime($slot["slot_time_from"])|date_format:"%h:%i %a"}{" - "}{strtotime($slot["slot_time_to"])|date_format:"%h:%i %a"}{" "}{$slot["slot_special_text"]}
                        {else}
                            {$slot["slot_time_from"]|substr:0:5}{" - "}{$slot["slot_time_to"]|substr:0:5}{" "}{$slot["slot_special_text"]}
                        {/if}
                    {elseif $slot["slot_special_mode"] == 0 && $slot["slot_special_text"] != ''}
                        {$slot["slot_special_text"]} 
                    {else}
                        {if $setting.time_format == "12"}
                            {strtotime($slot["slot_time_from"])|date_format:"%h:%i %a"}{" - "}{strtotime($slot["slot_time_to"])|date_format:"%h:%i %a"}
                        {else}
                            {$slot["slot_time_from"]|substr:0:5}{" - "}{$slot["slot_time_to"]|substr:0:5}
                        {/if}
                    {/if}
                    {if $slot["booked"] == 1}
                    </span>
                {/if}
            </div>
            {if ($z % $lines) == 0}
                {counter name='totCols' print=FALSE}
                {if $totCols == ($columns-1)}
                    {$styleAttr='style="margin-right: 20px;"'}
                {/if}
            </div>
            <div class="box_preview_column" {$styleAttr}>
            {/if}
            {counter name='z' print=FALSE}
        {/foreach}
    {/if}
</div>
|
{$calendar_title}