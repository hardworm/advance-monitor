{* Extend our master template *}
{extends file="auth_main.tpl"}

{* This block is defined in the auth_main.tpl template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the auth_main.tpl template *}
{block name=main}
{if $message != ""}
	<div class="col-sm-8 col-sm-offset-2 alert alert-danger alert-dismissible fade in fs14px" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {$message}
    </div>
{/if}

<div class="col-sm-10 col-sm-offset-2 create-form-t">
        <h1>{ci helper='language' function='lang' param1="forgot_password_heading"}</h1>
        <p>{ci helper='language' function='lang' param1="forgot_password_subheading"}</p>
	</div>

	<div class="create-form col-sm-8 col-sm-offset-2">
		<div class="row">
		<div class="col-sm-6">
    {ci helper='form' function='form_open' param1='auth/forgot_password' param2='class="form-signin"'}
		<div class="form-group">
        <label class="sr-only" for="email">Email:</label>
        {ci helper='form' function='form_input' param1=$email}
		</div>
		<div class="form-group">
{*        {ci helper='form' function='form_submit' param1="Submit" param2='Отправить' param3='class="btn btn-primary btn-block"'}*}
            <input type="submit" name="Submit" value="{ci helper='language' function='lang' param1="restore_password"}" class="btn btn-primary btn-block" />
		</div>
		<div class="form-group">
        <a href="login">Login</a>
		</div>
    </form>
	</div>
	</div>
	</div>
{/block}