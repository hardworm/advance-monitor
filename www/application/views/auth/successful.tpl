{* Extend our master template *}
{extends file="auth_main.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}



{* This block is defined in the master.php template *}
{block name=main}
    <div class="container">
        <div class="jumbotron">
            <p>{ci helper='language' function='lang' param1="register_confirm"}</p>
        </div>
    </div>
{/block}