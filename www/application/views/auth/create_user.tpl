{* Extend our master template *}
{extends file="auth_main.tpl"}

{* This block is defined in the auth_main.tpl template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the auth_main.tpl template *}
{block name=main}

{if $message != ""}
	<div class="col-sm-8 col-sm-offset-2 alert alert-danger alert-dismissible fade in fs14px" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {$message}
    </div>
{/if}
<div class="col-sm-10 col-sm-offset-2 create-form-t">
    <h1>{ci helper='language' function='lang' param1="h1"}</h1>
    <p>{ci helper='language' function='lang' param1="h1p"}</p>
</div>
		
<div class="create-form col-sm-8 col-sm-offset-2">
    <div class="row">
        <div class="col-sm-6">
            {ci helper='form' function='form_open' param1='auth/create_user' param2='class="form-signin"'}
            <div class="form-group">
                <label class="sr-only" for="first_name">First Name:</label>
                {ci helper='form' function='form_input' param1=$first_name}
            </div>
            <div class="form-group">
                <label class="sr-only" for="last_name">Last Name:</label>
                {ci helper='form' function='form_input' param1=$last_name}     
            </div>
            <div class="form-group">
                <label class="sr-only" for="DOB">DOB:</label>
                {ci helper='form' function='form_input' param1=$DOB}
            </div>
            <div class="form-group">
                <label class="radio-inline" for="is_male1">{ci helper='form' function='form_input' param1=$is_male1} <i>{ci helper='language' function='lang' param1="male"}</i></label>
                <label class="radio-inline" for="is_male2">{ci helper='form' function='form_input' param1=$is_male2} <i>{ci helper='language' function='lang' param1="female"}</i></label>
            </div>
            <div class="form-group">
                <label class="sr-only" for="email">Email:</label>
                {ci helper='form' function='form_input' param1=$email} 
            </div>
            <div class="form-group">
                <label class="sr-only" for="password">Password:</label>
                {ci helper='form' function='form_input' param1=$password} 
            </div>
            <div class="form-group">
                <label class="sr-only" for="password_confirm">Confirm Password:</label>
                {ci helper='form' function='form_input' param1=$password_confirm}
            </div>
			
			<div class="panel panel-default">
			  <div class="panel-body pl-10 pr-10 pt-10 pb-10">
				<div class="col-xs-8 pl-0 fs14px">{ci helper='language' function='lang' param1="zaniatia"}</div>
				<div class="col-xs-4 text-right pr-0 pt-10">
                <label class="radio-inline" for="zaniatia1"><input type="radio" name="zaniatia" value="1" id="zaniatia1" class="grey" placeholder="{ci helper='language' function='lang' param1="yes"}" required="" autofocus=""  /> <i>{ci helper='language' function='lang' param1="yes"}</i></label>
                <label class="radio-inline" for="zaniatia2"><input type="radio" name="zaniatia" value="0" id="zaniatia2" class="grey" placeholder="{ci helper='language' function='lang' param1="no"}" required="" autofocus=""  /> <i>{ci helper='language' function='lang' param1="no"}</i></label>
				</div>
			  </div>
			</div>
			
			<div class="panel panel-default">
			  <div class="panel-body pl-10 pr-10 pt-10 pb-10">
				<div class="col-xs-8 pl-0 fs14px">{ci helper='language' function='lang' param1="reabilitatcia"}</div>
				<div class="col-xs-4 text-right pr-0 pt-10">
                <label class="radio-inline" for="reabilitatcia1"><input type="radio" name="reabilitatcia" value="1" id="reabilitatcia1" class="grey" placeholder="{ci helper='language' function='lang' param1="yes"}" required="" autofocus=""  /> <i>{ci helper='language' function='lang' param1="yes"}</i></label>
                <label class="radio-inline" for="reabilitatcia2"><input type="radio" name="reabilitatcia" value="0" id="reabilitatcia2" class="grey" placeholder="{ci helper='language' function='lang' param1="no"}" required="" autofocus=""  /> <i>{ci helper='language' function='lang' param1="no"}</i></label>
				</div>
			  </div>
			</div>
			
            <div class="form-group mt-40 mb-50">
{*                {ci helper='form' function='form_submit' param1="submit" param2='Регистрация' param3='class="btn btn-primary btn-block sndf"'}*}
				<input type="submit" name="submit" value="{ci helper='language' function='lang' param1="register"}" class="btn btn-primary btn-block sndf" />
            </div>

            </form>
        </div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="{ci helper="url" function="base_url"}js/plugin/PickMeUp/css/pickmeup.min.css">
<script type="text/javascript" src="{ci helper="url" function="base_url"}js/plugin/PickMeUp/js/jquery.pickmeup.min.js"></script> 
<script>
    $('#DOB').pickmeup({ format  : 'Y-m-d' });
	$(document).ready(function() {	
	$('.form-signin').on('submit', function () {
		//event.preventDefault();
		var zn1 = $('#zaniatia1').prop('checked');
		var zn2 = $('#zaniatia2').prop('checked');
		var rb1 = $('#reabilitatcia1').prop('checked');
		var rb2 = $('#reabilitatcia2').prop('checked');
		console.log(zn1+'|'+zn2+'|'+rb1+'|'+rb2);
		
		if (zn1 == true && rb1 == true) {
		var form_data = $('.form-signin').serialize();
			//$('.form-signin').submit();
			console.log(form_data);
		} else if (zn1 == true && rb2 == true) {
			alert('{ci helper='language' function='lang' param1="confirm"}');
			return false;
		} else if (zn2 == true && rb1 == true) {
			alert('{ci helper='language' function='lang' param1="confirm"}');
			return false;
		} else if (zn2 == true && rb2 == true) {
			alert('{ci helper='language' function='lang' param1="confirm"}');
			return false;
		} else if (zn1 == false && rb1 == false) {
			alert('{ci helper='language' function='lang' param1="confirm"}');
			return false;
		}
		});
		
	});
</script>
{/block}