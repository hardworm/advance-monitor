{* Extend our master template *}
{extends file="auth_main.tpl"}

{* This block is defined in the auth_main.tpl template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the auth_main.tpl template *}
{block name=main}
    {if $message != ""}
    <div class="col-sm-8 col-sm-offset-2 alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {$message}
    </div>
    {/if}
		<div class="col-sm-10 col-sm-offset-2 create-form-t">
        <h1>{ci helper='language' function='lang' param1='reset_password_heading'}</h1>
        {if $message != ""}
        <p id="infoMessage">{$message}</p>
        {/if}
		</div>
		
		<div class="create-form col-sm-8 col-sm-offset-2">
    
		<div class="row">
		<div class="col-sm-6">
		{ci helper='form' function='form_open' param1="auth/reset_password/$code" param2='class="form-signin"'}
		<div class="form-group">
        <label for="new_password">{ci helper='language' function='lang' param1='reset_password_new_password_label'}</label>
        {ci helper='form' function='form_input' param1=$new_password}
        </div>
		<div class="form-group">
        <label for="email">{ci helper='language' function='lang' param1='reset_password_new_password_confirm_label' param2='new_password_confirm'}</label>
        {ci helper='form' function='form_input' param1=$new_password_confirm}
        </div>
		<div class="form-group">
        {ci helper='form' function='form_input' param1=$user_id}
        {ci helper='form' function='form_hidden' param1=$csrf}
		</div>
		<div class="form-group">
        {ci helper='form' function='form_submit' 
             param1="Submit" param2="{ci helper='language' function='lang' param1='reset_password_submit_btn'}" 
             param3='class="btn btn-primary btn-block"'}
	</div>

    </form>
	</div>
	</div>
	</div>
{/block}