{* Extend our master template *}
{extends file="auth_main.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}



{* This block is defined in the master.php template *}
{block name=main}
    {if $message != ""}
    <div class="col-sm-8 col-sm-offset-2 alert alert-warning alert-dismissible fade in" role="alert" style="position:absolute;">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {$message}
    </div>
    {/if}
    <script src="/dist/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	
	<div class="auth-form col-sm-8 col-sm-offset-2">

        <div class="a-center mb-45"><img class="img-responsive mc" src="/dist/images/logo.png" alt="" /></div>
		<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
        {ci helper='form' function='form_open' param1='auth/login' param2='class="form-signin"'}
		<div class="form-group">
		<label for="identity" class="sr-only">Login</label>
        {ci helper='form' function='form_input' param1=$identity}
		</div>
		<div class="form-group mt-25">
        <label for="inputPassword" class="sr-only">Password</label>
        {ci helper='form' function='form_input' param1=$password}
		</div>
        <div id="LnG" class="checkbox mb-20 mt-20">
            <label class="checkbox-inline mr-50">
                {ci helper='form' function='form_checkbox' param1="remember" param2="1" param3=FALSE param4='id="remember" class="green"'}{ci helper='language' function='lang' param1="login_remember_label"}
            </label>
			<label class="radio-inline" for="langR"><input type="radio" name="lang" value="ru-RU" id="langR" class="grey" placeholder="RUS" autofocus=""  /> <i data-lang="russian">RUS</i></label>
            <label class="radio-inline" for="langE"><input type="radio" name="lang" value="en-EN" id="langE" class="grey" placeholder="EN"  autofocus=""  /> <i data-lang="english">EN &nbsp;</i></label>
        </div>
        <input type="submit" name="Sign in" value="{ci helper='language' function='lang' param1="login_heading"}" class="btn btn-primary btn-block" />
        <p class="row mt-15"><a class="col-xs-6 a-left"  href="create_user">{ci helper='language' function='lang' param1="register"}</a> <a class="col-xs-6 a-right" href="forgot_password">{ci helper='language' function='lang' param1="restore_password"}</a></p>
    </form>
	</div>
	</div></div>
{/block}