<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <title>Clip-One - Responsive Admin Template</title>
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="stylesheet" href="<?php echo base_url("/dist/css/bootstrap.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/font-awesome/css/font-awesome.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/fonts/style.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/css/main.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/css/main-responsive.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/iCheck/skins/all.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/css/theme_light.css"); ?>" type="text/css" id="skin_color">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/css/print.css"); ?>" type="text/css" media="print"/>
        <!--[if IE 7]>
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/font-awesome/css/font-awesome-ie7.min.css"); ?>">
        <![endif]-->
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("/dist/assets/plugins/select2/select2.css"); ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/DataTables/media/css/DT_bootstrap.css"); ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/dist/css/style.css"); ?>" />
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <body>
        <div class="main-container">
		<nav class="navbar navbar-inverse navbar-fixed-top" id="amTn" role="navigation">
            <div class="container-fluid a-center">
                <a href="/"><img src="/dist/images/logo2.png" alt="Advance MONITOR"></a>
            </div>
        </nav>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                            <h1 class="page-header"><?php echo lang('index_heading'); ?></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 cont-box">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i>
                                <?php echo lang('index_subheading'); ?>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                    </a>
                                    <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <a class="btn btn-xs btn-link panel-refresh" href="#">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                    <a class="btn btn-xs btn-link panel-expand" href="#">
                                        <i class="fa fa-resize-full"></i>
                                    </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th><?php echo lang('index_fname_th'); ?></th>
                                            <th><?php echo lang('index_lname_th'); ?></th>
                                            <th><?php echo lang('index_email_th'); ?></th>
                                            <th><?php echo lang('index_groups_th'); ?></th>
                                            <th><?php echo lang('index_status_th'); ?></th>
                                            <th><?php echo lang('index_action_th'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach ($users as $user): ?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td>
                                                    <?php foreach ($user->groups as $group): ?>
                                                        <?php echo anchor("auth/edit_group/" . $group->id, htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8')); ?><br />
                                                    <?php endforeach ?>
                                                </td>
                                                <td><?php echo ($user->active) ? anchor("auth/deactivate/" . $user->id, lang('index_active_link')) : anchor("auth/activate/" . $user->id, lang('index_inactive_link')); ?></td>
                                                <td><?php echo anchor("auth/edit_user/" . $user->id, 'Edit'); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table><br>
                                <button type="button" class="btn btn-default"><?php echo anchor('auth/create_user', lang('index_create_user_link')) ?></button> &nbsp; <button type="button" class="btn btn-default"><?php echo anchor('auth/create_group', lang('index_create_group_link')) ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--[if lt IE 9]>
                <script src="<?php echo base_url("assets/plugins/respond.min.js"); ?>"></script>
                <script src="<?php echo base_url("assets/plugins/excanvas.min.js"); ?>"></script>
                <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                <![endif]-->
        <!--[if gte IE 9]><!-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <!--<![endif]-->
        <script src="<?php echo base_url("dist/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/bootstrap/js/bootstrap.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/blockUI/jquery.blockUI.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/iCheck/jquery.icheck.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/less/less-1.5.0.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/jquery-cookie/jquery.cookie.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/js/main.js"); ?>"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script type="text/javascript" src="<?php echo base_url("dist/assets/plugins/select2/select2.min.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("dist/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("dist/assets/plugins/DataTables/media/js/DT_bootstrap.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/js/table-data.js"); ?>"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
            jQuery(document).ready(function () {
                Main.init();
                TableData.init();
            });
        </script>
    </body>
    <!-- end: BODY -->
</html>