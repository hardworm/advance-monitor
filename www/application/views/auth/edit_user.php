<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <title></title>
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="stylesheet" href="<?php echo base_url("/dist/css/bootstrap.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/font-awesome/css/font-awesome.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/fonts/style.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/css/main.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/css/main-responsive.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/iCheck/skins/all.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/css/theme_light.css"); ?>" type="text/css" id="skin_color">
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/css/print.css"); ?>" type="text/css" media="print"/>
        <!--[if IE 7]>
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/font-awesome/css/font-awesome-ie7.min.css"); ?>">
        <![endif]-->
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("/dist/assets/plugins/select2/select2.css"); ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/dist/assets/plugins/DataTables/media/css/DT_bootstrap.css"); ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/dist/css/style.css"); ?>" />
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <body>
        <div class="main-container">
		<nav class="navbar navbar-inverse navbar-fixed-top" id="amTn" role="navigation">
            <div class="container-fluid a-center">
                <a href="/"><img src="/dist/images/logo2.png" alt="Advance MONITOR"></a>
            </div>
        </nav>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                            <h1 class="page-header"><?php echo lang('edit_user_heading');?></h1></div>
                </div>
				
				<div class="row">
                    <div class="col-md-12 cont-box">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><?php echo lang('edit_user_subheading');?>
								<div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                    </a>
                                    
                                    <a class="btn btn-xs btn-link panel-expand" href="#">
                                        <i class="fa fa-resize-full"></i>
                                    </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">

								<div id="infoMessage" class="mb-30"><?php echo $message;?></div>

<?php echo form_open(uri_string());?>
	<div class="form-group eus row">
      <div class="col-sm-6">
            <?php echo lang('edit_user_fname_label', 'first_name');?> 
            <?php echo form_input($first_name);?>
      </div>

      <div class="col-sm-6">
            <?php echo lang('edit_user_lname_label', 'last_name');?> 
            <?php echo form_input($last_name);?>
      </div>
	</div>
<!-- <div class="form-group eus row">
		<div class="col-sm-6">
            <?php // echo lang('edit_user_company_label', 'company');?>
            <?php // echo form_input($company);?>
		 </div>-->

<!--    <div class="col-sm-6">
            <?php // echo lang('edit_user_phone_label', 'phone');?> 
            <?php // echo form_input($phone);?>
		 </div>
	</div>-->
	
    <div class="form-group eus row">
		<div class="col-sm-6">
            <?php echo lang('edit_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
		</div>
		<div class="col-sm-6">
            <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
            <?php echo form_input($password_confirm);?>
		</div>
	</div>

      <?php if ($this->ion_auth->is_admin()): ?>

          <h2 class="a-center mt-30"><?php echo lang('edit_user_groups_heading');?></h2>
		  <p class="a-center mt-30 mb-30">
          <?php foreach ($groups as $group):?>
              <label class="checkbox-inline">
              <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
              <input type="checkbox" class="green" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
              <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
              </label>
          <?php endforeach?>
			</p>
      <?php endif ?>

      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

      <p id="deact" class="a-center mb-50"><?php echo form_submit('submit', lang('edit_user_submit_btn'));?></p>

<?php echo form_close();?>
</div>
				</div>
                </div>
				</div>
				
				


</div>
</div>
        <!--[if lt IE 9]>
                <script src="<?php echo base_url("assets/plugins/respond.min.js"); ?>"></script>
                <script src="<?php echo base_url("assets/plugins/excanvas.min.js"); ?>"></script>
                <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                <![endif]-->
        <!--[if gte IE 9]><!-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <!--<![endif]-->
        <script src="<?php echo base_url("dist/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/bootstrap/js/bootstrap.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/blockUI/jquery.blockUI.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/iCheck/jquery.icheck.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/less/less-1.5.0.min.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/jquery-cookie/jquery.cookie.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/js/main.js"); ?>"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script type="text/javascript" src="<?php echo base_url("dist/assets/plugins/select2/select2.min.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("dist/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("dist/assets/plugins/DataTables/media/js/DT_bootstrap.js"); ?>"></script>
        <script src="<?php echo base_url("dist/assets/js/table-data.js"); ?>"></script>
		<script src="/dist/assets/js/form-elements.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
            jQuery(document).ready(function () {
                Main.init();
                TableData.init();
				FormElements.init();
            });
        </script>
    </body>
    <!-- end: BODY -->
</html>
