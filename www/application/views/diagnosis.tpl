{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
    {if $isDiagnosis == FALSE}
	<div class="container-fluid page-header">
		<div class="col-sm-9 pl-0"><h1>{ci helper='language' function='lang' param1="diagnosis"}</h1></div>
		<div class="col-sm-3 a-right pr-0"><a class="btn btn-default" href="{ci helper="url" function="base_url"}diagnosis/add/" role="button"><i class="glyphicon glyphicon-plus"></i> {ci helper='language' function='lang' param1="diagnosis_add"}</a></div>
	</div>
    {else}
	<div class="container-fluid page-header">
		<div class="col-xs-12 pl-0"><h1>{ci helper='language' function='lang' param1="diagnosis"}</h1></div>
	</div>
	<div class="contBox">
        <p>{$diagnosis}</p>
	</div>
    {/if}
    
    <div id="chartIndivid" style="width: 900px; height: 500px;"></div>
    
{/block}


