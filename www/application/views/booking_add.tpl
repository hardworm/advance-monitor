{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
    
    <script language="javascript" type="text/javascript" src="{ci helper="url" function="base_url"}js/jquery.bxSlider.min.js"></script>
    <script language="javascript" type="text/javascript" src="{ci helper="url" function="base_url"}js/tmt_libs/tmt_core.js"></script>
    <script language="javascript" type="text/javascript" src="{ci helper="url" function="base_url"}js/tmt_libs/tmt_form.js"></script>
    <script language="javascript" type="text/javascript" src="{ci helper="url" function="base_url"}js/tmt_libs/tmt_validator.js"></script>
    <script language="javascript" type="text/javascript" src="{ci helper="url" function="base_url"}js/wach.calendar.min.js"></script>
    <script language="javascript" type="text/javascript" src="{ci helper="url" function="base_url"}js/lib.min.js"></script>
    
    
    <style>
	.month_navigation_button_custom {
		background-color:{$setting.month_navigation_button_bg};
	}
	.month_navigation_button_custom:hover {
		background-color:{$setting.month_navigation_button_bg_hover};
	}
	.month_container_custom {
		background-color:{$setting.month_container_bg};
	}
	.month_name_custom {
		color: {$setting.month_name_color};
	}
	.year_name_custom {
		color: {$setting.year_name_color};
	}
	.weekdays_custom {
		color: {$setting.day_names_color};
	}
	.field_input_custom {
		background-color: {$setting.field_input_bg};
		color: {$setting.field_input_color};
	}
	.book_now_custom {
		background-color: {$setting.book_now_button_bg};
		color: {$setting.book_now_button_color};
	}
	.book_now_custom:hover {
		background-color: {$setting.book_now_button_bg_hover};
		color: {$setting.book_now_button_color_hover};
	}
	.clear_custom {
		background-color: {$setting.clear_button_bg};
		color: {$setting.clear_button_color};
	}
	.clear_custom:hover {
		background-color: {$setting.clear_button_bg_hover};
		color: {$setting.clear_button_color_hover};
	}
    </style>
    
    <link rel="icon" href="favicon.ico" />
    <link rel="stylesheet" href="{ci helper="url" function="base_url"}dist/css/mainstyle.css" type="text/css" />
    
    <script language="javascript" type="text/javascript">
	var currentMonth;
	var currentYear;
	var pageX;
	var pageY;
	var today= new Date();
    {if $setting.show_first_filled_month == 0}
        var newday= new Date();
    {/if}
	
	{*} else {
		?>
		var newday = new Date(<?php echo $calendarObj->getFirstFilledMonth($calendarObj->getCalendarId()); ?>);
		<?php
	}*}
	
	var booking_day_white_bg = '{$setting.day_white_bg}';
	var booking_day_white_bg_hover = '{$setting.day_white_bg_hover}';
	var booking_day_black_bg = '{$setting.day_black_bg}';
	var booking_day_black_bg_hover = '{$setting.day_black_bg_hover}';
	var booking_day_white_line1_color = '{$setting.day_white_line1_color}';
	var booking_day_white_line1_color_hover = '{$setting.day_white_line1_color_hover}';
	var booking_day_white_line2_color = '{$setting.day_white_line2_color}';
	var booking_day_white_line2_color_hover = '{$setting.day_white_line2_color_hover}';
	var booking_day_black_line1_color = '{$setting.day_black_line1_color}';
	var booking_day_black_line1_color_hover = '{$setting.day_black_line1_color_hover}';
	var booking_day_black_line2_color = '{$setting.day_black_line2_color_hover}';
	var booking_day_black_line2_color_hover = '{$setting.day_black_line2_color_hover}';
	var booking_recaptcha_style = '{$setting.recaptcha_style}';
	
	$(function() {
		$('#back_today').fadeOut(0);
		getMonthCalendar((newday.getMonth()+1),newday.getFullYear(),'{$calendar_id}','{$publickey}');
		{if $setting.recaptcha_enabled == 1}
			Recaptcha.create("{$publickey}",
				"captcha",
				{
				  theme: "{$setting.recaptcha_style}",
				  callback: Recaptcha.focus_response_field
				}
		   );	
        {/if}
		
	});
	
	
	function getMonthName(month) {
		var m = new Array();
		m[0] ="January";
		m[1] ="February";
		m[2] ="March";
		m[3] ="April";
		m[4] ="May";
		m[5] ="June";
		m[6] ="July";
		m[7] ="August";
		m[8] ="September";
		m[9] ="October";
		m[10] ="November";
		m[11] ="December";
		$('#month_name').html(m[(month-1)]);
		currentMonth = month;
		
		if((today.getMonth()+1)!=(month)) {
			$('#back_today').fadeIn();
		} else {
			$('#back_today').fadeOut(0);
		}
	}
	
	
	function showResponse(calendar_id) {
		$('#container_all').parent().prepend('<div id="sfondo" class="modal_sfondo" onclick="hideResponse('+calendar_id+',\'\','+newday.getFullYear()+','+(newday.getMonth()+1)+')"></div>');
		$('#ok_response').attr("href","javascript:hideResponse("+calendar_id+",'',"+newday.getFullYear()+","+(newday.getMonth()+1)+");");
		$('#modal_response').fadeIn('slow');
		$('#submit_button').removeAttr("disabled");
	}
    
	function showCaptchaError() {
		$('#captcha_error').fadeIn();
		$('#submit_button').removeAttr("disabled");
	}
	
	function clearForm() {
		var formObj = document.forms[0];
        {if in_array("reservation_name", $arrFields)}
        	formObj.reservation_name.value='';
        {/if}
            
		{if in_array("reservation_surname", $arrFields)}
			formObj.reservation_surname.value='';
        {/if}
        {if in_array("reservation_email", $arrFields)}
			formObj.reservation_email.value='';
        {/if}
		{if in_array("reservation_phone", $arrFields)}
			formObj.reservation_phone.value='';
        {/if}
		{if in_array("reservation_message", $arrFields)}
			formObj.reservation_message.value='';
        {/if}
        {if in_array("reservation_field1", $arrFields)}
			formObj.reservation_field1.value='';
        {/if}
        {if in_array("reservation_field2", $arrFields)}
			formObj.reservation_field2.value='';
        {/if}
        {if in_array("reservation_field3", $arrFields)}
			formObj.reservation_field3.value='';
        {/if}
        {if in_array("reservation_field4", $arrFields)}
			formObj.reservation_field4.value='';
        {/if}		
		$('#captcha_error').fadeOut();
	}
	
	function updateCalendarSelect(category) {
		$.ajax({
		  url: '{ci helper="url" function="base_url"}booking/getCalendarsList/?category_id='+category,
		  success: function(data) {
			  arrData = data.split('|');
			  $('#calendar_select_input').html(arrData[0]);
			  $("#calendar_select_input").val($("#calendar_select_input option:first").val());
            {if $setting.show_first_filled_month == 0}
				var newday= today;
            {else}
				monthData = arrData[2].split(",");
				var newday = new Date(monthData[0],monthData[1],monthData[2]);
            {/if}
			$('#calendar_id').val($("#calendar_select_input option:first").val());
			 getMonthCalendar((newday.getMonth()+1),newday.getFullYear(),arrData[1],'{$publickey}');
		  }
		});
	}
    
	function updateCalendar(calendar_id) {
		$.ajax({
		  url: '{ci helper="url" function="base_url"}booking/getCalendar/?calendar_id='+calendar_id,
		  success: function(data) {
			  
            {if $setting.show_first_filled_month == 0}
				var newday= today;
            {else}
				monthData = data.split(",");
				var newday = new Date(monthData[0],monthData[1],monthData[2]);
            {/if}
				
			$('#calendar_id').val(calendar_id);
			getMonthCalendar((newday.getMonth()+1),newday.getFullYear(),calendar_id,'{$publickey}');
		  }
		});
		
	}
		
</script>
<div class="container-fluid page-header">
    <div class="col-xs-12 pl-0"><h1>{ci helper='language' function='lang' param1="h1"}</h1></div>
    <div class="col-xs-12 pl-0">
        <h4>{ci helper='language' function='lang' param1="h4"}</h4>
        <p>{ci helper='language' function='lang' param1="warring_TST"}</p>
        <p>{ci helper='language' function='lang' param1="warring_HDOT"}</p>
        <p>{ci helper='language' function='lang' param1="warring_HDOT&TST"}</p>
        <p>{ci helper='language' function='lang' param1="warring_hyperactive"}</p>
    </div>
</div>
<!-- ===============================================================
	box preview available time slots
================================================================ -->
<div class="box_preview_container_all" id="box_slots" style="display:none">
    <div class="box_preview_title" id="popup_title">{$calendar_title}</div>
    <div class="box_preview_slots_container" id="slots_popup">
        
    </div>
</div>

<!-- ===============================================================
	booking calendar begins here
================================================================ -->
<div class="main_container" id="container_all">
	
    <!-- =======================================
    	header (month + navigation + select)
	======================================== -->
   
	<div class="header_container">
    	<!-- month and navigation -->
    	<div class="month_container_all">
        	<!-- month -->
        	<div class="month_container  month_container_custom">
            	<div class="font_custom month_name month_name_custom" id="month_name"></div>
                <div class="font_custom month_year year_name_custom" id="month_year"></div>
            </div>
            
            <!-- navigation -->
            <div class="month_nav_container" id="month_nav">
            	<div class="mont_nav_button_container" id="month_nav_prev">
                    <a href="javascript:getPreviousMonth({$calendar_id},'{$publickey}',{$setting.calendar_month_limit_past});" class="month_nav_button month_navigation_button_custom"><img src="{ci helper="url" function="base_url"}dist/images/prev.png" /></a>
                </div>
                <div class="mont_nav_button_container" id="month_nav_next">
                    <a href="javascript:getNextMonth({$calendar_id},'{$publickey}',{$setting.calendar_month_limit_future});" class="month_nav_button month_navigation_button_custom"><img src="{ci helper="url" function="base_url"}dist/images/next.png" /></a>
                </div>
            </div>
            <div class="cleardiv"></div>
            <div class="back_today" id="back_today"><a href="javascript:getMonthCalendar((today.getMonth()+1),today.getFullYear(),$('#calendar_id').val(),'{$publickey}');">Back to today</a></div>
        </div>
        
        

		<div class="select_calendar_container">
        {if $setting.show_category_selection == 1 AND (! isset($GET.calendar_id) || $GET.calendar_id == 0) AND (! isset($GET.category_id) || $GET.category_id == 0)}
			<!-- select calendar -->
            
            	<!-- select message -->
				<div class="select_calendar_message" id="category_select_label" style="margin-bottom:10px">SELECT CATEGORY</div>
			
            	<!-- select -->
				<div class="select_container" id="category_select" style="margin-bottom:10px">
                    {if count($arrayCategories) > 0}
						<select name="category" onchange="javascript:updateCalendarSelect(this.options[this.selectedIndex].value);">
                            {foreach $arrayCategories as $categoryId => $category}
								<option value="{$categoryId}" {if $categoryId == $category_id}selected="selected"{/if}>{$category.category_name}</option>
                            {/foreach}
						</select>
                    {/if}
				</div>
				
				<div class="cleardiv"></div>
        {/if}
        
        {if $setting.show_calendar_selection == 1 AND (! isset($GET.calendar_id) OR $GET.calendar_id == 0)}
			<!-- select calendar -->
            
            	<!-- select message -->
				<div class="select_calendar_message" id="calendar_select_label">SELECT CALENDAR</div>
            
            	<!-- select -->
				<div class="select_container" id="calendar_select">
                    {if count($arrayCalendars) > 0}
						<select name="calendar" id="calendar_select_input" onchange="javascript:updateCalendar(this.options[this.selectedIndex].value);">
                            {foreach $arrayCalendars as $calendarId => $calendar}
								<option value="{$calendarId}" {if $calendarId == $calendar_id}selected="selected"{/if}>{$calendar.calendar_title}</option>
                            {/foreach}
						</select>
					{/if}
				</div>
				<div class="cleardiv"></div>
		{/if}
        </div>
    </div>
    
    <div class="cleardiv"></div>
    
    <!-- =======================================
    	calendar
	======================================== -->
    
    <!-- calendar -->
    <div class="calendar_container_all">
    	<!-- days name -->
        <div class="name_days_container" id="name_days_container">
            {if $setting.date_format == "UK" OR $setting.date_format == "EU"}
                <div class="font_custom day_name weekdays_custom">Monday</div>
                <div class="font_custom day_name weekdays_custom">Tuesday</div>
                <div class="font_custom day_name weekdays_custom">Wednesday</div>
                <div class="font_custom day_name weekdays_custom">Thursday</div>
                <div class="font_custom day_name weekdays_custom">Friday</div>
                <div class="font_custom day_name weekdays_custom">Saturday</div>
                <div class="font_custom day_name weekdays_custom" style="margin-right: 0px;">Sunday</div>
            {else}
                <div class="font_custom day_name weekdays_custom">Sunday</div>
                <div class="font_custom day_name weekdays_custom">Monday</div>
                <div class="font_custom day_name weekdays_custom">Tuesday</div>
                <div class="font_custom day_name weekdays_custom">Wednesday</div>
                <div class="font_custom day_name weekdays_custom">Thursday</div>
                <div class="font_custom day_name weekdays_custom">Friday</div>
                <div class="font_custom day_name weekdays_custom" style="margin-right: 0px;">Saturday</div>
            {/if}
        </div>
        
        <!-- days -->
        <div class="days_container_all" id="calendar_container">
        	<!-- content by js -->
        </div>
    </div>
    
    <!-- =======================================
    	booking form. It appears once user clicked on a day
	======================================== -->
    <form name="slot_reservation" id="slot_reservation" action="{ci helper="url" function="base_url"}booking/doReservation" method="post" target="iframe_submit" tmt:validate="true">
    <div class="booking_container_all" id="booking_container" style="display:none">
    	<div id="slot_form">
    	
        </div>
        <input type="hidden" name="calendar_id" id="calendar_id" value="{$calendar_id}" />
        <!-- rightside -->
        <div class="booking_right" style="background-color:{$setting.form_bg};color:{$setting.form_color}">
        	{*{if in_array("reservation_name", $arrFields)}
				<!-- name -->
                {if $reservFieldType.reservation_name == "text"}
                    <div class="booking_form_box_text">
                        <div class="booking_form_label">{$Labels.INDEX_NAME}</div>
                        <div class="booking_form_input">
                            <input type="text" name="reservation_name" class="field_input_custom" id="input_booking" {if in_array("reservation_name",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_NAME_ALERT}"{/if}/>
                         </div>
                    </div>
                {elseif $reservFieldType.reservation_name == "textarea"}
                    <div class="booking_form_box_textarea">
                        <div class="booking_form_label">{$Labels.INDEX_NAME}</div>
                        <div class="booking_form_input">
                            <textarea name="reservation_name" class="field_input_custom" id="textarea_booking" {if in_array("reservation_name",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_NAME_ALERT}"{/if}></textarea>
                        </div>
                    </div>
                {/if}
            {else}
                <input type="hidden" name="reservation_name" value="" />
            {/if}*}
            
            {*{if in_array("reservation_surname", $arrFields)}
				<!-- surname -->
                {if $reservFieldType.reservation_surname == "text"}
                    <div class="booking_form_box_text">
                        <div class="booking_form_label">{$Labels.INDEX_SURNAME}</div>
                        <div class="booking_form_input">
                            <input type="text" name="reservation_surname" class="field_input_custom" id="input_booking" {if in_array("reservation_surname",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_SURNAME_ALERT}"{/if}/>
                         </div>
                     </div>
                {elseif $reservFieldType.reservation_surname == 'textarea'}
                    <div class="booking_form_box_textarea">
                        <div class="booking_form_label">{$Labels.INDEX_SURNAME}</div>
                        <div class="booking_form_input">
                            <textarea name="reservation_surname" class="field_input_custom" id="textarea_booking" {if in_array("reservation_surname",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_SURNAME_ALERT}"{/if}></textarea>
                        </div>
                     </div>
                {/if}
            {else}
                <input type="hidden" name="reservation_surname" value="" />
            {/if}*}
             
            {*{if in_array("reservation_email", $arrFields)}
				<!-- name -->
				{if $reservFieldType.reservation_email == "text"}
                    <div class="booking_form_box_text">
                        <div class="booking_form_label">{$Labels.INDEX_EMAIL}</div>
                        <div class="booking_form_input">
                            <input type="text"  name="reservation_email" class="field_input_custom" id="input_booking" {if in_array("reservation_email",$mandatoryFields)} tmt:required="true" tmt:pattern="email" tmt:message="{$Labels.INDEX_EMAIL_ALERT}"{/if}/>
                        </div>
                    </div>
                {elseif $reservFieldType.reservation_email == 'textarea'}
                    <div class="booking_form_box_textarea">
                        <div class="booking_form_label">{$Labels.INDEX_EMAIL}</div>
                        <div class="booking_form_input">
                            <textarea  name="reservation_email" class="field_input_custom" id="textarea_booking" {if in_array("reservation_email",$mandatoryFields)} tmt:required="true" tmt:pattern="email" tmt:message="{$Labels.INDEX_EMAIL_ALERT}"{/if}></textarea>
                        </div>
                    </div>
                {/if}  
			{else}
                <input type="hidden" name="reservation_email" value="" />
            {/if}*}
            
            {if in_array("reservation_phone", $arrFields)}
				<!-- name -->
                {if $reservFieldType.reservation_phone == "text"}
                    <div class="booking_form_box_text">
                        <div class="booking_form_label">{$Labels.INDEX_PHONE}</div>
                        <div class="booking_form_input">
                            <input type="text" name="reservation_phone" class="form-control" id="input_booking" {if in_array("reservation_phone",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_PHONE_ALERT}"{/if} />
                        </div>
                    </div>
                {elseif $reservFieldType.reservation_phone == 'textarea'}
                    <div class="booking_form_box_textarea">
                        <div class="booking_form_label">{$Labels.INDEX_PHONE}</div>
                        <div class="booking_form_input">
                            <textarea name="reservation_phone" class="field_input_custom" id="textarea_booking" {if in_array("reservation_phone",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_PHONE_ALERT}"{/if}></textarea>
                        </div>
                    </div>
                {/if}
            {else}
                <input type="hidden" name="reservation_phone" value="" />
            {/if}
            
            {if in_array("reservation_message", $arrFields)}
				<!-- message -->
                {if $reservFieldType.reservation_message == "text"}
                    <div class="booking_form_box_text">
                        <div class="booking_form_label">{$Labels.INDEX_MESSAGE}</div>
                        <div class="booking_form_input">
                            <input type="text" id="input_booking" class="field_input_custom" name="reservation_message" {if in_array("reservation_message",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_MESSAGE_ALERT}"{/if} />
                        </div>
                    </div>
                {elseif $reservFieldType.reservation_message == 'textarea'}
                    <div class="booking_form_box_textarea">
                        <div class="booking_form_label">{$Labels.INDEX_MESSAGE}</div>
                        <div class="booking_form_input">
                            <textarea id="textarea_booking" class="form-control" name="reservation_message" {if in_array("reservation_message",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_MESSAGE_ALERT}"{/if}></textarea>
                        </div>
                    </div>
                {/if}
            {else}
                <input type="hidden" name="reservation_message" value="" />
            {/if}
            
			{if in_array("reservation_field1",$arrFields)}
				<!-- name -->
                {if $reservFieldType.reservation_field1 == "text"}
                    <div class="booking_form_box_text">
                        <div class="booking_form_label">{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD1}</div>
                        <div class="booking_form_input">
                            <input type="text" name="reservation_field1" class="field_input_custom" id="input_booking" {if in_array("reservation_field1",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD1_ALERT}"{/if} />
                        </div>
                    </div>
                {elseif $reservFieldType.reservation_field1 == 'textarea'}
                    <div class="booking_form_box_textarea">
                        <div class="booking_form_label">{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD1}</div>
                        <div class="booking_form_input">
                            <textarea name="reservation_field1" class="field_input_custom" id="textarea_booking" {if in_array("reservation_field1",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD1_ALERT}"{/if}></textarea>
                        </div>
                    </div>
                {/if}
            {else}
                <input type="hidden" name="reservation_field1" value="" />
            {/if}
            
			{if in_array("reservation_field2",$arrFields)}
				<!-- name -->
                {if $reservFieldType.reservation_field2 == "text"}
                    <div class="booking_form_box_text">
                        <div class="booking_form_label">{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD2}</div>
                        <div class="booking_form_input">
                            <input type="text" name="reservation_field2" class="field_input_custom" id="input_booking" {if in_array("reservation_field2",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD2_ALERT}"{/if} />
                        </div>
                    </div>
                {elseif $reservFieldType.reservation_field2 == 'textarea'}
                    <div class="booking_form_box_textarea">
                        <div class="booking_form_label">{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD2}</div>
                        <div class="booking_form_input">
                            <textarea name="reservation_field2" class="field_input_custom" id="textarea_booking" {if in_array("reservation_field2",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD2_ALERT}"{/if}></textarea>
                        </div>
                    </div>
                {/if}
            {else}
                <input type="hidden" name="reservation_field2" value="" />
            {/if}
            
			{if in_array("reservation_field3",$arrFields)}
				<!-- name -->
                {if $reservFieldType.reservation_field3 == "text"}
                    <div class="booking_form_box_text">
                        <div class="booking_form_label">{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD3}</div>
                        <div class="booking_form_input">
                            <input type="text" name="reservation_field3" class="field_input_custom" id="input_booking" {if in_array("reservation_field3",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD3_ALERT}"{/if} />
                        </div>
                    </div>
                {elseif $reservFieldType.reservation_field3 == 'textarea'}
                    <div class="booking_form_box_textarea">
                        <div class="booking_form_label">{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD3}</div>
                        <div class="booking_form_input">
                            <textarea name="reservation_field3" class="field_input_custom" id="textarea_booking" {if in_array("reservation_field3",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD3_ALERT}"{/if}></textarea>
                        </div>
                    </div>
                {/if}
            {else}
                <input type="hidden" name="reservation_field3" value="" />
            {/if}
            
			{if in_array("reservation_field4",$arrFields)}
				<!-- name -->
                {if $reservFieldType.reservation_field4 == "text"}
                    <div class="booking_form_box_text">
                        <div class="booking_form_label">{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD4}</div>
                        <div class="booking_form_input">
                            <input type="text" name="reservation_field4" class="field_input_custom" id="input_booking" {if in_array("reservation_field4",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD4_ALERT}"{/if} />
                        </div>
                    </div>
                {elseif $reservFieldType.reservation_field4 == 'textarea'}
                    <div class="booking_form_box_textarea">
                        <div class="booking_form_label">{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD4}</div>
                        <div class="booking_form_input">
                            <textarea name="reservation_field4" class="field_input_custom" id="textarea_booking" {if in_array("reservation_field4",$mandatoryFields)} tmt:required="true" tmt:message="{$Labels.INDEX_RESERVATION_ADDITIONAL_FIELD4_ALERT}"{/if}></textarea>
                        </div>
                    </div>
                {/if}
            {else}
                <input type="hidden" name="reservation_field4" value="" />
            {/if}
            
			{if $setting.show_terms == 1 AND $setting.terms_label != ''}
                <!-- terms -->
                <div class="cleardiv"></div>
                <div class="booking_form_box">
                    <div class="booking_form_label"><input type="checkbox" name="reservation_terms" value="checked" tmt:minchecked="1" tmt:message="{$Labels.INDEX_TERMS_AND_CONDITIONS_ALERT}"/>&nbsp;
					<a href="{$setting.terms_link}" target="_blank" class="terms_link">{$setting.terms_label}</a></div>
                    <div class="booking_form_input"></div>
                </div>
            {/if}  

            <!-- google capthca -->
			{if $setting.recaptcha_enabled == 1}
                <div class="booking_form_box" style="margin-top:10px;clear:both">
                    <div id="captcha_error" style="display:none">{$Labels.INDEX_INVALID_CODE}</div>
                    <div id="captcha"></div>
                </div>
            {/if}
            
            <div class="cleardiv"></div>
            <!-- book now button and clear -->
            <div class="action_form_button">
            	<div class="book_now_button_container">
					{if $setting.paypal == 1 AND $setting.paypal_account != '' AND $setting.paypal_locale != '' AND $setting.paypal_currency != ''}
                        <input type="hidden" name="with_paypal" id="with_paypal" value="1" />
                        <input type="button" id="submit_button" class="booking_button font_custom book_button book_now_custom btn btn-success" value="{$Labels.INDEX_BOOK_NOW}" style="cursor:pointer" />
                    {else}
                        <input type="submit" id="submit_button" class="booking_button font_custom book_button book_now_custom btn btn-success" value="{$Labels.INDEX_BOOK_NOW}" style="cursor:pointer" />
                    {/if}
                	
                </div>
            	<div class="reset_form_button"><a href="javascript:clearForm();" class="booking_button reset_button clear_custom">{$Labels.INDEX_CLEAR}</a></div>
            </div>
        </div>
        
    </div>
    </form>
    
    <div style="clear:both"></div>
</div>


<!-- ===============================================================
	box after booking
================================================================ -->
<div id="modal_response" class="modal" style="display:none">
	{if $setting.reservation_confirmation_mode == 1}
		{$Labels.INDEX_CONFIRM1}
	{elseif $setting.reservation_confirmation_mode == 2}
		{$Labels.INDEX_CONFIRM2}
	{elseif $setting.reservation_confirmation_mode == 3}
		{$Labels.INDEX_CONFIRM3}
	{/if}
    <br /><a href="javascript:hideResponse({$calendar_id},'{$publickey}');" class="booking_button ok_button book_now_custom" id="ok_response">OK</a>
</div>

<!-- preloader -->
<div id="modal_loading" class="modal_loading" style="display:none">
	<img src="{ci helper="url" function="base_url"}/dist/images/loading.png" border=0 />
</div>
<!-- necessary to submit form without reload the page -->
<iframe style="border:none;width:0px;height:0px" id="iframe_submit" name="iframe_submit"></iframe>

{/block}