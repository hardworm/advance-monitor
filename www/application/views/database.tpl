{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
	<div class="container-fluid page-header">
        <div class="col-xs-12 pl-0"><h1>DATA BASES</h1></div>
    </div>
    {if !empty($users)}
	<div class="contBox">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">DATA BASES</div>

        <!-- Table -->
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>DOB</th>
                    <th></th>
                    <th>Alarm</th>
                    <th>TST</th>
                    <th>HDOT</th>
                    <th>QS</th>
                    <th>Diagnosis</th>
                    <th>Message</th>
                </tr>
            </thead>
            <tbody>
                {foreach $users as $val}
                <tr>
                    <td>{$val.id}</td>
                    <td>{$val.first_name} {$val.last_name}</td>
                    <td>{$val.DOB}</td>
                    <td>{if $val.is_male == 1}M{elseif $val.is_male > -1}F{/if}</td>
                    <td>
                        {if $val.alarm_is_not_read >0}
                            <a href="{ci helper="url" function="base_url"}database/alarms/{$val.id}"><span class="label label-danger">{$val.alarm}</span></a>
                        {else}
                            <a href="{ci helper="url" function="base_url"}database/alarms/{$val.id}">{$val.alarm}</a>
                        {/if}
                    </td>
                    <td>
                        {if $val.TST > $val.TST_confirmed}<a href="{ci helper="url" function="base_url"}database/bookings/{$val.id}"><span class="label label-danger">{$val.TST}</span></a>{else}{$val.TST|default:0}{/if}
                    </td>
                    <td>
                        {if $val.HDOT > $val.HDOT_confirmed}<a href="{ci helper="url" function="base_url"}database/bookings/{$val.id}"><span class="label label-danger">{$val.HDOT}</span></a>{else}{$val.HDOT|default:0}{/if}
                    </td>
                    <td>
                        {if $val.QS_is_not_read >0}
                            <a href="{ci helper="url" function="base_url"}database/return_visits/{$val.id}"><span class="label label-danger">{$val.QS}</span></a>
                        {else}
                            <a href="{ci helper="url" function="base_url"}database/return_visits/{$val.id}">{$val.QS}</a>
                        {/if}
                    </td>
                    <td>{$val.diagnosis}</td>
                    <td><a href="mailto:{$val.email}">{$val.email}</a></td>
                </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
	</div>
            {$pagination}
    {/if}
    
{/block}