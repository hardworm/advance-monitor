{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}

	<div class="container-fluid page-header">
        <div class="col-xs-12 pl-0"><h1>{ci helper='language' function='lang' param1="alarm_form"}</h1></div>
    </div>
    <div id="step1" class="contBox">
        <h2>{ci helper='language' function='lang' param1="h2"}</h2>
        <p>{ci helper='language' function='lang' param1="information_h2"}</p>
        <div class="col-xs-12 col-md-8 col-sm-10 pl-0">
            <table class="table table-striped">
                <tr>
                    <td>{ci helper='language' function='lang' param1="cry"}</td>
                    <td><input type="checkbox" id="cry" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="excitation_hyperactivity"}</td>
                    <td><input type="checkbox" id="hyperactivated" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="flaccidity_weakness"}</td>
                    <td><input type="checkbox" id="floppy" class="checkbox_step1" value="1"></td>
                </tr>

                <tr>
                    <td>{ci helper='language' function='lang' param1="muscular_hypertonus"}</td>
                    <td><input type="checkbox" id="muscular_hypertonus" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="incoordination"}</td>
                    <td><input type="checkbox" id="incoordination" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="hypotonia"}</td>
                    <td><input type="checkbox" id="hypotonia" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="hypersensibity"}</td>
                    <td><input type="checkbox" id="hypersensibity" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="seizures"}</td>
                    <td><input type="checkbox" id="seizures" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="hyperkynesis"}</td>
                    <td><input type="checkbox" id="hyperkynesis" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="stereotypes"}</td>
                    <td><input type="checkbox" id="stereotypes" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="headache"}</td>
                    <td><input type="checkbox" id="headache" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="sleep"}</td>
                    <td><input type="checkbox" id="sleep" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="high_temperature"}</td>
                    <td><input type="checkbox" id="temperature" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="swelling"}</td>
                    <td><input type="checkbox" id="swelling" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="hypersalivation"}</td>
                    <td><input type="checkbox" id="hypersalivation" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="cool_limbs"}</td>
                    <td><input type="checkbox" id="cool_limbs" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="pale"}</td>
                    <td><input type="checkbox" id="pale" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="redness"}</td>
                    <td><input type="checkbox" id="errhitena" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="marple_skin"}</td>
                    <td><input type="checkbox" id="marple_skin" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="cyanos"}</td>
                    <td><input type="checkbox" id="cyanos" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="respiration"}</td>
                    <td><input type="checkbox" id="respiration" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="alimentary_system"}</td>
                    <td><input type="checkbox" id="alimentary_system" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="urinary_system"}</td>
                    <td><input type="checkbox" id="urinary_system" class="checkbox_step1" value="1"></td>
                </tr>
            </table>
            <a href="javascript:window.history.back();" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</a>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 1</button>
            <button id="next_step1" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div><!--/row-->

    <div id="step2" class="contBox" style="display:none">
        <h2>{ci helper='language' function='lang' param1="please_specify"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="table2">
                <tr class="cry" id="cry" style="display:none">
                    <td>{ci helper='language' function='lang' param1="cry"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="hyperactivated" id="hyperactivated" style="display:none">
                    <td>{ci helper='language' function='lang' param1="excitation_hyperactivity"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="floppy" id="floppy" style="display:none">
                    <td>{ci helper='language' function='lang' param1="flaccidity_weakness"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="muscular_hypertonus" id="muscular_hypertonus" style="display:none">
                    <td>{ci helper='language' function='lang' param1="muscular_hypertonus"}</td>
                    <td><select class="form-control">
                            <option value="hands">{ci helper='language' function='lang' param1="arms"}</option>
                            <option value="feet">{ci helper='language' function='lang' param1="legs"}</option>
                            <option value="general">{ci helper='language' function='lang' param1="general"}</option>
                        </select></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="incoordination" id="incoordination" style="display:none">
                    <td>{ci helper='language' function='lang' param1="incoordination"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="hypotonia" id="hypotonia" style="display:none">
                    <td>{ci helper='language' function='lang' param1="hypotonia"}</td>
                    <td></td>
                    <td><input data-category2="hands" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                
                <tr class="hypersensibity" id="hypersensibity" style="display:none">
                    <td>{ci helper='language' function='lang' param1="hypersensibity"}</td>
                    <td>{ci helper='language' function='lang' param1="hypersensibity1"}</td>
                    <td><input data-category2="to_sound" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>        
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="hypersensibity" id="hypersensibity2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="hypersensibity"}</td>
                    <td>{ci helper='language' function='lang' param1="hypersensibity2"}</td>
                    <td><input data-category2="to_light" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>     
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="hypersensibity" id="hypersensibity3" style="display:none">
                    <td>{ci helper='language' function='lang' param1="hypersensibity"}</td>
                    <td>{ci helper='language' function='lang' param1="hypersensibity3"}</td>
                    <td><input data-category2="to_touch" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="seizures" id="seizures" style="display:none">
                    <td>{ci helper='language' function='lang' param1="seizures"}</td>
                    <td>{ci helper='language' function='lang' param1="quickened"}</td>
                    <td><input data-category2="increased" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe2"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate2"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight2"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>       
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="seizures" id="seizures2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="seizures"}</td>
                    <td><select class="form-control">
                            <option value="first_time">{ci helper='language' function='lang' param1="first_occurred"}</option>
                            <option value="new_type">{ci helper='language' function='lang' param1="new_type"}</option>
                        </select></td>
                    <td><input data-category2="first_time" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="1attack">{ci helper='language' function='lang' param1="1attack"}</option>
                            <option value="2of10attack">{ci helper='language' function='lang' param1="2of10attack"}</option>
                            <option value="10attack">{ci helper='language' function='lang' param1="more_than_10_fits"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>           
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td><textarea class="form-control" data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="please_describe"}"></textarea></td>
                </tr>
                <tr class="hyperkynesis" id="hyperkynesis" style="display:none">
                    <td>{ci helper='language' function='lang' param1="hyperkynesis"}</td>
                    <td><select class="form-control">
                            <option value="increased">{ci helper='language' function='lang' param1="quickened"}</option>
                            <option value="first_time">{ci helper='language' function='lang' param1="first_occurred"}</option>
                            <option value="new_type">{ci helper='language' function='lang' param1="new_type"}</option>
                        </select></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td><textarea class="form-control" data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="please_describe"}"></textarea></td>
                </tr>
                <tr class="stereotypes" id="stereotypes" style="display:none">
                    <td>{ci helper='language' function='lang' param1="stereotypes"}</td>
                    <td><select class="form-control">
                            <option value="increased">{ci helper='language' function='lang' param1="quickened"}</option>
                            <option value="first_time">{ci helper='language' function='lang' param1="first_occurred"}</option>
                            <option value="new_type">{ci helper='language' function='lang' param1="new_type"}</option>
                        </select></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td><textarea class="form-control" data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="please_describe"}"></textarea></td>
                </tr>
                <tr class="headache" id="headache" style="display:none">
                    <td>{ci helper='language' function='lang' param1="headache"}</td>
                    <td><select class="form-control">
                            <option value="increased">{ci helper='language' function='lang' param1="quickened"}</option>
                            <option value="first_time">{ci helper='language' function='lang' param1="first_occurred"}</option>
                            <option value="new_type">{ci helper='language' function='lang' param1="new_type"}</option>
                        </select></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td><textarea class="form-control"  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="please_describe"}"></textarea></td>
                </tr>
                
                
                <tr class="sleep" id="sleep" style="display:none">
                    <td>{ci helper='language' function='lang' param1="sleep"}</td>
                    <td>{ci helper='language' function='lang' param1="daytime_sleep"}</td>
                    <td><input data-category2="day_dream" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="shortened">{ci helper='language' function='lang' param1="shortened"}</option>
                            <option value="missing">{ci helper='language' function='lang' param1="absent"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="sleep" id="sleep2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="sleep"}</td>
                    <td>{ci helper='language' function='lang' param1="nighttime_sleep"}</td>
                    <td><input data-category2="night_dream" type="checkbox" value="1"></td>
                    <td><select multiple="multiple" class="form-control">
                            <option value="sleep_short">{ci helper='language' function='lang' param1="sleep_short"}</option>
                            <option value="often_wakes_up">{ci helper='language' function='lang' param1="often_wakes_up"}</option>
                            <option value="wakes_up12">{ci helper='language' function='lang' param1="wakes_up12"}</option>
                            <option value="wakes_up_early">{ci helper='language' function='lang' param1="wakes_up_early"}</option>
                            <option value="difficulty_falling_asleep">{ci helper='language' function='lang' param1="difficulty_falling_asleep"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="sleep" id="sleep3" style="display:none">
                    <td>{ci helper='language' function='lang' param1="sleep"}</td>
                    <td>{ci helper='language' function='lang' param1="inversion_of_sleep"}</td>
                    <td><input data-category2="inversion_sleep" type="checkbox" value="6"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="temperature" id="temperature" style="display:none">
                    <td>{ci helper='language' function='lang' param1="high_temperature"}</td>
                    <td><select class="form-control">
                            <option value="down38">{ci helper='language' function='lang' param1="down38"}</option>
                            <option value="up38">{ci helper='language' function='lang' param1="up38"}</option>
                        </select></td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="swelling" id="swelling" style="display:none">
                    <td>{ci helper='language' function='lang' param1="swelling"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="hypersalivation" id="hypersalivation" style="display:none">
                    <td>{ci helper='language' function='lang' param1="hypersalivation"}</td>
                    <td><select class="form-control">
                            <option value="quickened">{ci helper='language' function='lang' param1="quickened"}</option>
                            <option value="first_time">{ci helper='language' function='lang' param1="first_occurred"}</option>
                            <option value="decreased">{ci helper='language' function='lang' param1="decreased"}</option>
                        </select></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option> 
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="cool_limbs" id="cool_limbs" style="display:none">
                    <td>{ci helper='language' function='lang' param1="cool_limbs"}</td>
                    <td></td>
                    <td><input  type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="pale" id="pale" style="display:none">
                    <td>{ci helper='language' function='lang' param1="pale"}</td>
                    <td><select class="form-control">
                            <option value="hands">{ci helper='language' function='lang' param1="hands"}</option>
                            <option value="feet">{ci helper='language' function='lang' param1="feet"}</option>
                            <option value="face">{ci helper='language' function='lang' param1="face"}</option>
                            <option value="face">{ci helper='language' function='lang' param1="face"}</option>
                            <option value="general">{ci helper='language' function='lang' param1="general"}</option>
                        </select></td>
                    <td><input  type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="errhitena" id="errhitena" style="display:none">
                    <td>{ci helper='language' function='lang' param1="redness"}</td>
                    <td><select class="form-control">
                            <option value="hands">{ci helper='language' function='lang' param1="hands"}</option>
                            <option value="feet">{ci helper='language' function='lang' param1="feet"}</option>
                            <option value="face">{ci helper='language' function='lang' param1="face"}</option>
                            <option value="general">{ci helper='language' function='lang' param1="general"}</option>
                        </select></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="marple_skin" id="marple_skin" style="display:none">
                    <td>{ci helper='language' function='lang' param1="marple_skin"}</td>
                    <td><select class="form-control">
                            <option value="hands">{ci helper='language' function='lang' param1="hands"}</option>
                            <option value="feet">{ci helper='language' function='lang' param1="feet"}</option>
                            <option value="general">{ci helper='language' function='lang' param1="general"}</option>
                        </select></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>                            
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="cyanos" id="cyanos" style="display:none">
                    <td>{ci helper='language' function='lang' param1="cyanos"}</td>
                    <td><select class="form-control">
                            <option value="limbs">{ci helper='language' function='lang' param1="limbs"}</option>
                            <option value="nasolabial_triangle">{ci helper='language' function='lang' param1="nasolabial_triangle"}</option>
                        </select></td>
                    <td><input data-category2="limbs" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>        
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                
                <tr class="respiration" id="irregular_breathing" style="display:none">
                    <td>{ci helper='language' function='lang' param1="irregular_breathing"}</td>
                    <td><select class="form-control">
                            <option value="fist_time">{ci helper='language' function='lang' param1="fist_time"}</option>
                            <option value="quickened">{ci helper='language' function='lang' param1="quickened"}</option>
                        </select></td>
                    <td><input id="irregular_breathing" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="respiration" id="periods_of_apnea" style="display:none">
                    <td>{ci helper='language' function='lang' param1="periods_of_apnea"}</td>
                    <td><select class="form-control">
                            <option value="fist_time">{ci helper='language' function='lang' param1="fist_time"}</option>
                            <option value="quickened">{ci helper='language' function='lang' param1="quickened"}</option>
                        </select></td>
                    <td><input id="periods_of_apnea" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option> 
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option>{ci helper='language' function='lang' param1="while_falling_asleep"}</option>
                            <option>{ci helper='language' function='lang' param1="first_half_nightp"}</option>
                            <option>{ci helper='language' function='lang' param1="second_half_night"}</option>
                            <option>{ci helper='language' function='lang' param1="early_morning"}</option>
                        </select></td>
                </tr>
                <tr class="respiration" id="tachypnea" style="display:none">
                    <td>{ci helper='language' function='lang' param1="tachypnea"}</td>
                    <td><select class="form-control">
                            <option value="fist_time">{ci helper='language' function='lang' param1="fist_time"}</option>
                            <option value="quickened">{ci helper='language' function='lang' param1="quickened"}</option>
                        </select></td>
                    <td><input id="tachypnea" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option> 
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="respiration" id="weak" style="display:none">
                    <td>{ci helper='language' function='lang' param1="diminished_breathing"}</td>
                    <td><select class="form-control">
                            <option value="fist_time">{ci helper='language' function='lang' param1="fist_time"}</option>
                            <option value="quickened">{ci helper='language' function='lang' param1="quickened"}</option>
                        </select></td>
                    <td><input id="weak" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option> 
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="respiration" id="difficult_to_exhail" style="display:none">
                    <td>{ci helper='language' function='lang' param1="difficulty_exhal"}</td>
                    <td><select class="form-control">
                            <option value="fist_time">{ci helper='language' function='lang' param1="fist_time"}</option>
                            <option value="quickened">{ci helper='language' function='lang' param1="quickened"}</option>
                        </select></td>
                    <td><input id="difficult_to_exhail" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option>{ci helper='language' function='lang' param1="while_falling_asleep"}</option>
                            <option>{ci helper='language' function='lang' param1="first_half_nightp"}</option>
                            <option>{ci helper='language' function='lang' param1="second_half_night"}</option>
                            <option>{ci helper='language' function='lang' param1="early_morning"}</option>
                        </select></td>
                </tr>
                <tr class="respiration" id="difficult_to_inhail" style="display:none">
                    <td>{ci helper='language' function='lang' param1="difficult_inhal"}</td>
                    <td><select class="form-control">
                            <option value="fist_time">{ci helper='language' function='lang' param1="fist_time"}</option>
                            <option value="quickened">{ci helper='language' function='lang' param1="quickened"}</option>
                        </select></td>
                    <td><input id="difficult_to_inhail" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option>{ci helper='language' function='lang' param1="while_falling_asleep"}</option>
                            <option>{ci helper='language' function='lang' param1="first_half_nightp"}</option>
                            <option>{ci helper='language' function='lang' param1="second_half_night"}</option>
                            <option>{ci helper='language' function='lang' param1="early_morning"}</option>
                        </select></td>
                </tr>
                <tr class="respiration" id="cough" style="display:none">
                    <td>{ci helper='language' function='lang' param1="cough"}</td>
                    <td><select class="form-control">
                            <option value="dry_cough">{ci helper='language' function='lang' param1="dry"}</option>
                            <option value="wet_cough">{ci helper='language' function='lang' param1="wet"}</option>
                        </select></td>
                    <td><input id="cough" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="respiration" id="snuffle" style="display:none">
                    <td>{ci helper='language' function='lang' param1="rheum"}</td>
                    <td></td>
                    <td><input id="snuffle" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="morning">{ci helper='language' function='lang' param1="morning"}</option>
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="evening">{ci helper='language' function='lang' param1="evening"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="during_day">{ci helper='language' function='lang' param1="during_day"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="alimentary_system" id="appetite" style="display:none">
                    <td>{ci helper='language' function='lang' param1="eating"}</td>
                    <td><select class="form-control">
                            <option value="increased">{ci helper='language' function='lang' param1="increased"}</option>
                            <option value="down">{ci helper='language' function='lang' param1="decreased"}</option>
                            <option value="unchanged">{ci helper='language' function='lang' param1="no_change"}</option>
                        </select></td>
                    <td><input id="appetite" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="alimentary_system" id="drinking" style="display:none">
                    <td>{ci helper='language' function='lang' param1="drinking"}</td>
                    <td><select class="form-control">
                            <option value="failure">{ci helper='language' function='lang' param1="refuse"}</option>
                            <option value="down">{ci helper='language' function='lang' param1="less"}</option>
                        </select></td>
                    <td><input id="drinking" type="checkbox" value="1"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="alimentary_system" id="stools" style="display:none">
                    <td>{ci helper='language' function='lang' param1="bowel_movement"}</td>
                    <td><select class="form-control">
                            <option value="lock">{ci helper='language' function='lang' param1="constipation"}</option>
                            <option value="diarrhea">{ci helper='language' function='lang' param1="diarrhea"}</option>
                        </select></td>
                    <td><input id="stools" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="alimentary_system" id="crygivaniya" style="display:none">
                    <td>{ci helper='language' function='lang' param1="crygivaniya"}</td>
                    <td><select class="form-control">
                            <option value="spontaneous">{ci helper='language' function='lang' param1="spontaneous"}</option>
                            <option value="after_eating">{ci helper='language' function='lang' param1="after_meal"}</option>
                        </select></td>
                    <td><input id="crygivaniya" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="frequent">{ci helper='language' function='lang' param1="repeated"}</option>
                            <option value="few">{ci helper='language' function='lang' param1="few"}</option>
                            <option value="single">{ci helper='language' function='lang' param1="single"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="alimentary_system" id="vomiting" style="display:none">
                    <td>{ci helper='language' function='lang' param1="vomiting"}</td>
                    <td><select class="form-control">
                            <option value="spontaneous">{ci helper='language' function='lang' param1="spontaneous"}</option>
                            <option value="after_eating">{ci helper='language' function='lang' param1="after_meal"}</option>
                        </select></td>
                    <td><input id="vomiting" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="indomitable">{ci helper='language' function='lang' param1="continuous"}</option>
                            <option value="strong">{ci helper='language' function='lang' param1="severe_intermittent"}</option>
                            <option value="single">{ci helper='language' function='lang' param1="single2"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="alimentary_system" id="reflux" style="display:none">
                    <td>{ci helper='language' function='lang' param1="reflux"}</td>
                    <td><select class="form-control">
                            <option value="fist_time">{ci helper='language' function='lang' param1="fist_time"}</option>
                            <option value="quickened">{ci helper='language' function='lang' param1="quickened"}</option>
                        </select></td>
                    <td><input id="reflux" type="checkbox" value="1"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="urinary_system" id="urinary" style="display:none">
                    <td>{ci helper='language' function='lang' param1="urinary_system"}</td>
                    <td><select class="form-control">
                            <option value="is_not">{ci helper='language' function='lang' param1="absent"}</option>
                            <option value="quickened">{ci helper='language' function='lang' param1="quickened"}</option>
                            <option value="few">{ci helper='language' function='lang' param1="became_rarer"}</option>
                        </select></td>
                    <td><input id="urinary" type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="">-</option>
                            <option value="low">{ci helper='language' function='lang' param1="in_small_quantities"}</option>
                            <option value="high">{ci helper='language' function='lang' param1="large_quantities"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <button id="back_step2" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 2</button>
            <button id="next_step2" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>

        </div>
    </div><!--/row-->
    <div id="step3" class="contBox" style="display:none">
        <h2>{ci helper='language' function='lang' param1="current_weather"}</h2>
        <div class="col-xs-12 col-md-8 col-sm-10 pl-0">
            <table class="table table-striped">
                <tr>
                    <td>{ci helper='language' function='lang' param1="the_current_temperature"}</td>
                    <td><input required="required" type="text" name="temp" class="checkbox_step3 form-control" value=""></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="weather"}</td>
                    <td>
                        <div class="checkbox"><label><input type="checkbox" name="freeze"  class="checkbox_step3"  value="25"> {ci helper='language' function='lang' param1="freeze"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="comfort" class="checkbox_step3"  value="0"> {ci helper='language' function='lang' param1="comfort"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="heat" class="checkbox_step3"  value="25"> {ci helper='language' function='lang' param1="heat"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="clear" class="checkbox_step3"  value="5"> {ci helper='language' function='lang' param1="clear"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="partly_cloudy" class="checkbox_step3"  value="10"> {ci helper='language' function='lang' param1="partially_cloudy"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="overcast" class="checkbox_step3"  value="15"> {ci helper='language' function='lang' param1="overcast"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="wind" class="checkbox_step3"  value="10"> {ci helper='language' function='lang' param1="windy"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="high_wind" class="checkbox_step3"  value="20"> {ci helper='language' function='lang' param1="strong_wind"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="hurricane" class="checkbox_step3"  value="25"> {ci helper='language' function='lang' param1="hurricane"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="rain" class="checkbox_step3"  value="10"> {ci helper='language' function='lang' param1="rain"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="shower" class="checkbox_step3"  value="15"> {ci helper='language' function='lang' param1="shower"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="storm" class="checkbox_step3"  value="25"> {ci helper='language' function='lang' param1="storm"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="snowfall" class="checkbox_step3"  value="15"> {ci helper='language' function='lang' param1="snowfall"}</label></div>
                        <div class="checkbox"><label><input type="checkbox" name="weather" class="checkbox_step3"  value="25"> {ci helper='language' function='lang' param1="snowstorm"}</label></div>
                    </td>
                </tr>
            </table>
            <button id="back_step3" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 3</button>
            <button id="sendForm" type="submit" class="btn btn-success">{ci helper='language' function='lang' param1="send"}</button>
        </div>
    </div>
    
    <script>
            $(".checkbox_step1").change(function (e) {
                if ($(this).prop("checked")) {
                    $("." + e.target.id).show();
                } else {
                    $("." + e.target.id).hide();
                }
            });

            $('#next_step1').click(function () {
                $('#step1').hide();
                $('#step2').show();
            });

            $('#back_step2').click(function () {
                $('#step2').hide();
                $('#step1').show();
            });
            $('#back_step3').click(function () {
                $('#step3').hide();
                $('#step2').show();
            });

            $('#next_step2').on('click', function () {

                setup = { };
				textInfo = { };
                var _ctr = $('#table2 tr:visible');
                _ctr.each(function () {
                    var _tc = $(this).find('td:eq(2) > input[type=checkbox]');
					var _ctda = _tc.data('category2');
					
                    var _ctrc = $(this).attr('id');
					var _ctrct = $(this).attr('id')+'_1';
                    var _txtd = $(this).children('td:first').text();
					
					if(typeof _ctda == "undefined") {
						  var _ctd1  = $(this).children('td:eq(1)').find('select');
						  
							if (!_ctd1.attr("multiple")) {
								_vtd1  = _ctd1.val();
							} else {
								_vtd1 = _ctd1.children('option:selected').map(function () {
									return this.value
								}).get();
								
							}
						} else {
							_vtd1  = _ctda;
						}
					
					var _txtde2 = $(this).children('td:eq(1)');
						if (_txtde2.children().is('select')==true) {
							_txtd2 = _txtde2.find('select').children('option:selected').text();
						} else if(_txtde2.children().is('select')==false && _txtde2.text() == '') {
							_txtd2 ='undefined';
						} else {
							_txtd2 = _txtde2.text();
						}
						
					/*_txtd2 = _txtde2.children().is('select');
					*/
					var _txtde3 = $(this).children('td:eq(3)');
					var _ctds3 = _txtde3.children().is('select');
                    var _ctd3 = _txtde3.find('select');
						if (_ctds3==true) {
							if (!_ctd3.attr("multiple")) {
								_vtd3 = _ctd3.val();
								_txtd3 = _ctd3.children('option:selected').text();
							}
							else {
								_vtd3 = _ctd3.children('option:selected').map(function () {
									return this.value
								}).get();
								_txtd3 = _ctd3.children('option:selected').map(function () {
									return $(this).text();
								}).get();
							}
						} else {
							if (_txtde3.text() == '') {
								_vtd3  = 'undefined';
								_txtd3 = 'undefined';
							} else {
								_txtd3 = 'undefined';
								_vtd3  = 'undefined';
							}
						}
					
                    var _txtde4 = $(this).children('td:eq(4)');
                    var _ctds4 = _txtde4.children().is('select');
                    var _ctd4 = _txtde4.find('select');
						if (_ctds4==true) {
							if (!_ctd4.attr("multiple")) {
								_vtd4 = _ctd4.val();
								_txtd4 = _ctd4.children('option:selected').text();
							}
							else {
								_vtd4 = _ctd4.children('option:selected').map(function () {
									return this.value
								}).get();
								_txtd4 = _ctd4.children('option:selected').map(function () {
									return $(this).text();
								}).get();
							}
						} else {
							if (_txtde4.text() == '') {
								_vtd4  = 'undefined';
								_txtd4 = 'undefined';
							} else {
								_txtd4 = 'undefined';
								_vtd4  = 'undefined';
							}
						}
					
					

                    var _vtd5 = $(this).children('td:eq(5)').find('textarea, select').val();
                    if (_tc.is(':checked')) {
                        setup[_ctrc] = _vtd1 + '|' + _vtd3 + '|' + _vtd4 + '|' + _vtd5;
						textInfo[_ctrct] =_txtd +'|'+ _txtd2 + '|' + _txtd3 + '|' + _txtd4 + '|' + _vtd5;
                    }
				//console.log(_ctda);	
                });
				
				
				$('#step2').hide();
                $('#step3').show();
            });
			
			$('#sendForm').on('click', function () {
				pogoda = { };
				var _ctr2 = $('#step3 table tr:visible').find('input:text, input:checked');
				_ctr2.each(function () {
					
					var _inpn = $(this).attr('name');
					var _inpv = $(this).val();
					if (_inpv !='') {
						_inpv = _inpv
					} else {
						_inpv = 'undefined';
					}
						pogoda[_inpn] = _inpv;
				});
				//console.log(textInfo);
                //console.log(setup);
				//console.log(pogoda);
				
				$.ajax({
                    url: '{ci helper="url" function="base_url"}alarm/save'
                    , type: 'POST'
                    , data: {
                            setup:JSON.stringify(setup),
                            textInfo:JSON.stringify(textInfo),
                            pogoda:JSON.stringify(pogoda)
                        }
                    , success: function (res) {
                        alert('Данные отправлены');
                        window.location.replace('../');
                    }
                });
				
			});
			
    </script>
    <script>
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
    </script>
{/block}