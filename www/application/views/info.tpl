{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
<div class="container-fluid page-header">
    <div class="col-sm-10 pl-0"><h1>{$title}</h1></div>
    {if $is_admin}<div class="col-sm-2 a-right pr-0"><a class="btn btn-default" href="{ci helper="url" function="base_url"}info/add/" role="button"><i class="glyphicon glyphicon-plus"></i> {ci helper='language' function='lang' param1="add"}</a></div>{/if}
</div>
    
    


    <div class="jumbotron">
        <h1>{$notice.title}</h1>
        <p>{$notice.short_desc}</p>
        <a class="btn btn-default btn-lg" href="{ci helper="url" function="base_url"}info/article/{$notice.id}" role="button"><i class="glyphicon glyphicon-eye-open"></i> {ci helper='language' function='lang' param1="read"}</a>
        {if $is_admin}<a class="btn btn-default" href="{ci helper="url" function="base_url"}info/delete/{$notice.id}" role="button"><i class="glyphicon glyphicon-remove"></i> Delete</a>{/if}
    </div>

    <div class="box-justify sjumbo">
        {foreach $arrInfo as $info}
        <div class="smalljumbotron">
            <h2>{$info.title}</h2>
            <p>{$info.short_desc}</p>
            <a class="btn btn-default" href="{ci helper="url" function="base_url"}info/article/{$info.id}" role="button"><i class="glyphicon glyphicon-eye-open"></i> {ci helper='language' function='lang' param1="read"}</a>
            {if $is_admin}<a class="btn btn-default" href="{ci helper="url" function="base_url"}info/delete/{$info.id}" role="button"><i class="glyphicon glyphicon-remove"></i> Delete</a>{/if}
        </div>
        {/foreach}
        <div class="empt"></div>
    </div>
{/block}