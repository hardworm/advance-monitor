{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}

    <div class="container-fluid page-header">
        <div class="col-sm-9 pl-0"><h1>{ci helper='language' function='lang' param1="h1"}</h1></div>
        <div class="col-sm-3 a-right pr-0"><a class="btn btn-default" href="{ci helper="url" function="base_url"}booking/add/" role="button"><i class="glyphicon glyphicon-plus"></i> {ci helper='language' function='lang' param1="booking_add"}</a></div>
    </div>

    
    {if $showUserFistReport == TRUE}
        <p class="mb-30"><a class="btn btn-default" href="{ci helper="url" function="base_url"}booking/fistVisit/" role="button"><i class="glyphicon glyphicon-plus"></i> {ci helper='language' function='lang' param1="fistVisit"}</a></p>
    {/if}
    {if $showUserReport == TRUE}
        <p class="mb-30"><a class="btn btn-default" href="{ci helper="url" function="base_url"}booking/returnVisit/" role="button"><i class="glyphicon glyphicon-plus"></i> {ci helper='language' function='lang' param1="returnVisit"}</a></p>
    {/if}
        <p class="mb-30"><a class="btn btn-default" href="{ci helper="url" function="base_url"}booking/exportExcell/" role="button"><i class="glyphicon glyphicon-plus"></i> {ci helper='language' function='lang' param1="exportExcell"}</a></p>
    {if !empty($reservation)}
        <div class="contBox">
            <h2>{ci helper='language' function='lang' param1="h2"}</h2>

            <!-- Table -->
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>{ci helper='language' function='lang' param1="date"}</th>
                        <th>{ci helper='language' function='lang' param1="group"}</th>
                        <th>{ci helper='language' function='lang' param1="number_of_seats"}</th>
                        <th>{ci helper='language' function='lang' param1="start_time"}</th>
                        <th>{ci helper='language' function='lang' param1="end_time"}</th>
                        <th>{ci helper='language' function='lang' param1="reservation_confirmed"}</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $reservation as $val}
                    <tr>
                        <td>{$val.slot_date}</td>
                        <td>{$val.calendar_title}</td>
                        <td>{$val.reservation_seats}</td>
                        <td>{$val.slot_time_from}</td>
                        <td>{$val.slot_time_to}</td>
                        <td>{if $val.reservation_confirmed==1}{ci helper='language' function='lang' param1="yes"}{else}{ci helper='language' function='lang' param1="no"}{/if}</td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    {/if}
    
{/block}