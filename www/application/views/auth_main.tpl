<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">

        <title>{block name=title}{/block}</title>

        <!-- Bootstrap core CSS -->
        <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../dist/css/style.css" rel="stylesheet">
		<link rel="stylesheet" href="/dist/assets/plugins/iCheck/skins/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
        <script src="/dist/js/bootstrap.min.js"></script>

       

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="../../js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="page row">
			
            {block name=main}{/block}
			
        </div> <!-- /container -->


        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../js/ie10-viewport-bug-workaround.js"></script>
		<script src="/dist/assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="/dist/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="/dist/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="/dist/assets/js/form-elements.js"></script>
		<script src="/dist/assets/js/main.js"></script>
		<script>
			jQuery(document).ready(function() {
			
				if(jQuery("div").is("#LnG")) {
				
				
				
				
				
					var cookie = jQuery.cookie('lang');
						if(cookie == null) {
							jQuery.cookie('lang', 'english', { expires: 3, path: '/' });
						}
						$(window).bind('load', '.iradio_minimal-grey', function() {
							if(cookie == 'english') {
								$('label[for=langE]').find('.iradio_minimal-grey').addClass('checked');
							} else if(cookie == 'russian'){
								$('label[for=langR]').find('.iradio_minimal-grey').addClass('checked');
							}
						});
						
						
					$('#LnG .radio-inline').on('click', 'i', function() {
						var lng= $(this).data('lang');
						//console.log(lng)
							$.cookie('lang', lng, { expires: 3, path: '/' });
							window.location.replace('/auth/login');
						});
						
					};
					
				
				Main.init();
				FormElements.init();
			
			});
		</script>
    </body>
</html>