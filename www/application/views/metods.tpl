{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
    
    <div class="container-fluid page-header">
    <div class="col-sm-10 pl-0"><h1>{$title}</h1></div>
    {if $isAdmin}<div class="col-sm-2 a-right pr-0"><a class="btn btn-default" href="{ci helper="url" function="base_url"}metods/add/" role="button"><i class="glyphicon glyphicon-plus"></i> {ci helper='language' function='lang' param1="add"}</a></div>{/if}
</div>


    <div class="box-justify sjumbo">
        {foreach $arrMetods as $metod}
        <div class="smalljumbotron">
            <h2>{$metod.title}</h2>
            <p>{$metod.short_desc}</p>
            <a class="btn btn-default" href="{ci helper="url" function="base_url"}metods/article/{$metod.id}" role="button"><i class="glyphicon glyphicon-eye-open"></i> {ci helper='language' function='lang' param1="read"}</a>
            {if $isAdmin}<a class="btn btn-default" href="{ci helper="url" function="base_url"}metods/delete/{$metod.id}" role="button"><i class="glyphicon glyphicon-remove"></i> Delete</a>{/if}
        </div>
        {/foreach}
        <div class="empt"></div>
    </div><!--/row-->
{/block}