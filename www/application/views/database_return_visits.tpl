{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
    <h1 class="page-header">DATA BASES QS</h1>
    {if !empty($return_visits)}
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">DATA BASES QS</div>

        <!-- Table -->
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Structural</th>
                    <th>Functional</th>
                    <th>Cognitive</th>
                    <th>Physiological</th>
                    <th>Diseases</th>
                    <th>Total score</th>
                </tr>
            </thead>
            <tbody>
                {foreach $return_visits as $val}
                <tr {if $val.is_read != 1}class="danger"{/if}>
                    <td><a href="{ci helper="url" function="base_url"}/database/return_visit/{$val.id}">{$val.id}</a></td>
                    <td>{$val.date}</td>
                    <td>{$val.structural_defects|default:0}</td>
                    <td>{$val.funtsionalnost_sphere|default:0}</td>
                    <td>{$val.cognitive|default:0}</td>
                    <td>{$val.physiological_services|default:0}</td>
                    <td>{$val.disease|default:0}</td>
                    <td>{$val.total_score|default:0}</td>
                </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
        {$pagination}
        <p><a class="btn btn-default" href="javascript:window.history.back();" role="button"><i class="glyphicon glyphicon-step-backward"></i>Назад</a></p>
    {/if}
{/block}