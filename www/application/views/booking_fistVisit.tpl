{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
<div class="container-fluid page-header">
        <div class="col-xs-12 pl-0"><h1>{ci helper='language' function='lang' param1="h1"}</h1></div>
    </div>

    <div class="contBox" id="step1">
        <h2>{ci helper='language' function='lang' param1="h2"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="fvOtchet">
                <tr id="name_of_mother">
                    <td>{ci helper='language' function='lang' param1="name_of_mother"}</td>
                    <td colspan="2"><textarea class="form-control" name="name_of_mother"></textarea></td>
                </tr>
                <tr id="name_of_father">
                    <td>{ci helper='language' function='lang' param1="name_of_father"}</td>
                    <td colspan="2"><textarea class="form-control" name="name_of_father"></textarea></td>
                </tr>
                <tr id="address">
                    <td>{ci helper='language' function='lang' param1="address"}</td>
                    <td colspan="2"><textarea class="form-control" name="address"></textarea></td>
                </tr>
                <tr id="home_phone">
                    <td>{ci helper='language' function='lang' param1="home_phone"}</td>
                    <td colspan="2">
                        <input name="home_phone" type="text" class="form-control">
                    </td>
                </tr>
                <tr id="mobile_phone_mother">
                    <td>{ci helper='language' function='lang' param1="mobile_phone_mother"}</td>
                    <td colspan="2">
                        <input name="mobile_phone_mother" type="text" class="form-control">
                    </td>
                </tr>
                <tr id="work_phone_mother">
                    <td>{ci helper='language' function='lang' param1="work_phone_mother"}</td>
                    <td colspan="2">
                        <input name="work_phone_mother" type="text" class="form-control">
                    </td>
                </tr>
                <tr id="email_mother">
                    <td>{ci helper='language' function='lang' param1="email_mother"}</td>
                    <td colspan="2">
                        <input name="email_mother" type="email" class="form-control">
                    </td>
                </tr>
                <tr id="mobile_phone_father">
                    <td>{ci helper='language' function='lang' param1="mobile_phone_father"}</td>
                    <td colspan="2">
                        <input name="mobile_phone_father" type="text" class="form-control">
                    </td>
                </tr>
                <tr id="work_phone_father">
                    <td>{ci helper='language' function='lang' param1="work_phone_father"}</td>
                    <td colspan="2">
                        <input name="work_phone_father" type="text" class="form-control">
                    </td>
                </tr>
                <tr id="email_father">
                    <td>{ci helper='language' function='lang' param1="email_father"}</td>
                    <td colspan="2">
                        <input name="email_father" type="email" class="form-control">
                    </td>
                </tr>
                <tr id="position_father">
                    <td>{ci helper='language' function='lang' param1="position_father"}</td>
                    <td colspan="2"><textarea class="form-control" name="position_father"></textarea></td>
                </tr>
                <tr id="position_mother">
                    <td>{ci helper='language' function='lang' param1="position_mother"}</td>
                    <td colspan="2"><textarea class="form-control" name="position_mother"></textarea></td>
                </tr>
                
                {if $lang == "russian"}
                <tr id="spoken_english">
                    <td>{ci helper='language' function='lang' param1="spoken_english"}</td>
                    <td colspan="2"><select name="spoken_english" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                {/if}
                
                <tr id="was_baby_preterm">
                    <td>{ci helper='language' function='lang' param1="was_baby_preterm"}</td>
                    <td colspan="2"><select name="was_baby_preterm" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="respiratory_care">
                    <td>{ci helper='language' function='lang' param1="respiratory_care"}</td>
                    <td colspan="2"><select name="respiratory_care" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="harm_during_pregnancy">
                    <td>{ci helper='language' function='lang' param1="harm_during_pregnancy"}</td>
                    <td colspan="2"><select name="harm_during_pregnancy" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="сhild_attends_school_or_kindergarten">
                    <td>{ci helper='language' function='lang' param1="school_or_kindergarten"}</td>
                    <td colspan="2"><select name="сhild_attends_school_or_kindergarten" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="hours_sitting">
                    <td>{ci helper='language' function='lang' param1="hours"} {ci helper='language' function='lang' param1="sitting"}</td>
                    <td colspan="2">
                        <input name="hours_sitting" type="text" class="form-control">
                    </td>
                </tr>
                <tr id="hours_lying_on_back">
                    <td>{ci helper='language' function='lang' param1="hours"} {ci helper='language' function='lang' param1="lying_back"}</td>
                    <td colspan="2">
                        <input name="hours_lying_on_back" type="text" class="form-control">
                    </td>
                </tr>
                <tr id="hours_lying_on_stomach">
                    <td>{ci helper='language' function='lang' param1="hours"} {ci helper='language' function='lang' param1="lying_stomach"}</td>
                    <td colspan="2">
                        <input name="hours_lying_on_stomach" type="text" class="form-control">
                    </td>
                </tr>
                <tr id="hours_other">
                    <td>{ci helper='language' function='lang' param1="hours"} {ci helper='language' function='lang' param1="hours_other"}</td>
                    <td colspan="2">
                        <input name="hours_other" type="text" class="form-control">
                    </td>
                </tr>
                <tr id="first_improvements">
                    <td>{ci helper='language' function='lang' param1="first_improvements"}</td>
                    <td colspan="2">
                        <input name="first_improvements" type="text" class="form-control">
                    </td>
                </tr>
                
                <tr id="find_us">
                    <td>{ci helper='language' function='lang' param1="find_us"}</td>
                    <td colspan="2"><select name="find_us" class="form-control">
                            <option value="word_of_mouth">{ci helper='language' function='lang' param1="word_of_mouth"}</option>
                            <option value="tv_radio">{ci helper='language' function='lang' param1="tv_radio"}</option>
                            <option value="newspaper">{ci helper='language' function='lang' param1="newspaper"}</option>
                            <option value="magazine">{ci helper='language' function='lang' param1="magazine"}</option>
                            <option value="from_family_of_child">{ci helper='language' function='lang' param1="from_family_of_child"}</option>
                            <option value="internet">{ci helper='language' function='lang' param1="internet"}</option>
                            <option value="medical">{ci helper='language' function='lang' param1="medical"}</option>
                        </select>
                    </td>
                </tr>
            </table>
            <a href="javascript:window.history.back();" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</a>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 1</button>
            <button id="next_step1" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div><!--/row-->
    
    <div class="contBox" id="step2" style="display:none">
        <h2>{ci helper='language' function='lang' param1="what_therapies"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="fvMedication">
                               
                <tr id="medication">
                    <td>{ci helper='language' function='lang' param1="medication"}</td>
                    <td colspan="2"><select name="medication" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="muscle_relaxants">
                    <td>{ci helper='language' function='lang' param1="muscle_relaxants"}</td>
                    <td colspan="2"><textarea class="form-control" name="muscle_relaxants"></textarea></td>
                </tr>
                <tr id="anticonvulsants">
                    <td>{ci helper='language' function='lang' param1="anticonvulsants"}</td>
                    <td colspan="2"><textarea class="form-control" name="anticonvulsants"></textarea></td>
                </tr>
                <tr id="antipsychotics">
                    <td>{ci helper='language' function='lang' param1="antipsychotics"}</td>
                    <td colspan="2"><textarea class="form-control" name="antipsychotics"></textarea></td>
                </tr>
                <tr id="antidepressants">
                    <td>{ci helper='language' function='lang' param1="antidepressants"}</td>
                    <td colspan="2"><textarea class="form-control" name="antidepressants"></textarea></td>
                </tr>
                <tr id="tranquilizers">
                    <td>{ci helper='language' function='lang' param1="tranquilizers"}</td>
                    <td colspan="2"><textarea class="form-control" name="tranquilizers"></textarea></td>
                </tr>
                <tr id="enzymes">
                    <td>{ci helper='language' function='lang' param1="enzymes"}</td>
                    <td colspan="2"><textarea class="form-control" name="enzymes"></textarea></td>
                </tr>
                <tr id="antihistamines">
                    <td>{ci helper='language' function='lang' param1="antihistamines"}</td>
                    <td colspan="2"><textarea class="form-control" name="antihistamines"></textarea></td>
                </tr>
                <tr id="laxatives">
                    <td>{ci helper='language' function='lang' param1="laxatives"}</td>
                    <td colspan="2"><textarea class="form-control" name="laxatives"></textarea></td>
                </tr>
                <tr id="hormones">
                    <td>{ci helper='language' function='lang' param1="hormones"}</td>
                    <td colspan="2"><textarea class="form-control" name="hormones"></textarea></td>
                </tr>
                
                <tr id="physiotherapy">
                    <td>{ci helper='language' function='lang' param1="physiotherapy"}</td>
                    <td colspan="2"><select name="physiotherapy" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="physiotherapy_details">
                    <td>{ci helper='language' function='lang' param1="physiotherapy_details"}</td>
                    <td colspan="2"><textarea class="form-control" name="physiotherapy_details"></textarea></td>
                </tr>
                <tr id="reflexology">
                    <td>{ci helper='language' function='lang' param1="reflexology"}</td>
                    <td colspan="2"><select name="reflexology" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="massage">
                    <td>{ci helper='language' function='lang' param1="massage"}</td>
                    <td colspan="2"><select name="massage" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="gemeopatiya">
                    <td>{ci helper='language' function='lang' param1="gemeopatiya"}</td>
                    <td colspan="2"><select name="gemeopatiya" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="BAA">
                    <td>{ci helper='language' function='lang' param1="baa"}</td>
                    <td colspan="2"><select name="BAA" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="BAA_details">
                    <td>{ci helper='language' function='lang' param1="baa_details"}</td>
                    <td colspan="2"><textarea class="form-control" name="BAA_details"></textarea></td>
                </tr>
                <tr id="other_treatments">
                    <td>{ci helper='language' function='lang' param1="other_treatments"}</td>
                    <td colspan="2"><textarea class="form-control" name="other_treatments"></textarea></td>
                </tr>
                
                <tr id="orthopedic_products">
                    <td>{ci helper='language' function='lang' param1="orthopedic_products"}</td>
                    <td colspan="2"><select name="orthopedic_products" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="orthopedic_products_details">
                    <td>{ci helper='language' function='lang' param1="orthopedic_products_details"}</td>
                    <td colspan="2"><textarea class="form-control" name="orthopedic_products_details"></textarea></td>
                </tr>
                
                <tr id="corrective_surgery">
                    <td>{ci helper='language' function='lang' param1="corrective_surgery"}</td>
                    <td colspan="2"><select name="corrective_surgery" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="corrective_surgery_details">
                    <td>{ci helper='language' function='lang' param1="corrective_surgery_details"}</td>
                    <td colspan="2"><textarea class="form-control" name="corrective_surgery_details"></textarea></td>
                </tr>
                
                <tr id="hypersensitivity_to_sound">
                    <td>{ci helper='language' function='lang' param1="hypersensitivity_to_sound"}</td>
                    <td colspan="2"><select name="hypersensitivity_to_sound" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                
            </table>
            <button id="back_step2" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 2</button>
            <button id="next_step2" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div><!--/row-->

    <div class="contBox" id="step3" style="display:none">
        <h2>{ci helper='language' function='lang' param1="pregnancy"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="fvHazard">
                
                <tr id="infection">
                    <td>{ci helper='language' function='lang' param1="infection"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="norm">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td>
                        <select class="form-control">
                            <option value="first_half">{ci helper='language' function='lang' param1="first_of_pregnancy"}</option>
                            <option value="second_half">{ci helper='language' function='lang' param1="second_of_pregnancy"}</option>
                            <option value="during_whole_period">{ci helper='language' function='lang' param1="both_trimesters"}</option>
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="stress">
                    <td>{ci helper='language' function='lang' param1="stress"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="norm">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td>
                        <select class="form-control">
                            <option value="first_half">{ci helper='language' function='lang' param1="first_of_pregnancy"}</option>
                            <option value="second_half">{ci helper='language' function='lang' param1="second_of_pregnancy"}</option>
                            <option value="during_whole_period">{ci helper='language' function='lang' param1="both_trimesters"}</option>
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="threat_miscarriage">
                    <td>{ci helper='language' function='lang' param1="threat_miscarriage"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="norm">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td>
                        <select class="form-control">
                            <option value="first_half">{ci helper='language' function='lang' param1="first_of_pregnancy"}</option>
                            <option value="second_half">{ci helper='language' function='lang' param1="second_of_pregnancy"}</option>
                            <option value="during_whole_period">{ci helper='language' function='lang' param1="both_trimesters"}</option>
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="toxemia">
                    <td>{ci helper='language' function='lang' param1="toxemia"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="norm">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="slight"}</option>
                        </select></td>
                    <td>
                        <select class="form-control">
                            <option value="first_half">{ci helper='language' function='lang' param1="first_of_pregnancy"}</option>
                            <option value="second_half">{ci helper='language' function='lang' param1="second_of_pregnancy"}</option>
                            <option value="during_whole_period">{ci helper='language' function='lang' param1="both_trimesters"}</option>
                        </select>
                    </td>
                    <td></td>
                </tr>
                
            </table>
            <button id="back_step3" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 3</button>
            <button id="next_step3" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>

        </div>
    </div><!--/row-->
    
    
    <div class="contBox" id="step4" style="display:none">
        <h2>{ci helper='language' function='lang' param1="menu"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="fvpitanie">
                <tr id="day1_breakfast">
                    <td>{ci helper='language' function='lang' param1="day1_breakfast"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day1_lunch">
                    <td>{ci helper='language' function='lang' param1="day1_lunch"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day1_afternoon_snack">
                    <td>{ci helper='language' function='lang' param1="day1_afternoon_snack"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day1_dinner">
                    <td>{ci helper='language' function='lang' param1="day1_dinner"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day1_between_meals">
                    <td>{ci helper='language' function='lang' param1="day1_between_meals"}</td>
                    <td><textarea class="form-control" name="time"></textarea></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                
                <tr id="day2_breakfast">
                    <td>{ci helper='language' function='lang' param1="day2_breakfast"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day2_lunch">
                    <td>{ci helper='language' function='lang' param1="day2_lunch"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day2_afternoon_snack">
                    <td>{ci helper='language' function='lang' param1="day2_afternoon_snack"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day2_dinner">
                    <td>{ci helper='language' function='lang' param1="day2_dinner"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day2_between_meals">
                    <td>{ci helper='language' function='lang' param1="day2_between_meals"}</td>
                    <td><textarea class="form-control" name="time"></textarea></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                
                <tr id="day3_breakfast">
                    <td>{ci helper='language' function='lang' param1="day3_breakfast"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day3_lunch">
                    <td>{ci helper='language' function='lang' param1="day3_lunch"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day3_afternoon_snack">
                    <td>{ci helper='language' function='lang' param1="day3_afternoon_snack"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day3_dinner">
                    <td>{ci helper='language' function='lang' param1="day3_dinner"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day3_between_meals">
                    <td>{ci helper='language' function='lang' param1="day3_between_meals"}</td>
                    <td><textarea class="form-control" name="time"></textarea></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="end_between_meals">
                    <td>{ci helper='language' function='lang' param1="end_between_meals"}</td>
                    <td><textarea class="form-control" name="time"></textarea></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                
            </table>
            <button id="back_step4" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 4</button>
            <button id="next_step4" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>

        </div>
    </div><!--/row-->
    
    <div class="contBox" id="step5" style="display:none">
        <h2>{ci helper='language' function='lang' param1="structural_damage"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="fvAbnormalities">
                
                <tr id="thorax">
                    <td>{ci helper='language' function='lang' param1="chest"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="cervical_spine">
                    <td>{ci helper='language' function='lang' param1="cervical_spine"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="thoracic_spine">
                    <td>{ci helper='language' function='lang' param1="thoracic_spine"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="lumbar_spine">
                    <td>{ci helper='language' function='lang' param1="lumbar_spine"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="belly">
                    <td>{ci helper='language' function='lang' param1="belly"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="pelvis">
                    <td>{ci helper='language' function='lang' param1="pelvis"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="hip">
                    <td>{ci helper='language' function='lang' param1="hip"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="legs">
                    <td>{ci helper='language' function='lang' param1="legs"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="foot">
                    <td>{ci helper='language' function='lang' param1="foot"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="shoulder-blade">
                    <td>{ci helper='language' function='lang' param1="shoulder-blade"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="upper_arm">
                    <td>{ci helper='language' function='lang' param1="upper_arm"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="hands">
                    <td>{ci helper='language' function='lang' param1="hands"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="head">
                    <td>{ci helper='language' function='lang' param1="head"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option value="severe">{ci helper='language' function='lang' param1="severe"}</option>
                            <option value="poor">{ci helper='language' function='lang' param1="poor"}</option>
                            <option value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
                            <option value="mild">{ci helper='language' function='lang' param1="mild"}</option>
                            <option selected value="normal">{ci helper='language' function='lang' param1="normal"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                
                
                
            </table>
            <button id="back_step5" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 5</button>
            <button id="next_step5" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>

        </div>
    </div><!--/row-->
    
	<div class="contBox" id="step6" style="display:none">
        <h2>{ci helper='language' function='lang' param1="funtsionalnost_sphere"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="fvFscope">
                
                <tr id="holding_head">
                    <td>{ci helper='language' function='lang' param1="holding_head"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="coordinated_limb_movements">
                    <td>{ci helper='language' function='lang' param1="coordinated_limb_movements"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="hand_movements">
                    <td>{ci helper='language' function='lang' param1="hand_movements"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="leg_movements">
                    <td>{ci helper='language' function='lang' param1="leg_movements"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="trunk_movements">
                    <td>{ci helper='language' function='lang' param1="trunk_movements"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="turn">
                    <td>{ci helper='language' function='lang' param1="turn"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="sit_up_unaided">
                    <td>{ci helper='language' function='lang' param1="sit_up_unaided"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="keep_balance">
                    <td>{ci helper='language' function='lang' param1="keep_balance"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="creep">
                    <td>{ci helper='language' function='lang' param1="creep"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="stand">
                    <td>{ci helper='language' function='lang' param1="stand"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="walk">
                    <td>{ci helper='language' function='lang' param1="walk"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="run">
                    <td>{ci helper='language' function='lang' param1="run"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="vocalize">
                    <td>{ci helper='language' function='lang' param1="vocalize"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="speak">
                    <td>{ci helper='language' function='lang' param1="speak"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="gape">
                    <td>{ci helper='language' function='lang' param1="gape"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="laugh">
                    <td>{ci helper='language' function='lang' param1="laugh"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="sing">
                    <td>{ci helper='language' function='lang' param1="sing"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="articulation_and_sound">
                    <td>{ci helper='language' function='lang' param1="articulation_and_sound"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="sight">
                    <td>{ci helper='language' function='lang' param1="sight"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="hearing">
                    <td>{ci helper='language' function='lang' param1="hearing"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="chew">
                    <td>{ci helper='language' function='lang' param1="chew"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="swallow_solid_food">
                    <td>{ci helper='language' function='lang' param1="swallow_solid_food"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="swallow_liquid_food">
                    <td>{ci helper='language' function='lang' param1="swallow_liquid_food"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="independently_drink">
                    <td>{ci helper='language' function='lang' param1="independently_drink"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="independently_there">
                    <td>{ci helper='language' function='lang' param1="independently_there"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="stool">
                    <td>{ci helper='language' function='lang' param1="stool"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="bladder_control">
                    <td>{ci helper='language' function='lang' param1="bladder_control"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                
            </table>
            <button id="back_step6" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 6</button>
            <button id="next_step6" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div>
                        
    <div class="contBox" id="step7" style="display:none">
        <h2>{ci helper='language' function='lang' param1="change_sphere"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="fvCscope">
                <tr id="react_to_their_environment">
                    <td>{ci helper='language' function='lang' param1="react_to_their_environment"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="stare">
                    <td>{ci helper='language' function='lang' param1="stare"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="recognize_people">
                    <td>{ci helper='language' function='lang' param1="recognize_people"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="communicate_nonverbally">
                    <td>{ci helper='language' function='lang' param1="communicate_nonverbally"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="communicate_verbally">
                    <td>{ci helper='language' function='lang' param1="communicate_verbally"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="say_phrases">
                    <td>{ci helper='language' function='lang' param1="say_phrases"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="understand_speech">
                    <td>{ci helper='language' function='lang' param1="understand_speech"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="perform_simple_requests">
                    <td>{ci helper='language' function='lang' param1="perform_simple_requests"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="allocate_favorite_toy">
                    <td>{ci helper='language' function='lang' param1="allocate_favorite_toy"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="play_with_other">
                    <td>{ci helper='language' function='lang' param1="play_with_other"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="independently_play">
                    <td>{ci helper='language' function='lang' param1="independently_play"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="follow_social_norms">
                    <td>{ci helper='language' function='lang' param1="follow_social_norms"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="concentrate_on_tasks">
                    <td>{ci helper='language' function='lang' param1="concentrate_on_tasks"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="listen_to_storiess">
                    <td>{ci helper='language' function='lang' param1="listen_to_storiess"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="read">
                    <td>{ci helper='language' function='lang' param1="read"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="write">
                    <td>{ci helper='language' function='lang' param1="write"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="adequately_respond_emotionally">
                    <td>{ci helper='language' function='lang' param1="respond_emotionally"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="perform_complex_tasks">
                    <td>{ci helper='language' function='lang' param1="perform_complex_tasks"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="tutor">
                    <td>{ci helper='language' function='lang' param1="tutor"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="work_in_group">
                    <td>{ci helper='language' function='lang' param1="work_in_group"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="eye_contact">
                    <td>{ci helper='language' function='lang' param1="eye_contact"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="pointing_gesture">
                    <td>{ci helper='language' function='lang' param1="pointing_gesture"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="to_be_quiet_for_request">
                    <td>{ci helper='language' function='lang' param1="to_be_quiet_for_request"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="concentrate_on_conversation">
                    <td>{ci helper='language' function='lang' param1="concentrate_on_conversation"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                
            </table>
            <button id="back_step7" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 7</button>
            <button id="next_step7" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div>
                        
    <div class="contBox" id="step8" style="display:none">
        <h2>{ci helper='language' function='lang' param1="change_sphere2"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="fvPhscope">
                
                <tr id="appetite">
                    <td>{ci helper='language' function='lang' param1="appetite"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="digestion">
                    <td>{ci helper='language' function='lang' param1="digestion"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr id="bowel_movement">
                    <td>{ci helper='language' function='lang' param1="bowel_movement"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="breathing">
                    <td>{ci helper='language' function='lang' param1="breathing"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="dream">
                    <td>{ci helper='language' function='lang' param1="dream"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select class="form-control">
                            <option selected value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td></td>
                    <td></td>
                </tr>
                
            </table>
            <button id="back_step8" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 8</button>
            <button id="next_step8" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div>
    
    <div class="contBox" id="step9" style="display:none">
        <h2>{ci helper='language' function='lang' param1="symptoms"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="fvSymptoms">
                
                <tr id="convulsions">
                    <td>{ci helper='language' function='lang' param1="convulsions"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="bronchitis">
                    <td>{ci helper='language' function='lang' param1="bronchitis"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="sars">
                    <td>{ci helper='language' function='lang' param1="sars"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="allergy">
                    <td>{ci helper='language' function='lang' param1="allergy"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td><textarea class="form-control"></textarea></td>
                </tr>
                <tr id="bronchial_asthma">
                    <td>{ci helper='language' function='lang' param1="bronchial_asthma"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="skin_diseases">
                    <td>{ci helper='language' function='lang' param1="skin_diseases"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="intestinal_colic">
                    <td>{ci helper='language' function='lang' param1="intestinal_colic"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="other">
                    <td>{ci helper='language' function='lang' param1="other"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td><textarea class="form-control"></textarea></td>
                </tr>
                <tr id="add_info">
                    <td>{ci helper='language' function='lang' param1="add_info"}</td>
                    <td></td>
                    <td><input type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td><textarea class="form-control"></textarea></td>
                </tr>
                
            </table>
            <button id="back_step9" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 9</button>
            <button id="sendForm" type="submit" class="btn btn-success">{ci helper='language' function='lang' param1="send"}</button>
        </div>
    </div>
        
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">{ci helper='language' function='lang' param1="sending_completed"}</h4>
                </div>
                <div class="modal-body">
                    <p>{ci helper='language' function='lang' param1="ATTENTION"}</p>
                </div>
            </div>
        </div>
    </div>
    
    <script>

 function checkCurr(d) {
	if(window.event) 
	  { 
		if(event.keyCode == 37 || event.keyCode == 39) return; 
	  }  
	d.value = d.value.replace(/\D/g,'');
};           
//Первичный визит - отчет родителей		
$('#next_step1').on('click', function () {
    fv_otchet = { };
	fv_otchet_vals = { };
    

    var fvo_tr = $('#fvOtchet tr:visible');
    fvo_tr.each(function() {

        var fv_td1 = $(this).children('td:eq(0)');
        var fv_td2 = $(this).children('td:eq(1)');

        var fv_name = $(this).attr('id');
        var fv_txt1 = fv_td1.text();
        var fv_val1 = 'undefined';

        var check_sel2 = fv_td2.children().is('select');
        var check_inp2 = fv_td2.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel2 = fv_td2.find('select');
        var find_inp2 = fv_td2.find('textarea, input:text');

        if (check_sel2 == true) {
            if (!find_sel2.attr("multiple")) {
                fv_txt2 = find_sel2.children('option:selected').text();
                fv_val2 = find_sel2.val();
            } else {
                fv_txt2 = find_sel2.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val2 = find_sel2.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp2 == true && find_inp2.val() != '') {
            fv_txt2 = find_inp2.val();
			fv_val2 = find_inp2.val();
        } else if (fv_td2.text() != '') {
            fv_txt2 = fv_td2.text();
			fv_val2 = 'undefined';
        } else {
            fv_txt2 = 'undefined';
			fv_val2 = 'undefined';
        }

        fv_otchet[fv_name] 		= fv_txt1 + '|' + fv_txt2;
		fv_otchet_vals[fv_name] = fv_val1 + '|' + fv_val2;

    });
	console.log(fv_otchet);
	console.log(fv_otchet_vals);
	
		$('#step1').hide();
        $('#step2').show();
		$(window).scrollTop(0);
});

//Первичный визит - Какое лечение получает ваш ребенок?		
$('#next_step2').on('click', function () {
    fv_medication = { };
	fv_medication_vals = { };

    var fvm_tr = $('#fvMedication tr:visible');
    fvm_tr.each(function() {

        var fv_td1 = $(this).children('td:eq(0)');
        var fv_td2 = $(this).children('td:eq(1)');

        var fv_name = $(this).attr('id');
        var fv_txt1 = fv_td1.text();
        var fv_val1 = 'undefined';
		
		var check_sel2 = fv_td2.children().is('select');
        var check_inp2 = fv_td2.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel2 = fv_td2.find('select');
        var find_inp2 = fv_td2.find('textarea, input:text');

        if (check_sel2 == true) {
            if (!find_sel2.attr("multiple")) {
                fv_txt2 = find_sel2.children('option:selected').text();
                fv_val2 = find_sel2.val();
            } else {
                fv_txt2 = find_sel2.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val2 = find_sel2.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp2 == true && find_inp2.val() != '') {
            fv_txt2 = find_inp2.val();
			fv_val2 = find_inp2.val();
        } else if (fv_td2.text() != '') {
            fv_txt2 = fv_td2.text();
			fv_val2 = 'undefined';
        } else {
            fv_txt2 = 'undefined';
			fv_val2 = 'undefined';
        }

        fv_medication[fv_name] 		= fv_txt1 + '|' + fv_txt2;
        fv_medication[fv_name] 		= fv_txt1 + '|' + fv_txt2;
		fv_medication_vals[fv_name] = fv_val1 + '|' + fv_val2;

    });
	console.log(fv_medication);
	console.log(fv_medication_vals);
	
		$('#step2').hide();
        $('#step3').show();
		$(window).scrollTop(0);
});

//Первичный визит - Перенесенные вредности во время беременностиа		
$('#next_step3').on('click', function () {
    fv_hazard = { };
	fv_hazard_vals = { };

    var fvh_tr = $('#fvHazard tr:visible');
    fvh_tr.each(function() {

        var fv_td1 = $(this).children('td:eq(0)');
        var fv_td2 = $(this).children('td:eq(2)');
        var fv_td3 = $(this).children('td:eq(3)');
        var fv_td4 = $(this).children('td:eq(4)');
        var fv_td5 = $(this).children('td:eq(5)');

        var fv_name = $(this).attr('id');
        var fv_txt1 = fv_td1.text();
        var fv_val1 = 'undefined';
		
		var check_chbx = fv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = fv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = fv_td3.children().is('select');
        var check_inp3 = fv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = fv_td3.find('select');
        var find_inp3  = fv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                fv_txt3 = find_sel3.children('option:selected').text();
                fv_val3 = find_sel3.val();
            } else {
                fv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            fv_txt3 = find_inp3.val();
			fv_val3 = find_inp3.val();
        } else if (fv_td3.text() != '') {
            fv_txt3 = fv_td3.text();
			fv_val3 = 'undefined';
        } else {
            fv_txt3 = 'undefined';
			fv_val3 = 'undefined';
        }
		
		var check_sel4 = fv_td4.children().is('select');
        var check_inp4 = fv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = fv_td4.find('select');
        var find_inp4  = fv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                fv_txt4 = find_sel4.children('option:selected').text();
                fv_val4 = find_sel4.val();
            } else {
                fv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            fv_txt4 = find_inp4.val();
			fv_val4 = find_inp4.val();
        } else if (fv_td4.text() != '') {
            fv_txt4 = fv_td4.text();
			fv_val4 = 'undefined';
        } else {
            fv_txt4 = 'undefined';
			fv_val4 = 'undefined';
        }
		
				
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        fv_hazard[fv_name] 		= fv_txt1 + '|' + fv_txt3 + '|' + fv_txt4;
		fv_hazard_vals[fv_name] = fv_val1 + '|' + fv_val3 + '|' + fv_val4;
		}

    });
	console.log(fv_hazard);
	console.log(fv_hazard_vals);
	
		$('#step3').hide();
        $('#step4').show();
		$(window).scrollTop(0);
});

//Первичный визит - Пример ежедневного меню
$('#next_step4').on('click', function () {

var inpVals = [];
	$('#fvpitanie .form-control').each(function(index) {
		var iVal = $(this).val();
		if (iVal == '') {
			$(this).parent('td').addClass('has-error')
								.removeClass('has-success');
			inpVals.push('error')
		} else {
			$(this).parent('td').addClass('has-success')
								.removeClass('has-error');
			inpVals.push('success')
		}
		//inpVals[index] = iVal;
	});
	//console.log(inpVals);

	if ($.inArray('error', inpVals) == -1) {

    fv_pitanie = { };
	fv_pitanie_vals = { };

    var fvm_tr = $('#fvpitanie tr:visible');
    fvm_tr.each(function() {

        var fv_td1 = $(this).children('td:eq(0)');
        var fv_td2 = $(this).children('td:eq(1)');
		var fv_td3 = $(this).children('td:eq(2)');
		
        var fv_name = $(this).attr('id');
        var fv_txt1 = fv_td1.text();
        var fv_val1 = 'undefined';
		
		var check_sel2 = fv_td2.children().is('select');
        var check_inp2 = fv_td2.children().is('textarea, input[type="text"], input[type="number"], input[type="time"]');
        var find_sel2 = fv_td2.find('select');
        var find_inp2 = fv_td2.find('textarea, input:text, input[type="time"]');

        if (check_sel2 == true) {
            if (!find_sel2.attr("multiple")) {
                fv_txt2 = find_sel2.children('option:selected').text();
                fv_val2 = find_sel2.val();
            } else {
                fv_txt2 = find_sel2.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val2 = find_sel2.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp2 == true && find_inp2.val() != '') {
            fv_txt2 = find_inp2.val();
			fv_val2 = find_inp2.val();
        } else if (fv_td2.text() != '') {
            fv_txt2 = fv_td2.text();
			fv_val2 = 'undefined';
        } else {
            fv_txt2 = 'undefined';
			fv_val2 = 'undefined';
        }
		
		var check_sel3 = fv_td3.children().is('select');
        var check_inp3 = fv_td3.children().is('textarea, input[type="text"], input[type="number"], input[type="time"]');
        var find_sel3 = fv_td3.find('select');
        var find_inp3 = fv_td3.find('textarea, input:text, input[type="time"]');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                fv_txt3 = find_sel3.children('option:selected').text();
                fv_val3 = find_sel3.val();
            } else {
                fv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            fv_txt3 = find_inp3.val();
			fv_val3 = find_inp3.val();
        } else if (fv_td3.text() != '') {
            fv_txt3 = fv_td3.text();
			fv_val3 = 'undefined';
        } else {
            fv_txt3 = 'undefined';
			fv_val3 = 'undefined';
        }

        fv_pitanie[fv_name] 		= fv_txt1 + '|' + fv_txt2 + '|' + fv_txt3;
		fv_pitanie_vals[fv_name]    = fv_val1 + '|' + fv_val2 + '|' + fv_val3;

    });
	console.log(fv_pitanie);
	console.log(fv_pitanie_vals);
	
		$('#step4').hide();
        $('#step5').show();
		$(window).scrollTop(0);
		
		} else {
		alert('{ci helper='language' function='lang' param1="complete_all_lines"}')
	}
});


//Первичный визит - Структурные нарушения	
$('#next_step5').on('click', function () {
    fv_abnormalities = { };
	fv_abnormalities_vals = { };

    var fvab_tr = $('#fvAbnormalities tr:visible');
    fvab_tr.each(function() {

        var fv_td1 = $(this).children('td:eq(0)');
        var fv_td2 = $(this).children('td:eq(2)');
        var fv_td3 = $(this).children('td:eq(3)');
        var fv_td4 = $(this).children('td:eq(4)');
        var fv_td5 = $(this).children('td:eq(5)');

        var fv_name = $(this).attr('id');
        var fv_txt1 = fv_td1.text();
        var fv_val1 = 'undefined';
		
		var check_chbx = fv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = fv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = fv_td3.children().is('select');
        var check_inp3 = fv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = fv_td3.find('select');
        var find_inp3  = fv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                fv_txt3 = find_sel3.children('option:selected').text();
                fv_val3 = find_sel3.val();
            } else {
                fv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            fv_txt3 = find_inp3.val();
			fv_val3 = find_inp3.val();
        } else if (fv_td3.text() != '') {
            fv_txt3 = fv_td3.text();
			fv_val3 = 'undefined';
        } else {
            fv_txt3 = 'undefined';
			fv_val3 = 'undefined';
        }
		
		var check_sel4 = fv_td4.children().is('select');
        var check_inp4 = fv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = fv_td4.find('select');
        var find_inp4  = fv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                fv_txt4 = find_sel4.children('option:selected').text();
                fv_val4 = find_sel4.val();
            } else {
                fv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            fv_txt4 = find_inp4.val();
			fv_val4 = find_inp4.val();
        } else if (fv_td4.text() != '') {
            fv_txt4 = fv_td4.text();
			fv_val4 = 'undefined';
        } else {
            fv_txt4 = 'undefined';
			fv_val4 = 'undefined';
        }
		
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        fv_abnormalities[fv_name] 		= fv_txt1 + '|' + fv_txt3 + '|' + fv_txt4;
		fv_abnormalities_vals[fv_name]  = fv_val1 + '|' + fv_val3 + '|' + fv_val4;
		}

    });
	console.log(fv_abnormalities);
	console.log(fv_abnormalities_vals);
	
		$('#step5').hide();
        $('#step6').show();
		$(window).scrollTop(0);
});

//Первичный визит - Функциональная сфера
$('#next_step6').on('click', function () {
    fv_fscope = { };
	fv_fscope_vals = { };

    var fvfs_tr = $('#fvFscope tr:visible');
    fvfs_tr.each(function() {

        var fv_td1 = $(this).children('td:eq(0)');
        var fv_td2 = $(this).children('td:eq(2)');
        var fv_td3 = $(this).children('td:eq(3)');
        var fv_td4 = $(this).children('td:eq(4)');
        var fv_td5 = $(this).children('td:eq(5)');

        var fv_name = $(this).attr('id');
        var fv_txt1 = fv_td1.text();
        var fv_val1 = 'undefined';
		
		var check_chbx = fv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = fv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = fv_td3.children().is('select');
        var check_inp3 = fv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = fv_td3.find('select');
        var find_inp3  = fv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                fv_txt3 = find_sel3.children('option:selected').text();
                fv_val3 = find_sel3.val();
            } else {
                fv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            fv_txt3 = find_inp3.val();
			fv_val3 = find_inp3.val();
        } else if (fv_td3.text() != '') {
            fv_txt3 = fv_td3.text();
			fv_val3 = 'undefined';
        } else {
            fv_txt3 = 'undefined';
			fv_val3 = 'undefined';
        }
		
		var check_sel4 = fv_td4.children().is('select');
        var check_inp4 = fv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = fv_td4.find('select');
        var find_inp4  = fv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                fv_txt4 = find_sel4.children('option:selected').text();
                fv_val4 = find_sel4.val();
            } else {
                fv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            fv_txt4 = find_inp4.val();
			fv_val4 = find_inp4.val();
        } else if (fv_td4.text() != '') {
            fv_txt4 = fv_td4.text();
			fv_val4 = 'undefined';
        } else {
            fv_txt4 = 'undefined';
			fv_val4 = 'undefined';
        }
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        fv_fscope[fv_name] 		= fv_txt1 + '|' + fv_txt3 + '|' + fv_txt4;
		fv_fscope_vals[fv_name] = fv_val1 + '|' + fv_val3 + '|' + fv_val4;
		}

    });
	console.log(fv_fscope);
	console.log(fv_fscope_vals);
	
		$('#step6').hide();
        $('#step7').show();
		$(window).scrollTop(0);
});

//Изменения когнитивной сферы
$('#next_step7').on('click', function () {
    fv_cscope = { };
	fv_cscope_vals = { };

    var fvcs_tr = $('#fvCscope tr:visible');
    fvcs_tr.each(function() {

        var fv_td1 = $(this).children('td:eq(0)');
        var fv_td2 = $(this).children('td:eq(2)');
        var fv_td3 = $(this).children('td:eq(3)');
        var fv_td4 = $(this).children('td:eq(4)');
        var fv_td5 = $(this).children('td:eq(5)');

        var fv_name = $(this).attr('id');
        var fv_txt1 = fv_td1.text();
        var fv_val1 = 'undefined';
		
		var check_chbx = fv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = fv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = fv_td3.children().is('select');
        var check_inp3 = fv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = fv_td3.find('select');
        var find_inp3  = fv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                fv_txt3 = find_sel3.children('option:selected').text();
                fv_val3 = find_sel3.val();
            } else {
                fv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            fv_txt3 = find_inp3.val();
			fv_val3 = find_inp3.val();
        } else if (fv_td3.text() != '') {
            fv_txt3 = fv_td3.text();
			fv_val3 = 'undefined';
        } else {
            fv_txt3 = 'undefined';
			fv_val3 = 'undefined';
        }
		
		var check_sel4 = fv_td4.children().is('select');
        var check_inp4 = fv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = fv_td4.find('select');
        var find_inp4  = fv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                fv_txt4 = find_sel4.children('option:selected').text();
                fv_val4 = find_sel4.val();
            } else {
                fv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            fv_txt4 = find_inp4.val();
			fv_val4 = find_inp4.val();
        } else if (fv_td4.text() != '') {
            fv_txt4 = fv_td4.text();
			fv_val4 = 'undefined';
        } else {
            fv_txt4 = 'undefined';
			fv_val4 = 'undefined';
        }
			
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        fv_cscope[fv_name] 		= fv_txt1 + '|' + fv_txt3 + '|' + fv_txt4;
		fv_cscope_vals[fv_name] = fv_val1 + '|' + fv_val3 + '|' + fv_val4;
		}
	
    });
	
	console.log(fv_cscope);
	console.log(fv_cscope_vals);
	
		$('#step7').hide();
        $('#step8').show();
		$(window).scrollTop(0);
	
});

//Изменение физиологической сферы
$('#next_step8').on('click', function () {
    fv_phscope = { };
	fv_phscope_vals = { };

    var fvphs_tr = $('#fvPhscope tr:visible');
    fvphs_tr.each(function() {

        var fv_td1 = $(this).children('td:eq(0)');
        var fv_td2 = $(this).children('td:eq(2)');
        var fv_td3 = $(this).children('td:eq(3)');
        var fv_td4 = $(this).children('td:eq(4)');
        var fv_td5 = $(this).children('td:eq(5)');

        var fv_name = $(this).attr('id');
        var fv_txt1 = fv_td1.text();
        var fv_val1 = 'undefined';
		
		var check_chbx = fv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = fv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = fv_td3.children().is('select');
        var check_inp3 = fv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = fv_td3.find('select');
        var find_inp3  = fv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                fv_txt3 = find_sel3.children('option:selected').text();
                fv_val3 = find_sel3.val();
            } else {
                fv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            fv_txt3 = find_inp3.val();
			fv_val3 = find_inp3.val();
        } else if (fv_td3.text() != '') {
            fv_txt3 = fv_td3.text();
			fv_val3 = 'undefined';
        } else {
            fv_txt3 = 'undefined';
			fv_val3 = 'undefined';
        }
		
		var check_sel4 = fv_td4.children().is('select');
        var check_inp4 = fv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = fv_td4.find('select');
        var find_inp4  = fv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                fv_txt4 = find_sel4.children('option:selected').text();
                fv_val4 = find_sel4.val();
            } else {
                fv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            fv_txt4 = find_inp4.val();
			fv_val4 = find_inp4.val();
        } else if (fv_td4.text() != '') {
            fv_txt4 = fv_td4.text();
			fv_val4 = 'undefined';
        } else {
            fv_txt4 = 'undefined';
			fv_val4 = 'undefined';
        }
			
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        fv_phscope[fv_name] 	 = fv_txt1 + '|' + fv_txt3 + '|' + fv_txt4;
		fv_phscope_vals[fv_name] = fv_val1 + '|' + fv_val3 + '|' + fv_val4;
		}
	
    });
	
	console.log(fv_phscope);
	console.log(fv_phscope_vals);
	
		$('#step8').hide();
        $('#step9').show();
		$(window).scrollTop(0);
	
});
//Заболевание и симптомы
$('#sendForm').on('click', function () {
    fv_symptoms = { };
	fv_symptoms_vals = { };

    var fvsym_tr = $('#fvSymptoms tr:visible');
    fvsym_tr.each(function() {

        var fv_td1 = $(this).children('td:eq(0)');
        var fv_td2 = $(this).children('td:eq(2)');
        var fv_td3 = $(this).children('td:eq(3)');
        var fv_td4 = $(this).children('td:eq(4)');
        var fv_td5 = $(this).children('td:eq(5)');

        var fv_name = $(this).attr('id');
        var fv_txt1 = fv_td1.text();
        var fv_val1 = 'undefined';
		
		var check_chbx = fv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = fv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = fv_td3.children().is('select');
        var check_inp3 = fv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = fv_td3.find('select');
        var find_inp3  = fv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                fv_txt3 = find_sel3.children('option:selected').text();
                fv_val3 = find_sel3.val();
            } else {
                fv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            fv_txt3 = find_inp3.val();
			fv_val3 = find_inp3.val();
        } else if (fv_td3.text() != '') {
            fv_txt3 = fv_td3.text();
			fv_val3 = 'undefined';
        } else {
            fv_txt3 = 'undefined';
			fv_val3 = 'undefined';
        }
		
		var check_sel4 = fv_td4.children().is('select');
        var check_inp4 = fv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = fv_td4.find('select');
        var find_inp4  = fv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                fv_txt4 = find_sel4.children('option:selected').text();
                fv_val4 = find_sel4.val();
            } else {
                fv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				fv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            fv_txt4 = find_inp4.val();
			fv_val4 = find_inp4.val();
        } else if (fv_td4.text() != '') {
            fv_txt4 = fv_td4.text();
			fv_val4 = 'undefined';
        } else {
            fv_txt4 = 'undefined';
			fv_val4 = 'undefined';
        }
			
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        fv_symptoms[fv_name] 	 = fv_txt1 + '|' + fv_txt3 + '|' + fv_txt4;
		fv_symptoms_vals[fv_name] = fv_val1 + '|' + fv_val3 + '|' + fv_val4;
		}
	
    });
	
	console.log(fv_symptoms);
	console.log(fv_symptoms_vals);
	
	
	$.ajax({
        url: '{ci helper="url" function="base_url"}booking/saveFistVisit'
        , type: 'POST'
        , data: {
                general_otchet:JSON.stringify($.extend(fv_otchet,fv_medication, fv_hazard, fv_pitanie)),
                general_otchet_vals:JSON.stringify($.extend(fv_otchet_vals,fv_medication_vals, fv_hazard_vals, fv_pitanie_vals)),
                fv:JSON.stringify($.extend(fv_abnormalities,fv_fscope,fv_cscope,fv_phscope,fv_symptoms)),
                fv_vals:JSON.stringify($.extend(fv_abnormalities_vals,fv_fscope_vals,fv_cscope_vals,fv_phscope_vals,fv_symptoms_vals))
            }
        , success: function (res) {
            $('#myModal').modal({
            show: true,
            //backdrop: false
            });
            $('#myModal').on('hidden.bs.modal', function (e) {
                window.location.replace('../');
            })
            setTimeout(function() { 
                window.location.replace('../');
            }, 20000)
            
        }
    });
	
});


	

			$('#back_step2').click(function () {
                $('#step2').hide();
                $('#step1').show();
				$(window).scrollTop(0);
				//window.location.hash='#step1';
            });
            $('#back_step3').click(function () {
                $('#step3').hide();
                $('#step2').show();
				//window.location.hash='#step2';
				$(window).scrollTop(0);
            });
			$('#back_step4').click(function () {
                $('#step4').hide();
                $('#step3').show();
				//window.location.hash='#step3';
				$(window).scrollTop(0);
            });
            $('#back_step5').click(function () {
                $('#step5').hide();
                $('#step4').show();
				$(window).scrollTop(0);
            });
            $('#back_step6').click(function () {
                $('#step6').hide();
                $('#step5').show();
				$(window).scrollTop(0);
            });
			$('#back_step7').click(function () {
                $('#step7').hide();
                $('#step6').show();
				$(window).scrollTop(0);
            });
			$('#back_step8').click(function () {
                $('#step8').hide();
                $('#step7').show();
				$(window).scrollTop(0);
            });
			$('#back_step9').click(function () {
                $('#step9').hide();
                $('#step8').show();
				$(window).scrollTop(0);
            });

    </script>
{/block}