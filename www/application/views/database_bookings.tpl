{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
    <h1 class="page-header">DATA BASES ALARM</h1>
    {if !empty($bookings)}
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">DATA BASES ALARM</div>

        <!-- Table -->
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Calendar</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Seats</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Confirmed</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody>
                {foreach $bookings as $val}
                <tr {if $val.reservation_confirmed != 1}class="danger"{/if}>
                    <td><a href="{ci helper="url" function="base_url"}admin/reservation.php?calendar_id={$val.calendar_id}">{$val.reservation_id}</a></td>
                    <td>{$val.calendar_title}</td>
                    <td>{$val.slot_date}</td>
                    <td>{$val.slot_time_from}</td>
                    <td>{$val.reservation_seats}</td>
                    <td>{$val.first_name} {$val.last_name}</td>
                    <td><a href="mailto:{$val.email}">{$val.email}</a></td>
                    <td>{if $val.reservation_confirmed==1}Yes{else}No{/if}</td>
                    <td><a href="{ci helper="url" function="base_url"}admin/reservation_detail.php?reservation_id={$val.reservation_id}">Detail</a></td>
                </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
        <p><a class="btn btn-default" href="javascript:window.history.back();" role="button"><i class="glyphicon glyphicon-step-backward"></i>Назад</a></p>
    {/if}
    
{/block}