{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}

	<div class="container-fluid page-header">
		<div class="col-sm-9 pl-0"><h1>{ci helper='language' function='lang' param1="alarm_form"}</h1></div>
		<div class="col-sm-3 a-right pr-0"><a class="btn btn-default" href="{ci helper="url" function="base_url"}alarm/exportExcell/" role="button"><i class="glyphicon glyphicon-plus"></i> {ci helper='language' function='lang' param1="exportExcell"}</a></div>
	</div>

    {if $isReportThisDay == FALSE}
    <p class="mb-30"><a class="btn btn-default" href="{ci helper="url" function="base_url"}alarm/add/" role="button"><i class="glyphicon glyphicon-plus"></i> {ci helper='language' function='lang' param1="alarm_add"}</a></p>
    {else}
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
    {/if}
    
	<div class="contBox">
    <div id="chartIndivid" style="width: 900px; height: 500px;"></div>
    <div id="chartWeaserIndivid" style="width: 900px; height: 500px;"></div>
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", { packages: ["corechart","line"] });
        google.setOnLoadCallback(drawChart);
            
        function drawChart() {
            //индивидуальный график
            var dataIndividual = $.ajax({
                url: "{ci helper="url" function="base_url"}/alarm/getIndividualReport",
                dataType:"json",
                async: false
                }).responseText;
               
            var data = new google.visualization.DataTable();    
            
            data.addColumn('string', '{ci helper='language' function='lang' param1="date"}');
            data.addColumn('number', '{ci helper='language' function='lang' param1="behavioural"}');
            data.addColumn('number', '{ci helper='language' function='lang' param1="neurologic"}');
            data.addColumn('number', '{ci helper='language' function='lang' param1="autonomic_nervous_system"}');
            data.addColumn('number', '{ci helper='language' function='lang' param1="respiratory_system"}');
            data.addColumn('number', '{ci helper='language' function='lang' param1="digestive_system"}');
            data.addColumn('number', '{ci helper='language' function='lang' param1="urinary_system"}');
            data.addRows(JSON.parse(dataIndividual));
            
            var optionsIndivid = {
                title: '{ci helper='language' function='lang' param1="requests_within_7_days"}',
                width: 600,
                height: 400,
                legend: { position: 'top', maxLines: 7 },
                bar: { groupWidth: '75%' },
                isStacked: true
            };
            var chart = new google.visualization.BarChart(document.getElementById('chartIndivid'));
            chart.draw(data, optionsIndivid);
            //TODO: добавить график погоды
            
            var dataWeaserIndividual = $.ajax({
                url: "{ci helper="url" function="base_url"}/alarm/getWeaserIndividualReport",
                dataType:"json",
                async: false
                }).responseText;
               
            var data = new google.visualization.DataTable();    
            
            data.addColumn('string', '{ci helper='language' function='lang' param1="date"}');
            data.addColumn('number', '{ci helper='language' function='lang' param1="weather"}');
            data.addRows(JSON.parse(dataWeaserIndividual));
            
            var optionsWeaserIndivid = {
                title: '{ci helper='language' function='lang' param1="rating_forecast_last_7_days"}',
                width: 900,
                height: 500,
            };
            var chart = new google.charts.Line(document.getElementById('chartWeaserIndivid'));
            chart.draw(data, optionsWeaserIndivid);
            
            {if $isReportThisDay == TRUE}
            var dataGeneral = $.ajax({
                url: "{ci helper="url" function="base_url"}/alarm/getGeneralReport",
                dataType:"json",
                async: false
                }).responseText;
            
            //общий график
            var generalReport = new google.visualization.DataTable();
            generalReport.addColumn('string','{ci helper='language' function='lang' param1="date"}');
            generalReport.addColumn('number', 'Anonym1');
            generalReport.addColumn('number', 'Anonym2');
            generalReport.addColumn('number', 'Anonym3');
            generalReport.addColumn('number', 'Anonym4');
            generalReport.addColumn('number', '{ci helper='language' function='lang' param1="you"}');
            generalReport.addRows(JSON.parse(dataGeneral));

            var options = {
                title: '{ci helper='language' function='lang' param1="requests_within_7_days"}',
                vAxis: { title: "{ci helper='language' function='lang' param1="ssore"}" },
                hAxis: { title: "{ci helper='language' function='lang' param1="date"}" },
                seriesType: "bars",
            };
            
            var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
            chart.draw(generalReport, options);
            {/if}
        }
    </script>
    </div>
{/block}