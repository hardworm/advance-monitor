{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
    <h1 class="page-header">DATA BASES ALARM</h1>
    {if !empty($alarms)}
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">DATA BASES ALARM</div>

        <!-- Table -->
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Behavior</th>
                    <th>Neurologic</th>
                    <th>Vegetative</th>
                    <th>Respiratory</th>
                    <th>Alimentary</th>
                    <th>Urinary</th>
                    <th>Total score</th>
                    <th>Weaser score</th>
                    <th>GS</th>
                    <th>Moon<br>illumination</th>
                    <th>Moon<br>Age</th>
                </tr>
            </thead>
            <tbody>
                {foreach $alarms as $val}
                <tr {if $val.is_read != 1}class="danger"{/if}>
                    <td><a href="{ci helper="url" function="base_url"}/database/alarm/{$val.id}">{$val.id}</a></td>
                    <td>{$val.date}</td>
                    <td>{$val.behavior|default:0}</td>
                    <td>{$val.neurologic|default:0}</td>
                    <td>{$val.vegetative|default:0}</td>
                    <td>{$val.respiratory|default:0}</td>
                    <td>{$val.alimentary|default:0}</td>
                    <td>{$val.urinary|default:0}</td>
                    <td>{$val.total_score|default:0}</td>
                    <td>{$val.weaser_score|default:0}</td>
                    <td>{$val.kp|default:0}</td>
                    <td>{$val.illumination|default:0}%</td>
                    <td>{$val.age|default:0}</td>
                </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
        {$pagination}
        <p><a class="btn btn-default" href="javascript:window.history.back();" role="button"><i class="glyphicon glyphicon-step-backward"></i>Назад</a></p>
    {/if}
    
{/block}