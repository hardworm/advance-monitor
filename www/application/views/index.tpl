<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">

        <title>{block name=title}{/block}</title>

        <!-- Bootstrap core CSS -->
        <link href="{ci helper="url" function="base_url"}dist/css/bootstrap.min.css" rel="stylesheet">
		<link href="{ci helper="url" function="base_url"}dist/css/style.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
        <script src="{ci helper="url" function="base_url"}/dist/js/bootstrap.min.js"></script>
        
        <!-- Custom styles for this template -->

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="{ci helper="url" function="base_url"}js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="{ci helper="url" function="base_url"}js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" id="amTn" role="navigation">
            <div class="container-fluid a-center">
                <a href="{ci helper="url" function="base_url"}"><img src="/dist/images/logo2.png" alt="Advance MONITOR"></a>
            </div>
        </nav>

        <div class="container-fluid" id="amCs">
            <div class="row">
                <div class="col-sm-3 amsidebar">
                    <nav class="navbar navbar-default">
                      <div class="container-fluid">
                        <div class="navbar-header">
                        <img src="/dist/images/logo2.png" class="visible-xs-inline ml-15" height="48" alt="Advance MONITOR">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>

                        </div>

                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                      <ul class="nav nav-sidebar">
                        <li {if $this->data.body_class == "diagnosis"} class="active"{/if}>
                            <a href="{ci helper="url" function="base_url"}diagnosis">{ci helper='language' function='lang' param1="diagnosis"}</a></li>
                        <li {if $this->data.body_class == "info" OR $this->data.body_class == "dashboard"} class="active"{/if}>
                            <a href="{ci helper="url" function="base_url"}info">{ci helper='language' function='lang' param1="information"}</a></li>
                        <li {if $this->data.body_class == "metods"} class="active"{/if}>
                            <a href="{ci helper="url" function="base_url"}metods">{ci helper='language' function='lang' param1="methods"}</a></li>
                        {* <li><a href="#">Данные</a></li>*}
                        <li {if $this->data.body_class == "alarm"} class="active"{/if}>
                            <a href="{ci helper="url" function="base_url"}alarm">{ci helper='language' function='lang' param1="alarm_form"}</a></li>
                        <li {if $this->data.body_class == "booking"} class="active"{/if}>
                            <a href="{ci helper="url" function="base_url"}booking">{ci helper='language' function='lang' param1="book_a_course"}</a></li>
						{if $isAdmin == 1}    
                        <li {if $this->data.body_class == "database"} class="active"{/if}>
                            <a href="{ci helper="url" function="base_url"}database/">DATA BASE</a></li>
                        <li {if $this->data.body_class == "auth"} class="active"{/if}>
                            <a href="{ci helper="url" function="base_url"}auth/">{ci helper='language' function='lang' param1="users"}</a></li>
                    {/if}
                        <li><a href="{ci helper="url" function="base_url"}auth/logout">{ci helper='language' function='lang' param1="log_out"}</a></li>
					
                    </ul>
                </div>
              </div>
            </nav>
                    
        </div>
     <div class="col-sm-9" id="main">
            {block name=main}{/block}
    </div>
    </div>
  </div>
<hr>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        
        <script src="{ci helper="url" function="base_url"}/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>