{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
    <div class="container-fluid page-header">
        <div class="col-xs-12 pl-0"><h1>{ci helper='language' function='lang' param1="diagnosis"}</h1></div>
    </div>

    <div id="step1" class="contBox">
        <div class="col-xs-12 col-md-8 col-sm-10 pl-0">
            <table class="table table-striped">
                <tr>
                    <td>{ci helper='language' function='lang' param1="cerebral_palsy"}</td>
                    <td><input type="checkbox" id="cerebral_palsy" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="post_traumatic_brain_injury"}</td>
                    <td><input type="checkbox" id="post_traumatic_brain_injury" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="spinal_injury"}</td>
                    <td><input type="checkbox" id="spinal_injury" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="retardation"}</td>
                    <td><input type="checkbox" id="retardation" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="epilepsy"}</td>
                    <td><input type="checkbox" id="epilepsy" class="checkbox_step1" value="1"></td>
                </tr>

                <tr>
                    <td>{ci helper='language' function='lang' param1="autism"}</td>
                    <td><input type="checkbox" id="autism" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="asperger_syndrome"}</td>
                    <td><input type="checkbox" id="asperger_syndrome" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="autistopodobnoe_disease"}</td>
                    <td><input type="checkbox" id="autistopodobnoe_disease" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="microcephaly"}</td>
                    <td><input type="checkbox" id="microcephaly" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="hydrocephalus"}</td>
                    <td><input type="checkbox" id="hydrocephalus" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="genetic_disease"}</td>
                    <td><input type="checkbox" id="genetic_disease" class="checkbox_step1" value="1"></td>
                </tr>
                <tr>
                    <td>{ci helper='language' function='lang' param1="other"}</td>
                    <td><input type="checkbox" id="other" class="checkbox_step1" value="1"></td>
                </tr>
            </table>
            <a href="javascript:window.history.back();" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> {ci helper='language' function='lang' param1="back"}</a>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 1</button>
            <button id="next_step1" type="button" class="btn btn-success">{ci helper='language' function='lang' param1="next"} <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div><!--/row-->

    <div id="step2" class="contBox" style="display:none">
        <h2>{ci helper='language' function='lang' param1="h2"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="table2">
                <tr class="cerebral_palsy" id="cerebral_palsy1" style="display:none">
                    <td>{ci helper='language' function='lang' param1="cerebral_palsy"}</td>
                    <td>{ci helper='language' function='lang' param1="spastic_type"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="tetraplegia">{ci helper='language' function='lang' param1="tetraplegia"}</option>
                            <option value="paraplegia">{ci helper='language' function='lang' param1="paraplegia"}</option>
                            <option value="hemiplegia">{ci helper='language' function='lang' param1="hemiplegia"}</option>
                            <option value="spastic_diplegia">{ci helper='language' function='lang' param1="spastic_diplegia"}</option>
                            <option value="double_hemiplegia">{ci helper='language' function='lang' param1="double_hemiplegia"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="cerebral_palsy" id="cerebral_palsy2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="cerebral_palsy"}</td>
                    <td>{ci helper='language' function='lang' param1="dyskineticl_type"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="hyperkinetic">{ci helper='language' function='lang' param1="hyperkinetic"}</option>
                            <option value="dystonic">{ci helper='language' function='lang' param1="dystonic"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="cerebral_palsy" id="cerebral_palsy3" style="display:none">
                    <td>{ci helper='language' function='lang' param1="cerebral_palsy"}</td>
                    <td>{ci helper='language' function='lang' param1="atonic_type"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="atonic-astatic_form">{ci helper='language' function='lang' param1="atonic-astatic_form"}</option>
                            <option value="congenital_cerebellar_ataxia">{ci helper='language' function='lang' param1="congenital_cerebellar_ataxia"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                <tr class="cerebral_palsy" id="cerebral_palsy4" style="display:none">
                    <td>{ci helper='language' function='lang' param1="cerebral_palsy"}</td>
                    <td>{ci helper='language' function='lang' param1="multiform"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                <tr class="cerebral_palsy" id="cerebral_palsy5" style="display:none">
                    <td>{ci helper='language' function='lang' param1="cerebral_palsy"}</td>
                    <td>{ci helper='language' function='lang' param1="unspecified"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><textarea class="form-control"></textarea></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr class="post_traumatic_brain_injury" id="post_traumatic_brain_injury" style="display:none">
                    <td>{ci helper='language' function='lang' param1="post_traumatic_brain_injury"}</td>
                    <td>{ci helper='language' function='lang' param1="tetraparesis"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="post_traumatic_brain_injury" id="post_traumatic_brain_injury2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="post_traumatic_brain_injury"}</td>
                    <td>{ci helper='language' function='lang' param1="hemiparesis"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="post_traumatic_brain_injury" id="post_traumatic_brain_injury3" style="display:none">
                    <td>{ci helper='language' function='lang' param1="post_traumatic_brain_injury"}</td>
                    <td>{ci helper='language' function='lang' param1="diplegia"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="post_traumatic_brain_injury" id="post_traumatic_brain_injury4" style="display:none">
                    <td>{ci helper='language' function='lang' param1="post_traumatic_brain_injury"}</td>
                    <td>{ci helper='language' function='lang' param1="other"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><textarea class="form-control"></textarea></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr class="spinal_injury" id="spinal_injury" style="display:none">
                    <td>{ci helper='language' function='lang' param1="spinal_injury"}</td>
                    <td>{ci helper='language' function='lang' param1="tetraparesis"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="spinal_injury" id="spinal_injury2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="spinal_injury"}</td>
                    <td>{ci helper='language' function='lang' param1="hemiparesis"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="spinal_injury" id="spinal_injury3" style="display:none">
                    <td>{ci helper='language' function='lang' param1="spinal_injury"}</td>
                    <td>{ci helper='language' function='lang' param1="diplegia"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="spinal_injury" id="spinal_injury4" style="display:none">
                    <td>{ci helper='language' function='lang' param1="spinal_injury"}</td>
                    <td>{ci helper='language' function='lang' param1="other"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><textarea class="form-control"></textarea></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr class="retardation" id="retardation" style="display:none">
                    <td>{ci helper='language' function='lang' param1="retardation"}</td>
                    <td>{ci helper='language' function='lang' param1="motor"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="retardation" id="retardation2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="retardation"}</td>
                    <td>{ci helper='language' function='lang' param1="cognitive"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="retardation" id="retardation3" style="display:none">
                    <td>{ci helper='language' function='lang' param1="retardation"}</td>
                    <td>{ci helper='language' function='lang' param1="speech"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                
                <tr class="epilepsy" id="epilepsy" style="display:none">
                    <td>{ci helper='language' function='lang' param1="epilepsy"}</td>
                    <td>Grand Mal</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="mixed">{ci helper='language' function='lang' param1="mixed"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="epilepsy" id="epilepsy2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="epilepsy"}</td>
                    <td>Petit Mal</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="mixed">{ci helper='language' function='lang' param1="mixed"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="epilepsy" id="epilepsy3" style="display:none">
                    <td>{ci helper='language' function='lang' param1="epilepsy"}</td>
                    <td>{ci helper='language' function='lang' param1="focal"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="mixed">{ci helper='language' function='lang' param1="mixed"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="epilepsy" id="epilepsy4" style="display:none">
                    <td>{ci helper='language' function='lang' param1="epilepsy"}</td>
                    <td>{ci helper='language' function='lang' param1="west_syndrome"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="mixed">{ci helper='language' function='lang' param1="mixed"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="epilepsy" id="epilepsy5" style="display:none">
                    <td>{ci helper='language' function='lang' param1="epilepsy"}</td>
                    <td>{ci helper='language' function='lang' param1="lennox-gastaut"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><select class="form-control">
                            <option value="day">{ci helper='language' function='lang' param1="day"}</option>
                            <option value="night">{ci helper='language' function='lang' param1="night"}</option>
                            <option value="mixed">{ci helper='language' function='lang' param1="mixed"}</option>
                        </select></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="epilepsy" id="epilepsy6" style="display:none">
                    <td>{ci helper='language' function='lang' param1="epilepsy"}</td>
                    <td>{ci helper='language' function='lang' param1="other"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><textarea class="form-control"></textarea></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr class="autism" id="autism" style="display:none">
                    <td>{ci helper='language' function='lang' param1="autism"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                <tr class="asperger_syndrome" id="asperger_syndrome" style="display:none">
                    <td>{ci helper='language' function='lang' param1="asperger_syndrome"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                <tr class="autistopodobnoe_disease" id="autistopodobnoe_disease" style="display:none">
                    <td>{ci helper='language' function='lang' param1="autistopodobnoe_disease"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                <tr class="microcephaly" id="microcephaly" style="display:none">
                    <td>{ci helper='language' function='lang' param1="microcephaly"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                <tr class="hydrocephalus" id="hydrocephalus" style="display:none">
                    <td>{ci helper='language' function='lang' param1="hydrocephalus"}</td>
                    <td>{ci helper='language' function='lang' param1="decompensated"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="hydrocephalus" id="hydrocephalus2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="hydrocephalus"}</td>
                    <td>{ci helper='language' function='lang' param1="subcompensated"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="hydrocephalus" id="hydrocephalus3" style="display:none">
                    <td>{ci helper='language' function='lang' param1="hydrocephalus"}</td>
                    <td>{ci helper='language' function='lang' param1="compensated"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr class="genetic_disease" id="genetic_disease" style="display:none">
                    <td>{ci helper='language' function='lang' param1="genetic_disease"}</td>
                    <td>{ci helper='language' function='lang' param1="amgelman_syndrome"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="genetic_disease" id="genetic_disease2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="genetic_disease"}</td>
                    <td>{ci helper='language' function='lang' param1="downs_syndrome"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                <tr class="genetic_disease" id="genetic_disease2" style="display:none">
                    <td>{ci helper='language' function='lang' param1="genetic_disease"}</td>
                    <td>{ci helper='language' function='lang' param1="other"}</td>
                    <td><input type="checkbox" value="1"></td>
                    <td><textarea class="form-control"></textarea></td>
                    <td><select class="form-control">
                            <option value="high">{ci helper='language' function='lang' param1="high"}</option>
                            <option value="middle">{ci helper='language' function='lang' param1="middle"}</option>
                            <option value="low">{ci helper='language' function='lang' param1="low"}</option>
                        </select></td>
                    <td></td>
                </tr>
                
                <tr class="other" id="other" style="display:none">
                    <td>{ci helper='language' function='lang' param1="other"}</td>
                    <td></td>
                    <td><input type="checkbox" value="1"></td>
                    <td><textarea class="form-control"></textarea></td>
                    <td></td>
                    <td></td>
                </tr>
                
            </table>
            <button id="back_step2" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> {ci helper='language' function='lang' param1="back"}</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 2</button>
            <button id="sendForm" type="submit" class="btn btn-success">{ci helper='language' function='lang' param1="send"}</button>

        </div>
    </div><!--/row-->
    
    <script>
            $(".checkbox_step1").change(function (e) {
                if ($(this).prop("checked")) {
                    $("." + e.target.id).show();
                } else {
                    $("." + e.target.id).hide();
                }
            });

            $('#next_step1').click(function () {
                $('#step1').hide();
                $('#step2').show();
            });

            $('#back_step2').click(function () {
                $('#step2').hide();
                $('#step1').show();
            });

            $('#sendForm').on('click', function () {
				diagnos = { };
                var _ctr = $('#table2 tr:visible');
                _ctr.each(function () {
                    var _tc = $(this).find('td:eq(2) > input[type=checkbox]');
					var _txtd = $(this).children('td:first').text();
					var _txtn = $(this).attr('id');
					
					var _txtde1 = $(this).children('td:eq(0)');
					var _ctds1  = _txtde1.children().is('select');
					var _ctdi1  = _txtde1.children().is('textarea, input:text');
                    var _ctd1   = _txtde1.find('select');
					var _cti1   = _txtde1.find('textarea, input:text');
					
						if (_ctds1==true) {
							if (!_ctd1.attr("multiple")) {
								_txtd1 = _ctd1.children('option:selected').text();
							} else {
								_txtd1 = _ctd1.children('option:selected').map(function () {
									return $(this).text();
								}).get();
							}
						} else if(_ctdi1==true && _cti1.val()!='') {
								_txtd1 = _cti1.val();
							} else if (_txtde1.text() != ''){
								_txtd1 = _txtde1.text();
							} else {
								_txtd3 = 'undefined';
							}
						
						
					var _txtde2 = $(this).children('td:eq(1)');
					var _ctds2  = _txtde2.children().is('select');
					var _ctdi2  = _txtde2.children().is('textarea, input:text');
                    var _ctd2   = _txtde2.find('select');
					var _cti2   = _txtde2.find('textarea, input:text');
					
						if (_ctds2==true) {
							if (!_ctd2.attr("multiple")) {
								_txtd2 = _ctd2.children('option:selected').text();
							} else {
								_txtd2 = _ctd2.children('option:selected').map(function () {
									return $(this).text();
								}).get();
							}
						} else if(_ctdi2==true && _cti2.val()!='') {
								_txtd2 = _cti2.val();
							} else if (_txtde2.text() != ''){
								_txtd2 = _txtde2.text();
							} else {
								_txtd2 = 'undefined';
							}
						
						
					var _txtde3 = $(this).children('td:eq(3)');
					var _ctds3  = _txtde3.children().is('select');
					var _ctdi3  = _txtde3.children().is('textarea, input:text');
                    var _ctd3   = _txtde3.find('select');
					var _cti3   = _txtde3.find('textarea, input:text');
					
						if (_ctds3==true) {
							if (!_ctd3.attr("multiple")) {
								_txtd3 = _ctd3.children('option:selected').text();
							} else {
								_txtd3 = _ctd3.children('option:selected').map(function () {
									return $(this).text();
								}).get();
							}
						} else if(_ctdi3==true && _cti3.val()!='') {
								_txtd3 = _cti3.val();
							} else if (_txtde3.text() != ''){
								_txtd3 = _txtde3.text();
							} else {
								_txtd3 = 'undefined';
							}
						
					var _txtde4 = $(this).children('td:eq(4)');
					var _ctds4  = _txtde4.children().is('select');
					var _ctdi4  = _txtde4.children().is('textarea, input:text');
                    var _ctd4   = _txtde4.find('select');
					var _cti4   = _txtde4.find('textarea, input:text');
					
						if (_ctds4==true) {
							if (!_ctd4.attr("multiple")) {
								_txtd4 = _ctd4.children('option:selected').text();
							} else {
								_txtd4 = _ctd4.children('option:selected').map(function () {
									return $(this).text();
								}).get();
							}
						} else if(_ctdi4==true && _cti4.val()!='') {
								_txtd4 = _cti4.val();
							} else if (_txtde4.text() != ''){
								_txtd4 = _txtde4.text();
							} else {
								_txtd4 = 'undefined';
							}
						
                    if (_tc.is(':checked')) {
						diagnos[_txtn] =_txtd1 +'|'+ _txtd2 + '|' + _txtd3 + '|' + _txtd4;
                    }
                });
				console.log(diagnos);
				$.ajax({
                    url: '{ci helper="url" function="base_url"}diagnosis/save'
                    , type: 'POST'
                    , data: {
                            diagnos:JSON.stringify(diagnos)
                        }
                    , success: function (res) {
                        alert('{ci helper='language' function='lang' param1="send_and"}');
                        window.location.replace('{ci helper="url" function="base_url"}booking/fistVisit/');
                    }
                });
				
            });
			
    </script>
{/block}