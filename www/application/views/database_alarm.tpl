{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {nocache}
    {$title}
    {/nocache}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
    {nocache}
    <div class="blog-post">
        <h2 class="blog-post-title">Report ID {$alarm.id}</h2>
        <p class="blog-post-meta">{$alarm.date}</p>
        {foreach $alarm.json_text as $val}
            {$val|replace:"|":"  "|replace:"undefined":""}<br>
        {/foreach}
    <div>
    {/nocache}
    <p><a class="btn btn-default" href="javascript:window.history.back();" role="button"><i class="glyphicon glyphicon-step-backward"></i>Назад</a></p>
{/block}