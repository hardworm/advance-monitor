{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {nocache}
    {$article->title}
    {/nocache}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
    {nocache}

    <div class="container-fluid page-header">
        <div class="col-xs-12 pl-0"><h1>{$article->title}</h1></div>
    </div>

    <div class="contBox">
    <h4>{$article->date}</h4>
    <div class="blog-post">
        {$article->desc}
    <div>
    {/nocache}
    <p><a class="btn btn-default" href="javascript:window.history.back();" role="button"><i class="glyphicon glyphicon-step-backward"></i>Назад</a></p>
    </div>
{/block}