{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}

{* This block is defined in the master.php template *}
{block name=main}
<div class="container-fluid page-header">
        <div class="col-xs-12 pl-0"><h1>{ci helper='language' function='lang' param1="h1"}</h1></div>
    </div>

    <div class="contBox" id="step1">
        <h2>{ci helper='language' function='lang' param1="h2"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="rvOtchet">
                <tr id="contact_with_advance">
                    <td>{ci helper='language' function='lang' param1="contact_with_advance"}</td>
                    <td colspan="2"><select name="contact_with_advance" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="hours_recommended">
                    <td>{ci helper='language' function='lang' param1="hours_recommended"}</td>
                    <td colspan="2"><input name="hours_recommended" title="{ci helper='language' function='lang' param1="only_numeric_values"}" onkeyup="checkCurr(this)" type="text" class="form-control" id="hyperactivated" value="0"></td>
                </tr>
                <tr id="hours_paid_daily">
                    <td>{ci helper='language' function='lang' param1="hours_paid_daily"}</td>
                    <td colspan="2"><input name="hours_paid_daily" title="{ci helper='language' function='lang' param1="only_numeric_values"}" onkeyup="checkCurr(this)" type="text" class="form-control" value="0"></td>
                </tr>

                <tr id="reason_reduce_operating_time">
                    <td>{ci helper='language' function='lang' param1="reason_reduce_operating_time"}</td>
                    <td><select name="reason_reduce_operating_time" class="form-control" multiple="multiple">
                            <option value="lack_of_time">{ci helper='language' function='lang' param1="lack_of_time"}</option>
                            <option value="difficulties_build_schedule">{ci helper='language' function='lang' param1="difficulties_build_schedule"}</option>
                            <option value="resistance_of_child">{ci helper='language' function='lang' param1="resistance_of_child"}</option>
                            <option value="disease">{ci helper='language' function='lang' param1="disease"}</option>
                            <option value="holidays">{ci helper='language' function='lang' param1="holidays"}</option>
                            <option value="technical_difficulties">{ci helper='language' function='lang' param1="technical_difficulties"}</option>
                            <option value="other">{ci helper='language' function='lang' param1="other"}</option>
                        </select>
                    </td>
                    <td><textarea class="form-control" name="other_text"></textarea></td>
                </tr>
                <tr id="IMS">
                    <td>{ci helper='language' function='lang' param1="ims"}</td>
                    <td colspan="2"><select name="IMS" class="form-control">
                            <option value="yes">{ci helper='language' function='lang' param1="yes"}</option>
                            <option value="no">{ci helper='language' function='lang' param1="no"}</option>
                        </select>
                    </td>
                </tr>
                <tr id="main_target">
                    <td>{ci helper='language' function='lang' param1="main_target"}</td>
                    <td colspan="2"><textarea class="form-control" name="main_target"></textarea></td>
                </tr>
            </table>
            <a href="javascript:window.history.back();" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</a>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 1</button>
            <button id="next_step1" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div><!--/row-->
    
    <div class="contBox" id="step2" style="display:none">
        <h2>{ci helper='language' function='lang' param1="example_menu"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="rvpitanie">
                <tr id="day1_breakfast">
                    <td>{ci helper='language' function='lang' param1="day1_breakfast"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day1_lunch">
                    <td>{ci helper='language' function='lang' param1="day1_lunch"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day1_afternoon_snack">
                    <td>{ci helper='language' function='lang' param1="day1_afternoon_snack"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day1_dinner">
                    <td>{ci helper='language' function='lang' param1="day1_dinner"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day1_between_meals">
                    <td>{ci helper='language' function='lang' param1="day1_between_meals"}</td>
                    <td><textarea class="form-control" name="time"></textarea></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                
                <tr id="day2_breakfast">
                    <td>{ci helper='language' function='lang' param1="day2_breakfast"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day2_lunch">
                    <td>{ci helper='language' function='lang' param1="day2_lunch"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day2_afternoon_snack">
                    <td>{ci helper='language' function='lang' param1="day2_afternoon_snack"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day2_dinner">
                    <td>{ci helper='language' function='lang' param1="day2_dinner"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day2_between_meals">
                    <td>{ci helper='language' function='lang' param1="day2_between_meals"}</td>
                    <td><textarea class="form-control" name="time"></textarea></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                
                <tr id="day3_breakfast">
                    <td>{ci helper='language' function='lang' param1="day3_breakfast"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day3_lunch">
                    <td>{ci helper='language' function='lang' param1="day3_lunch"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day3_afternoon_snack">
                    <td>{ci helper='language' function='lang' param1="day3_afternoon_snack"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day3_dinner">
                    <td>{ci helper='language' function='lang' param1="day3_dinner"}</td>
                    <td><input name="time" class="form-control" type="time" value="" /></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="day3_between_meals">
                    <td>{ci helper='language' function='lang' param1="day3_between_meals"}</td>
                    <td><textarea class="form-control" name="time"></textarea></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                <tr id="end_between_meals">
                    <td>{ci helper='language' function='lang' param1="end_between_meals"}</td>
                    <td><textarea class="form-control" name="time"></textarea></td>
                    <td colspan="2"><textarea class="form-control" name="food"></textarea></td>
                </tr>
                
            </table>
            <button id="back_step2" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 2</button>
            <button id="next_step2" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>

        </div>
    </div><!--/row-->
	
<div class="contBox" id="step3" style="display:none">
    <h2>{ci helper='language' function='lang' param1="structural_disorders"}</h2>
    <div class="col-xs-12 pl-0">
      <table class="table table-striped" id="rvViolations">
        <tr id="thorax">
          <td>{ci helper='language' function='lang' param1="thorax"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['thorax'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['thorax'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['thorax'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['thorax'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['thorax'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
                <option {if $last['thorax'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="cervical_spine">
          <td>{ci helper='language' function='lang' param1="cervical_spine"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['cervical_spine'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['cervical_spine'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['cervical_spine'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['cervical_spine'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['cervical_spine'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
              <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                  {for $j=0 to 9}
                      <option {if $last['cervical_spine'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                  {/for}
              </select>
          </td>
          <td></td>
        </tr>
        <tr id="thoracic_spine">
          <td>{ci helper='language' function='lang' param1="thoracic_spine"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['thoracic_spine'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['thoracic_spine'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['thoracic_spine'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['thoracic_spine'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['thoracic_spine'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
              <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                  {for $j=0 to 9}
                      <option {if $last['thoracic_spine'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                  {/for}
              </select>
          </td>
          <td></td>
        </tr>
        <tr id="lumbar_spine">
          <td>{ci helper='language' function='lang' param1="lumbar_spine"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['lumbar_spine'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['lumbar_spine'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['lumbar_spine'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['lumbar_spine'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['lumbar_spine'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option {if $last['lumbar_spine'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="belly">
          <td>{ci helper='language' function='lang' param1="belly"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['belly'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['belly'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['belly'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['belly'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['belly'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option {if $last['belly'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="pelvis">
          <td>{ci helper='language' function='lang' param1="pelvis"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['pelvis'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['pelvis'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['pelvis'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['pelvis'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['pelvis'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option {if $last['pelvis'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="hip">
          <td>{ci helper='language' function='lang' param1="hip"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['hip'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['hip'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['hip'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['hip'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['hip'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option  {if $last['hip'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="legs">
          <td>{ci helper='language' function='lang' param1="legs"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['legs'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['legs'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['legs'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['legs'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['legs'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option {if $last['legs'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="foot">
          <td>{ci helper='language' function='lang' param1="foot"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['foot'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['foot'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['foot'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['foot'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['foot'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option {if $last['foot'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="shoulder-blade">
          <td>{ci helper='language' function='lang' param1="shoulder-blade"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['shoulder-blade'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['shoulder-blade'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['shoulder-blade'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['shoulder-blade'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['shoulder-blade'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option {if $last['shoulder-blade'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="upper_arm">
          <td>{ci helper='language' function='lang' param1="upper_arm"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['upper_arm'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['upper_arm'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['upper_arm'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['upper_arm'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['upper_arm'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option  {if $last['upper_arm'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="hands">
          <td>{ci helper='language' function='lang' param1="hands"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['hands'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['hands'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['hands'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['hands'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['hands'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option  {if $last['hands'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        <tr id="head">
          <td>{ci helper='language' function='lang' param1="head"}</td>
          <td></td>
          <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
          <td><select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
              <option {if $last['head'][1] == "severe"}selected{/if} value="severe">{ci helper='language' function='lang' param1="severe"}</option>
              <option {if $last['head'][1] == "poor"}selected{/if} value="poor">{ci helper='language' function='lang' param1="poor"}</option>
              <option {if $last['head'][1] == "moderate"}selected{/if} value="moderate">{ci helper='language' function='lang' param1="moderate"}</option>
              <option {if $last['head'][1] == "mild"}selected{/if} value="mild">{ci helper='language' function='lang' param1="mild"}</option>
              <option {if $last['head'][1] == "normal"}selected{/if} value="normal">{ci helper='language' function='lang' param1="normal"}</option>
            </select></td>
          <td>
            <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
              {for $j=0 to 9}
              <option {if $last['head'][2] == $j}selected{/if} value="{$j}">{$j}</option>
              {/for}
            </select>
          </td>
          <td></td>
        </tr>
        
        
        
      </table>
      <button id="back_step3" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
      <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 3</button>
      <button id="next_step3" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>

    </div>
  </div><!--/row-->
  	
 <div class="contBox" id="step4" style="display:none">
        <h2>{ci helper='language' function='lang' param1="funtsionalnost_sphere"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="rvFscope">
                
                <tr id="holding_head">
                    <td>{ci helper='language' function='lang' param1="holding_head"}</td>
                    <td></td>
                    <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select   data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['holding_head'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['holding_head'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['holding_head'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['holding_head'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['holding_head'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['holding_head'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="coordinated_limb_movements">
                    <td>{ci helper='language' function='lang' param1="coordinated_limb_movements"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['coordinated_limb_movements'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['coordinated_limb_movements'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['coordinated_limb_movements'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['coordinated_limb_movements'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['coordinated_limb_movements'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['coordinated_limb_movements'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="hand_movements">
                    <td>{ci helper='language' function='lang' param1="hand_movements"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['hand_movements'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['hand_movements'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['hand_movements'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['hand_movements'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['hand_movements'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['hand_movements'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="leg_movements">
                    <td>{ci helper='language' function='lang' param1="leg_movements"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['leg_movements'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['leg_movements'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['leg_movements'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['leg_movements'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['leg_movements'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['leg_movements'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="trunk_movements">
                    <td>{ci helper='language' function='lang' param1="trunk_movements"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['trunk_movements'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['trunk_movements'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['trunk_movements'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['trunk_movements'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['trunk_movements'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['trunk_movements'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="turn">
                    <td>{ci helper='language' function='lang' param1="turn"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['turn'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['turn'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['turn'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['turn'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['turn'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['turn'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="sit_up_unaided">
                    <td>{ci helper='language' function='lang' param1="sit_up_unaided"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['sit_up_unaided'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['sit_up_unaided'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['sit_up_unaided'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['sit_up_unaided'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['sit_up_unaided'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['sit_up_unaided'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="keep_balance">
                    <td>{ci helper='language' function='lang' param1="keep_balance"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['keep_balance'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['keep_balance'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['keep_balance'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['keep_balance'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['keep_balance'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['keep_balance'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="creep">
                    <td>{ci helper='language' function='lang' param1="creep"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['creep'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['creep'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['creep'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['creep'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['creep'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['creep'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="stand">
                    <td>{ci helper='language' function='lang' param1="stand"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['stand'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['stand'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['stand'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['stand'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['stand'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option   {if $last['stand'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="walk">
                    <td>{ci helper='language' function='lang' param1="walk"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['walk'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['walk'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['walk'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['walk'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['walk'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['walk'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="run">
                    <td>{ci helper='language' function='lang' param1="run"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['run'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['run'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['run'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['run'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['run'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['run'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="vocalize">
                    <td>{ci helper='language' function='lang' param1="vocalize"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['vocalize'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['vocalize'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['vocalize'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['vocalize'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['vocalize'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['vocalize'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="speak">
                    <td>{ci helper='language' function='lang' param1="speak"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['speak'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['speak'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['speak'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['speak'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['speak'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['speak'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="gape">
                    <td>{ci helper='language' function='lang' param1="gape"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['gape'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['gape'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['gape'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['gape'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['gape'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['gape'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="laugh">
                    <td>{ci helper='language' function='lang' param1="laugh"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['laugh'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['laugh'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['laugh'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['laugh'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['laugh'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['laugh'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="sing">
                    <td>{ci helper='language' function='lang' param1="sing"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['sing'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['sing'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['sing'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['sing'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['sing'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['sing'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="articulation_and_sound">
                    <td>{ci helper='language' function='lang' param1="articulation_and_sound"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['articulation_and_sound'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['articulation_and_sound'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['articulation_and_sound'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['articulation_and_sound'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['articulation_and_sound'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['articulation_and_sound'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="sight">
                    <td>{ci helper='language' function='lang' param1="sight"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['sight'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['sight'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['sight'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['sight'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['sight'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['sight'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="hearing">
                    <td>{ci helper='language' function='lang' param1="hearing"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['hearing'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['hearing'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['hearing'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['hearing'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['hearing'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['hearing'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="chew">
                    <td>{ci helper='language' function='lang' param1="chew"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['chew'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['chew'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['chew'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['chew'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['chew'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['chew'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="swallow_solid_food">
                    <td>{ci helper='language' function='lang' param1="swallow_solid_food"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['swallow_solid_food'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['swallow_solid_food'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['swallow_solid_food'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['swallow_solid_food'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['swallow_solid_food'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['swallow_solid_food'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="swallow_liquid_food">
                    <td>{ci helper='language' function='lang' param1="swallow_liquid_food"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['swallow_liquid_food'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['swallow_liquid_food'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['swallow_liquid_food'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['swallow_liquid_food'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['swallow_liquid_food'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['swallow_liquid_food'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="independently_drink">
                    <td>{ci helper='language' function='lang' param1="independently_drink"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['independently_drink'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['independently_drink'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['independently_drink'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['independently_drink'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['independently_drink'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="independently_there">
                    <td>{ci helper='language' function='lang' param1="independently_there"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['independently_there'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['independently_there'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['independently_there'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['independently_there'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['independently_there'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['independently_there'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="stool">
                    <td>{ci helper='language' function='lang' param1="stool"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['stool'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['stool'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['stool'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['stool'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['stool'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['stool'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="bladder_control">
                    <td>{ci helper='language' function='lang' param1="bladder_control"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['bladder_control'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['bladder_control'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['bladder_control'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['bladder_control'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['bladder_control'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['bladder_control'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                
            </table>
            <button id="back_step4" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 4</button>
            <button id="next_step4" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div>
                        
    <div class="contBox" id="step5" style="display:none">
        <h2>{ci helper='language' function='lang' param1="change_sphere"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="rvCscope">
                <tr id="react_to_their_environment">
                    <td>{ci helper='language' function='lang' param1="react_to_their_environment"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['react_to_their_environment'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['react_to_their_environment'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['react_to_their_environment'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['react_to_their_environment'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['react_to_their_environment'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['react_to_their_environment'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="stare">
                    <td>{ci helper='language' function='lang' param1="stare"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['stare'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['stare'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['stare'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['stare'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['stare'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['stare'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="recognize_people">
                    <td>{ci helper='language' function='lang' param1="recognize_people"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['recognize_people'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['recognize_people'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['recognize_people'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['recognize_people'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['recognize_people'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['recognize_people'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="communicate_nonverbally">
                    <td>{ci helper='language' function='lang' param1="communicate_nonverbally"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['communicate_nonverbally'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['communicate_nonverbally'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['communicate_nonverbally'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['communicate_nonverbally'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['communicate_nonverbally'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['communicate_nonverbally'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="communicate_verbally">
                    <td>{ci helper='language' function='lang' param1="communicate_verbally"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['communicate_verbally'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['communicate_verbally'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['communicate_verbally'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['communicate_verbally'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['communicate_verbally'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['communicate_verbally'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="say_phrases">
                    <td>{ci helper='language' function='lang' param1="say_phrases"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['say_phrases'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['say_phrases'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['say_phrases'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['say_phrases'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['say_phrases'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['say_phrases'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="understand_speech">
                    <td>{ci helper='language' function='lang' param1="understand_speech"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['understand_speech'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['understand_speech'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['understand_speech'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['understand_speech'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['understand_speech'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['understand_speech'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="perform_simple_requests">
                    <td>{ci helper='language' function='lang' param1="perform_simple_requests"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['perform_simple_requests'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['perform_simple_requests'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['perform_simple_requests'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['perform_simple_requests'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['perform_simple_requests'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                                <option {if $last['perform_simple_requests'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="allocate_favorite_toy">
                    <td>{ci helper='language' function='lang' param1="allocate_favorite_toy"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['allocate_favorite_toy'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['allocate_favorite_toy'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['allocate_favorite_toy'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['allocate_favorite_toy'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['allocate_favorite_toy'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['allocate_favorite_toy'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="play_with_other">
                    <td>{ci helper='language' function='lang' param1="play_with_other"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['play_with_other'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['play_with_other'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['play_with_other'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['play_with_other'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['play_with_other'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['play_with_other'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="independently_play">
                    <td>{ci helper='language' function='lang' param1="independently_play"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['independently_play'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['independently_play'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['independently_play'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['independently_play'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['independently_play'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['independently_play'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}

                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="follow_social_norms">
                    <td>{ci helper='language' function='lang' param1="follow_social_norms"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['follow_social_norms'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['follow_social_norms'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['follow_social_norms'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['follow_social_norms'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['follow_social_norms'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                                <option {if $last['follow_social_norms'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}

                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="concentrate_on_tasks">
                    <td>{ci helper='language' function='lang' param1="concentrate_on_tasks"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['concentrate_on_tasks'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['concentrate_on_tasks'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['concentrate_on_tasks'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['concentrate_on_tasks'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['concentrate_on_tasks'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                                <option {if $last['concentrate_on_tasks'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}

                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="listen_to_storiess">
                    <td>{ci helper='language' function='lang' param1="listen_to_storiess"}</td>
                    <td></td>
                    <td><input   data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['listen_to_storiess'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['listen_to_storiess'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['listen_to_storiess'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['listen_to_storiess'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['listen_to_storiess'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['listen_to_storiess'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}

                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="read">
                    <td>{ci helper='language' function='lang' param1="read"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['read'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['read'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['read'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['read'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['read'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['read'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}

                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="write">
                    <td>{ci helper='language' function='lang' param1="write"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['write'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['write'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['write'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['write'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['write'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['write'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}

                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="adequately_respond_emotionally">
                    <td>{ci helper='language' function='lang' param1="respond_emotionally"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['adequately_respond_emotionally'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['adequately_respond_emotionally'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['adequately_respond_emotionally'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['adequately_respond_emotionally'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['adequately_respond_emotionally'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['adequately_respond_emotionally'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="perform_complex_tasks">
                    <td>{ci helper='language' function='lang' param1="perform_complex_tasks"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['perform_complex_tasks'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['perform_complex_tasks'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['perform_complex_tasks'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['perform_complex_tasks'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['perform_complex_tasks'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option  {if $last['perform_complex_tasks'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="tutor">
                    <td>{ci helper='language' function='lang' param1="tutor"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['tutor'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['tutor'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['tutor'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['tutor'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['tutor'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                                <option {if $last['tutor'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="work_in_group">
                    <td>{ci helper='language' function='lang' param1="work_in_group"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['work_in_group'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['work_in_group'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['work_in_group'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['work_in_group'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['work_in_group'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                                <option {if $last['work_in_group'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="eye_contact">
                    <td>{ci helper='language' function='lang' param1="eye_contact"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['eye_contact'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['eye_contact'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['eye_contact'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['eye_contact'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['eye_contact'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                                <option {if $last['eye_contact'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="pointing_gesture">
                    <td>{ci helper='language' function='lang' param1="pointing_gesture"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['pointing_gesture'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['pointing_gesture'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['pointing_gesture'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['pointing_gesture'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['pointing_gesture'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['pointing_gesture'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="to_be_quiet_for_request">
                    <td>{ci helper='language' function='lang' param1="to_be_quiet_for_request"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['to_be_quiet_for_request'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['to_be_quiet_for_request'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['to_be_quiet_for_request'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['to_be_quiet_for_request'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['to_be_quiet_for_request'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['to_be_quiet_for_request'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="concentrate_on_conversation">
                    <td>{ci helper='language' function='lang' param1="concentrate_on_conversation"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['concentrate_on_conversation'][1] == "not_at_all"}selected{/if} value="not_at_all">{ci helper='language' function='lang' param1="not_at_all"}</option>
                            <option {if $last['concentrate_on_conversation'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['concentrate_on_conversation'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['concentrate_on_conversation'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['concentrate_on_conversation'][1] == "well"}selected{/if} value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['concentrate_on_conversation'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                
            </table>
            <button id="back_step5" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 5</button>
            <button id="next_step5" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div>
                        
    <div class="contBox" id="step6" style="display:none">
        <h2>{ci helper='language' function='lang' param1="change_sphere2"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="rvPscope">
                
                <tr id="appetite">
                    <td>{ci helper='language' function='lang' param1="appetite"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['appetite'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['appetite'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['appetite'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['appetite'][1] == "well"}selected{/if}  value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select class="form-control slballs">
                            {for $j=0 to 9}
                                <option {if $last['appetite'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="digestion">
                    <td>{ci helper='language' function='lang' param1="digestion"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}" type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['digestion'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['digestion'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['digestion'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['digestion'][1] == "well"}selected{/if}  value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['digestion'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="bowel_movement">
                    <td>{ci helper='language' function='lang' param1="bowel_movement"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select   data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['bowel_movement'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['bowel_movement'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['bowel_movement'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['bowel_movement'][1] == "well"}selected{/if}  value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                                <option {if $last['bowel_movement'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="breathing">
                    <td>{ci helper='language' function='lang' param1="breathing"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['breathing'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['breathing'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['breathing'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['breathing'][1] == "well"}selected{/if}  value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['breathing'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="dream">
                    <td>{ci helper='language' function='lang' param1="dream"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" checked disabled value="1"></td>
                    <td><select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="significant_changes"}" class="form-control selrate">
                            <option {if $last['dream'][1] == "poorly"}selected{/if} value="poorly">{ci helper='language' function='lang' param1="poorly"}</option>
                            <option {if $last['dream'][1] == "moderately"}selected{/if} value="moderately">{ci helper='language' function='lang' param1="moderately"}</option>
                            <option {if $last['dream'][1] == "fairly_well"}selected{/if} value="fairly_well">{ci helper='language' function='lang' param1="fairly_well"}</option>
                            <option {if $last['dream'][1] == "well"}selected{/if}  value="well">{ci helper='language' function='lang' param1="well"}</option>
                        </select></td>
                    <td>
                        <select data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}" class="form-control slballs">
                            {for $j=0 to 9}
                            <option {if $last['dream'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                
            </table>
            <button id="back_step6" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 6</button>
            <button id="next_step6" type="button" class="btn btn-success">Next <i class="glyphicon glyphicon-forward"></i></button>
        </div>
    </div>
  
     <div class="contBox" id="step7" style="display:none">
        <h2>{ci helper='language' function='lang' param1="symptoms"}</h2>
        <div class="col-xs-12 pl-0">
            <table class="table table-striped" id="rvSymptoms">
                
                <tr id="convulsions">
                    <td>{ci helper='language' function='lang' param1="convulsions"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" {if isset($last['convulsions'])}checked{/if} value="1"></td>
                    <td></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}"  class="form-control">
                            {for $j=0 to 9}
                            <option {if $last['convulsions'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="bronchitis">
                    <td>{ci helper='language' function='lang' param1="bronchitis"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" {if isset($last['bronchitis'])}checked{/if}  value="1"></td>
                    <td></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}"  class="form-control">
                            {for $j=0 to 9}
                            <option {if $last['bronchitis'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="sars">
                    <td>{ci helper='language' function='lang' param1="sars"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" {if isset($last['sars'])}checked{/if} value="1"></td>
                    <td></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}"   class="form-control">
                            {for $j=0 to 9}
                            <option {if $last['sars'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="allergy">
                    <td>{ci helper='language' function='lang' param1="allergy"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  {if isset($last['allergy'])}checked{/if} value="1"></td>
                    <td></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}"   class="form-control">
                            {for $j=0 to 9}
                            <option {if $last['allergy'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="bronchial_asthma">
                    <td>{ci helper='language' function='lang' param1="bronchial_asthma"}</td>
                    <td></td>
                    <td><input data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" {if isset($last['bronchial_asthma'])}checked{/if} value="1"></td>
                    <td></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}"   class="form-control">
                            {for $j=0 to 9}
                            <option  {if $last['bronchial_asthma'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="skin_diseases">
                    <td>{ci helper='language' function='lang' param1="skin_diseases"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" {if isset($last['skin_diseases'])}checked{/if} value="1"></td>
                    <td></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}"  class="form-control">
                            {for $j=0 to 9}
                            <option {if $last['skin_diseases'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="intestinal_colic">
                    <td>{ci helper='language' function='lang' param1="intestinal_colic"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx"  {if isset($last['intestinal_colic'])}checked{/if} value="1"></td>
                    <td></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}"   class="form-control">
                            {for $j=0 to 9}
                            <option {if $last['intestinal_colic'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="other">
                    <td>{ci helper='language' function='lang' param1="other"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}"  class="form-control">
                            {for $j=0 to 9}
                            <option  {if $last['other'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr id="new_other">
                    <td>{ci helper='language' function='lang' param1="new_other"}</td>
                    <td></td>
                    <td><input  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="any_changes"}"  type="checkbox" class="chbx" value="1"></td>
                    <td></td>
                    <td>
                        <select  data-toggle="tooltip" data-placement="top" title="{ci helper='language' function='lang' param1="scale"}"  class="form-control">
                            {for $j=0 to 9}
                            <option {if $last['new_other'][2] == $j}selected{/if} value="{$j}">{$j}</option>
                            {/for}
                        </select>
                    </td>
                    <td><textarea class="form-control"></textarea></td>
                </tr>
                
            </table>
            <button id="back_step7" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-backward"></i> Back</button>
            <button type="button" class="btn">{ci helper='language' function='lang' param1="step"} 7</button>
            <button id="sendForm" type="submit" class="btn btn-success">{ci helper='language' function='lang' param1="send"}</button>
        </div>
    </div>
        
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">{ci helper='language' function='lang' param1="sending_completed"}</h4>
                </div>
                <div class="modal-body">
                    <p>{ci helper='language' function='lang' param1="ATTENTION"}</p>
                </div>
            </div>
        </div>
    </div>
    
    <script>

 function checkCurr(d) {
	if(window.event) 
	  { 
		if(event.keyCode == 37 || event.keyCode == 39) return; 
	  }  
	d.value = d.value.replace(/\D/g,'');
};   
$('[data-toggle="tooltip"]').tooltip()
//Повторный визит - отчет родителей			
$('#next_step1').on('click', function () {
    general_otchet = { };
	general_otchet_vals = { };
    

    var rvo_tr = $('#rvOtchet tr:visible');
    rvo_tr.each(function() {

        var rv_td1 = $(this).children('td:eq(0)');
        var rv_td2 = $(this).children('td:eq(1)');
        var rv_td3 = $(this).children('td:eq(2)');
        var rv_td4 = $(this).children('td:eq(3)');

        var rv_name = $(this).attr('id');
        var rv_txt1 = rv_td1.text();
        var rv_val1 = 'undefined';
		

        var check_sel2 = rv_td2.children().is('select');
        var check_inp2 = rv_td2.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel2 = rv_td2.find('select');
        var find_inp2 = rv_td2.find('textarea, input:text');

        if (check_sel2 == true) {
            if (!find_sel2.attr("multiple")) {
                rv_txt2 = find_sel2.children('option:selected').text();
                rv_val2 = find_sel2.val();
            } else {
                rv_txt2 = find_sel2.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val2 = find_sel2.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp2 == true && find_inp2.val() != '') {
            rv_txt2 = find_inp2.val();
			rv_val2 = find_inp2.val();
        } else if (rv_td2.text() != '') {
            rv_txt2 = rv_td2.text();
			rv_val2 = 'undefined';
        } else {
            rv_txt2 = 'undefined';
			rv_val2 = 'undefined';
        }
		
		var check_sel3 = rv_td3.children().is('select');
        var check_inp3 = rv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3 = rv_td3.find('select');
        var find_inp3 = rv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                rv_txt3 = find_sel3.children('option:selected').text();
                rv_val3 = find_sel3.val();
            } else {
                rv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            rv_txt3 = find_inp3.val();
			rv_val3 = find_inp3.val();
        } else if (rv_td3.text() != '') {
            rv_txt3 = rv_td3.text();
			rv_val3 = 'undefined';
        } else {
            rv_txt3 = 'undefined';
			rv_val3 = 'undefined';
        }

        general_otchet[rv_name] 		= rv_txt1 + '|' + rv_txt2 + '|' + rv_txt3;
		general_otchet_vals[rv_name] = rv_val1 + '|' + rv_val2 + '|' + rv_val3 ;

    });
	console.log(general_otchet);
	console.log(general_otchet_vals);
	
		$('#step1').hide();
        $('#step2').show();
		$(window).scrollTop(0);
});

//Повторный визит - пример ежедвного меню			
$('#next_step2').on('click', function () {
	
	var inpVals = [];
	$('#rvpitanie .form-control').each(function(index) {
		var iVal = $(this).val();
		if (iVal == '') {
			$(this).parent('td').addClass('has-error')
								.removeClass('has-success');
			inpVals.push('error')
		} else {
			$(this).parent('td').addClass('has-success')
								.removeClass('has-error');
			inpVals.push('success')
		}
		//inpVals[index] = iVal;
	});
	console.log(inpVals);

	if ($.inArray('error', inpVals) == -1) {
	
	
    general_pitanie = { };
	general_pitanie_vals = { };
    

    var rvo_tr = $('#rvpitanie tr:visible');
    rvo_tr.each(function() {
		
		
        var rv_td1 = $(this).children('td:eq(0)');
        var rv_td2 = $(this).children('td:eq(1)');
        var rv_td3 = $(this).children('td:eq(2)');
        var rv_td4 = $(this).children('td:eq(3)');

        var rv_name = $(this).attr('id');
        var rv_txt1 = rv_td1.text();
        var rv_val1 = 'undefined';
		
		

        var check_sel2 = rv_td2.children().is('select');
        var check_inp2 = rv_td2.children().is('textarea, input[type="text"], input[type="number"], input[type="time"]');
        var find_sel2 = rv_td2.find('select');
        var find_inp2 = rv_td2.find('textarea, input:text, input[type="time"]');

        if (check_sel2 == true) {
            if (!find_sel2.attr("multiple")) {
                rv_txt2 = find_sel2.children('option:selected').text();
                rv_val2 = find_sel2.val();
            } else {
                rv_txt2 = find_sel2.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val2 = find_sel2.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp2 == true && find_inp2.val() != '') {
            rv_txt2 = find_inp2.val();
			rv_val2 = find_inp2.val();
        } else if (rv_td2.text() != '') {
            rv_txt2 = rv_td2.text();
			rv_val2 = 'undefined';
        } else {
            rv_txt2 = 'undefined';
			rv_val2 = 'undefined';
        }
		
		var check_sel3 = rv_td3.children().is('select');
        var check_inp3 = rv_td3.children().is('textarea, input[type="text"], input[type="time"]');
        var find_sel3 = rv_td3.find('select');
        var find_inp3 = rv_td3.find('textarea, input:text, input[type="time"]');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                rv_txt3 = find_sel3.children('option:selected').text();
                rv_val3 = find_sel3.val();
            } else {
                rv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            rv_txt3 = find_inp3.val();
			rv_val3 = find_inp3.val();
        } else if (rv_td3.text() != '') {
            rv_txt3 = rv_td3.text();
			rv_val3 = 'undefined';
        } else {
            rv_txt3 = 'undefined';
			rv_val3 = 'undefined';
        }

        general_pitanie[rv_name] 		= rv_txt1 + '|' + rv_txt2 + '|' + rv_txt3;
		general_pitanie_vals[rv_name] = rv_val1 + '|' + rv_val2 + '|' + rv_val3 ;

    });
	console.log(general_pitanie);
	console.log(general_pitanie_vals);
	
		$('#step2').hide();
        $('#step3').show();
		$(window).scrollTop(0);
		
	} else {
		alert('{ci helper='language' function='lang' param1="complete_all_lines"}');
	}
});

//Повторный визит - Структурные нарушения	
$('#next_step3').on('click', function () {
    rv_violations = { };
	rv_violations_vals = { };

    var rvv_tr = $('#rvViolations tr:visible');
    rvv_tr.each(function() {

        var rv_td1 = $(this).children('td:eq(0)');
        var rv_td2 = $(this).children('td:eq(2)');
        var rv_td3 = $(this).children('td:eq(3)');
        var rv_td4 = $(this).children('td:eq(4)');
        var rv_td5 = $(this).children('td:eq(5)');

        var rv_name = $(this).attr('id');
        var rv_txt1 = rv_td1.text();
        var rv_val1 = 'undefined';
		
		var check_chbx = rv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = rv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = rv_td3.children().is('select');
        var check_inp3 = rv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = rv_td3.find('select');
        var find_inp3  = rv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                rv_txt3 = find_sel3.children('option:selected').text();
                rv_val3 = find_sel3.val();
            } else {
                rv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            rv_txt3 = find_inp3.val();
			rv_val3 = find_inp3.val();
        } else if (rv_td3.text() != '') {
            rv_txt3 = rv_td3.text();
			rv_val3 = 'undefined';
        } else {
            rv_txt3 = 'undefined';
			rv_val3 = 'undefined';
        }
		
		var check_sel4 = rv_td4.children().is('select');
        var check_inp4 = rv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = rv_td4.find('select');
        var find_inp4  = rv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                rv_txt4 = find_sel4.children('option:selected').text();
                rv_val4 = find_sel4.val();
            } else {
                rv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            rv_txt4 = find_inp4.val();
			rv_val4 = find_inp4.val();
        } else if (rv_td4.text() != '') {
            rv_txt4 = rv_td4.text();
			rv_val4 = 'undefined';
        } else {
            rv_txt4 = 'undefined';
			rv_val4 = 'undefined';
        }
		
		var check_sel5 = rv_td5.children().is('select');
        var check_inp5 = rv_td5.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel5  = rv_td5.find('select');
        var find_inp5  = rv_td5.find('textarea, input:text');

        if (check_sel5 == true) {
            if (!find_sel5.attr("multiple")) {
                rv_txt5 = find_sel5.children('option:selected').text();
                rv_val5 = find_sel5.val();
            } else {
                rv_txt5 = find_sel5.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val5 = find_sel5.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp5 == true && find_inp5.val() != '') {
            rv_txt5 = find_inp5.val();
			rv_val5 = find_inp5.val();
        } else if (rv_td5.text() != '') {
            rv_txt5 = rv_td5.text();
			rv_val5 = 'undefined';
        } else {
            rv_txt5 = 'undefined';
			rv_val5 = 'undefined';
        }
		
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        rv_violations[rv_name] 		= rv_txt1 + '|' + rv_txt3 + '|' + rv_txt4 + '|' + rv_txt5;
		rv_violations_vals[rv_name] = rv_val1 + '|' + rv_val3 + '|' + rv_val4 + '|' + rv_val5;
		}

    });
    {*console.log(rv_violations);
	console.log(rv_violations_vals);*}
	
		$('#step3').hide();
        $('#step4').show();
		$(window).scrollTop(0);
});

//Повторный визит - Фунциональная сфера	
$('#next_step4').on('click', function () {
    rv_fscope = { };
	rv_fscope_vals = { };

    var rvfs_tr = $('#rvFscope tr:visible');
    rvfs_tr.each(function() {

        var rv_td1 = $(this).children('td:eq(0)');
        var rv_td2 = $(this).children('td:eq(2)');
        var rv_td3 = $(this).children('td:eq(3)');
        var rv_td4 = $(this).children('td:eq(4)');
        var rv_td5 = $(this).children('td:eq(5)');

        var rv_name = $(this).attr('id');
        var rv_txt1 = rv_td1.text();
        var rv_val1 = 'undefined';
		
		var check_chbx = rv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = rv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = rv_td3.children().is('select');
        var check_inp3 = rv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = rv_td3.find('select');
        var find_inp3  = rv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                rv_txt3 = find_sel3.children('option:selected').text();
                rv_val3 = find_sel3.val();
            } else {
                rv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            rv_txt3 = find_inp3.val();
			rv_val3 = find_inp3.val();
        } else if (rv_td3.text() != '') {
            rv_txt3 = rv_td3.text();
			rv_val3 = 'undefined';
        } else {
            rv_txt3 = 'undefined';
			rv_val3 = 'undefined';
        }
		
		var check_sel4 = rv_td4.children().is('select');
        var check_inp4 = rv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = rv_td4.find('select');
        var find_inp4  = rv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                rv_txt4 = find_sel4.children('option:selected').text();
                rv_val4 = find_sel4.val();
            } else {
                rv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            rv_txt4 = find_inp4.val();
			rv_val4 = find_inp4.val();
        } else if (rv_td4.text() != '') {
            rv_txt4 = rv_td4.text();
			rv_val4 = 'undefined';
        } else {
            rv_txt4 = 'undefined';
			rv_val4 = 'undefined';
        }
		
		var check_sel5 = rv_td5.children().is('select');
        var check_inp5 = rv_td5.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel5  = rv_td5.find('select');
        var find_inp5  = rv_td5.find('textarea, input:text');

        if (check_sel5 == true) {
            if (!find_sel5.attr("multiple")) {
                rv_txt5 = find_sel5.children('option:selected').text();
                rv_val5 = find_sel5.val();
            } else {
                rv_txt5 = find_sel5.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val5 = find_sel5.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp5 == true && find_inp5.val() != '') {
            rv_txt5 = find_inp5.val();
			rv_val5 = find_inp5.val();
        } else if (rv_td5.text() != '') {
            rv_txt5 = rv_td5.text();
			rv_val5 = 'undefined';
        } else {
            rv_txt5 = 'undefined';
			rv_val5 = 'undefined';
        }
		
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        rv_fscope[rv_name] 		= rv_txt1 + '|' + rv_txt3 + '|' + rv_txt4 + '|' + rv_txt5;
		rv_fscope_vals[rv_name] = rv_val1 + '|' + rv_val3 + '|' + rv_val4 + '|' + rv_val5;
		}

    });
	{*console.log(rv_fscope);
	console.log(rv_fscope_vals);*}
	
		$('#step4').hide();
        $('#step5').show();
		$(window).scrollTop(0);
});

//Повторный визит - изменение когнитивной сферы		
$('#next_step5').on('click', function () {
    rv_cscope = { };
	rv_cscope_vals = { };

    var rvfs_tr = $('#rvCscope tr:visible');
    rvfs_tr.each(function() {

        var rv_td1 = $(this).children('td:eq(0)');
        var rv_td2 = $(this).children('td:eq(2)');
        var rv_td3 = $(this).children('td:eq(3)');
        var rv_td4 = $(this).children('td:eq(4)');
        var rv_td5 = $(this).children('td:eq(5)');

        var rv_name = $(this).attr('id');
        var rv_txt1 = rv_td1.text();
        var rv_val1 = 'undefined';
		
		var check_chbx = rv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = rv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = rv_td3.children().is('select');
        var check_inp3 = rv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = rv_td3.find('select');
        var find_inp3  = rv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                rv_txt3 = find_sel3.children('option:selected').text();
                rv_val3 = find_sel3.val();
            } else {
                rv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            rv_txt3 = find_inp3.val();
			rv_val3 = find_inp3.val();
        } else if (rv_td3.text() != '') {
            rv_txt3 = rv_td3.text();
			rv_val3 = 'undefined';
        } else {
            rv_txt3 = 'undefined';
			rv_val3 = 'undefined';
        }
		
		var check_sel4 = rv_td4.children().is('select');
        var check_inp4 = rv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = rv_td4.find('select');
        var find_inp4  = rv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                rv_txt4 = find_sel4.children('option:selected').text();
                rv_val4 = find_sel4.val();
            } else {
                rv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            rv_txt4 = find_inp4.val();
			rv_val4 = find_inp4.val();
        } else if (rv_td4.text() != '') {
            rv_txt4 = rv_td4.text();
			rv_val4 = 'undefined';
        } else {
            rv_txt4 = 'undefined';
			rv_val4 = 'undefined';
        }
		
		var check_sel5 = rv_td5.children().is('select');
        var check_inp5 = rv_td5.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel5  = rv_td5.find('select');
        var find_inp5  = rv_td5.find('textarea, input:text');

        if (check_sel5 == true) {
            if (!find_sel5.attr("multiple")) {
                rv_txt5 = find_sel5.children('option:selected').text();
                rv_val5 = find_sel5.val();
            } else {
                rv_txt5 = find_sel5.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val5 = find_sel5.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp5 == true && find_inp5.val() != '') {
            rv_txt5 = find_inp5.val();
			rv_val5 = find_inp5.val();
        } else if (rv_td5.text() != '') {
            rv_txt5 = rv_td5.text();
			rv_val5 = 'undefined';
        } else {
            rv_txt5 = 'undefined';
			rv_val5 = 'undefined';
        }
		
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        rv_cscope[rv_name] 		= rv_txt1 + '|' + rv_txt3 + '|' + rv_txt4 + '|' + rv_txt5;
		rv_cscope_vals[rv_name] = rv_val1 + '|' + rv_val3 + '|' + rv_val4 + '|' + rv_val5;
		}

    });
	{*console.log(rv_cscope);
	console.log(rv_cscope_vals);*}
	
		$('#step5').hide();
        $('#step6').show();
		$(window).scrollTop(0);
});

//Повторный визит - изменение физиологической сферы
$('#next_step6').on('click', function () {
    rv_pscope = { };
	rv_pscope_vals = { };

    var rvfs_tr = $('#rvPscope tr:visible');
    rvfs_tr.each(function() {

        var rv_td1 = $(this).children('td:eq(0)');
        var rv_td2 = $(this).children('td:eq(2)');
        var rv_td3 = $(this).children('td:eq(3)');
        var rv_td4 = $(this).children('td:eq(4)');
        var rv_td5 = $(this).children('td:eq(5)');

        var rv_name = $(this).attr('id');
        var rv_txt1 = rv_td1.text();
        var rv_val1 = 'undefined';
		
		var check_chbx = rv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = rv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = rv_td3.children().is('select');
        var check_inp3 = rv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = rv_td3.find('select');
        var find_inp3  = rv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                rv_txt3 = find_sel3.children('option:selected').text();
                rv_val3 = find_sel3.val();
            } else {
                rv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            rv_txt3 = find_inp3.val();
			rv_val3 = find_inp3.val();
        } else if (rv_td3.text() != '') {
            rv_txt3 = rv_td3.text();
			rv_val3 = 'undefined';
        } else {
            rv_txt3 = 'undefined';
			rv_val3 = 'undefined';
        }
		
		var check_sel4 = rv_td4.children().is('select');
        var check_inp4 = rv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = rv_td4.find('select');
        var find_inp4  = rv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                rv_txt4 = find_sel4.children('option:selected').text();
                rv_val4 = find_sel4.val();
            } else {
                rv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            rv_txt4 = find_inp4.val();
			rv_val4 = find_inp4.val();
        } else if (rv_td4.text() != '') {
            rv_txt4 = rv_td4.text();
			rv_val4 = 'undefined';
        } else {
            rv_txt4 = 'undefined';
			rv_val4 = 'undefined';
        }
		
		var check_sel5 = rv_td5.children().is('select');
        var check_inp5 = rv_td5.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel5  = rv_td5.find('select');
        var find_inp5  = rv_td5.find('textarea, input:text');

        if (check_sel5 == true) {
            if (!find_sel5.attr("multiple")) {
                rv_txt5 = find_sel5.children('option:selected').text();
                rv_val5 = find_sel5.val();
            } else {
                rv_txt5 = find_sel5.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val5 = find_sel5.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp5 == true && find_inp5.val() != '') {
            rv_txt5 = find_inp5.val();
			rv_val5 = find_inp5.val();
        } else if (rv_td5.text() != '') {
            rv_txt5 = rv_td5.text();
			rv_val5 = 'undefined';
        } else {
            rv_txt5 = 'undefined';
			rv_val5 = 'undefined';
        }
		
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        rv_pscope[rv_name] 		= rv_txt1 + '|' + rv_txt3 + '|' + rv_txt4 + '|' + rv_txt5;
		rv_pscope_vals[rv_name] = rv_val1 + '|' + rv_val3 + '|' + rv_val4 + '|' + rv_val5;
		}

    });
	{*console.log(rv_pscope);
	console.log(rv_pscope_vals);*}
	
		$('#step6').hide();
        $('#step7').show();
		$(window).scrollTop(0);
});

//Заболевание и симптомы
$('#sendForm').on('click', function () {
    rv_symptoms = { };
	rv_symptoms_vals = { };

    var rvfs_tr = $('#rvSymptoms tr:visible');
    rvfs_tr.each(function() {

        var rv_td1 = $(this).children('td:eq(0)');
        var rv_td2 = $(this).children('td:eq(2)');
        var rv_td3 = $(this).children('td:eq(3)');
        var rv_td4 = $(this).children('td:eq(4)');
        var rv_td5 = $(this).children('td:eq(5)');

        var rv_name = $(this).attr('id');
        var rv_txt1 = rv_td1.text();
        var rv_val1 = 'undefined';
		
		var check_chbx = rv_td2.children().is('input[type="checkbox"]');
		var find_chbx  = rv_td2.find('input[type="checkbox"]');
		
		var check_sel3 = rv_td3.children().is('select');
        var check_inp3 = rv_td3.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel3  = rv_td3.find('select');
        var find_inp3  = rv_td3.find('textarea, input:text');

        if (check_sel3 == true) {
            if (!find_sel3.attr("multiple")) {
                rv_txt3 = find_sel3.children('option:selected').text();
                rv_val3 = find_sel3.val();
            } else {
                rv_txt3 = find_sel3.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val3 = find_sel3.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp3 == true && find_inp3.val() != '') {
            rv_txt3 = find_inp3.val();
			rv_val3 = find_inp3.val();
        } else if (rv_td3.text() != '') {
            rv_txt3 = rv_td3.text();
			rv_val3 = 'undefined';
        } else {
            rv_txt3 = 'undefined';
			rv_val3 = 'undefined';
        }
		
		var check_sel4 = rv_td4.children().is('select');
        var check_inp4 = rv_td4.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel4  = rv_td4.find('select');
        var find_inp4  = rv_td4.find('textarea, input:text');

        if (check_sel4 == true) {
            if (!find_sel4.attr("multiple")) {
                rv_txt4 = find_sel4.children('option:selected').text();
                rv_val4 = find_sel4.val();
            } else {
                rv_txt4 = find_sel4.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val4 = find_sel4.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp4 == true && find_inp4.val() != '') {
            rv_txt4 = find_inp4.val();
			rv_val4 = find_inp4.val();
        } else if (rv_td4.text() != '') {
            rv_txt4 = rv_td4.text();
			rv_val4 = 'undefined';
        } else {
            rv_txt4 = 'undefined';
			rv_val4 = 'undefined';
        }
		
		var check_sel5 = rv_td5.children().is('select');
        var check_inp5 = rv_td5.children().is('textarea, input[type="text"], input[type="number"]');
        var find_sel5  = rv_td5.find('select');
        var find_inp5  = rv_td5.find('textarea, input:text');

        if (check_sel5 == true) {
            if (!find_sel5.attr("multiple")) {
                rv_txt5 = find_sel5.children('option:selected').text();
                rv_val5 = find_sel5.val();
            } else {
                rv_txt5 = find_sel5.children('option:selected').map(function() {
                    return $(this).text();
                }).get();
				rv_val5 = find_sel5.children('option:selected').map(function () {
					return this.value
				}).get();
            }
        } else if (check_inp5 == true && find_inp5.val() != '') {
            rv_txt5 = find_inp5.val();
			rv_val5 = find_inp5.val();
        } else if (rv_td5.text() != '') {
            rv_txt5 = rv_td5.text();
			rv_val5 = 'undefined';
        } else {
            rv_txt5 = 'undefined';
			rv_val5 = 'undefined';
        }
		
		
		if (check_chbx == true && find_chbx.is(':checked')) {

        rv_symptoms[rv_name] 		= rv_txt1 + '|' + rv_txt3 + '|' + rv_txt4 + '|' + rv_txt5;
		rv_symptoms_vals[rv_name] = rv_val1 + '|' + rv_val3 + '|' + rv_val4 + '|' + rv_val5;
		}

    });
	console.log(general_otchet);
	
	$.ajax({
        url: '{ci helper="url" function="base_url"}booking/saveReturnVisit'
        , type: 'POST'
        , data: {
                general_otchet:JSON.stringify($.extend(general_otchet,general_pitanie)),
                general_otchet_vals:JSON.stringify($.extend(general_otchet_vals, general_pitanie_vals)),
                rv:JSON.stringify($.extend(rv_violations, rv_fscope, rv_cscope, rv_pscope, rv_symptoms)),
                rv_vals:JSON.stringify($.extend(rv_violations_vals, rv_fscope_vals, rv_cscope_vals, rv_pscope_vals, rv_symptoms_vals))
            }
        , success: function (res) {
            $('#myModal').modal({
            show: true,
            //backdrop: false
            });
            $('#myModal').on('hidden.bs.modal', function (e) {
                window.location.replace('../');
            })
            setTimeout(function() { 
                window.location.replace('../');
            }, 20000)
        }
    });
});

            $('#back_step2').click(function () {
                $('#step2').hide();
                $('#step1').show();
				$(window).scrollTop(0);
            });
			
            $('#back_step3').click(function () {
                $('#step3').hide();
                $('#step2').show();
				$(window).scrollTop(0);
            });
			
			$('#back_step4').click(function () {
                $('#step4').hide();
                $('#step3').show();
				$(window).scrollTop(0);
            });
            $('#back_step5').click(function () {
                $('#step5').hide();
                $('#step4').show();
				$(window).scrollTop(0);
            });
            $('#back_step6').click(function () {
                $('#step6').hide();
                $('#step5').show();
				$(window).scrollTop(0);
            });
			$('#back_step7').click(function () {
                $('#step7').hide();
                $('#step6').show();
				$(window).scrollTop(0);
            });
			
			$('.selrate').on('change', function(){
				var cuv  = $(this).data('curval'),
					chv  = $(this).val(),
					nsl  = $(this).parent('td').next('td').children('.slballs'),
					nslv = nsl.data('ncurval'),
					nsli = nsl.data('noptind'),
					nslz = nsl.children('option:eq(0)'),
					iskl = ['normal'];
					
				//console.log(nslv+' | ' +nsli)
				//console.log(cuv +' | ' + chv);
				//console.log($.inArray(cuv, iskl) +' | ' + $.inArray(chv, iskl));
				
				//Было not стало не not
				if ($.inArray(cuv, iskl) >= 0 && $.inArray(chv, iskl) == -1) {
					//nslz.removeAttr('selected');
					nsl.find('option:eq(0)').attr('selected', 'selected');
					nsl.removeAttr('disabled').val(0);
				} 
				//Было not стало not	
				else if ($.inArray(cuv, iskl) >= 0 && $.inArray(chv, iskl) >=0) {
					nsl.find('option:eq(0)').attr('selected', 'selected');
					nsl.val(0).attr('disabled', 'disabled');
				}
				//Было x стало y и не not
				 else if(cuv != chv && $.inArray(chv, iskl) == -1) {
					nsl.find('option:eq(0)').attr('selected', 'selected');
					nsl.val(0).attr('disabled', 'disabled');

				} 
				//Было x стало x и не not
				else if (cuv == chv && $.inArray(chv, iskl) == -1) {
					nsl.find('option:eq(0)').removeAttr('selected');
					nsl.removeAttr('disabled').val(nslv);
					nsl.find('option:eq('+nsli+')').attr('selected', 'selected');
				} 
				//Было x стало not
				else if (cuv != chv && $.inArray(chv, iskl) >= 0) {
					nsl.val(0).attr('disabled', 'disabled');
					nsl.find('option:eq(0)').attr('selected', 'selected');
				}
			});
			
			$(window).load(function(){
				$('.selrate').each(function(){
					$(this).data('curval', $(this).val());
					//$(this).data('slind', $(this).val());
					//console.log($(this).data('slind'));
				});
				
				$('.slballs').each(function(){
					var normal = $(this).parent('td').prev('td').children('.selrate').val();
					if (normal == 'normal') {
						$(this).data('ncurval', 0);
						$(this).data('noptind', 0);
						$(this).val(0).
								attr('disabled', 'disabled');
						$(this).children('option:eq(0)').
								attr('selected', 'selected');
					} else {
						$(this).data('ncurval', $(this).val());
						$(this).data('noptind', $(this).find('option:selected').index());
					}
				});
			});
			
			
			
    </script>
{/block}