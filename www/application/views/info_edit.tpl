<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title>{$title}</title>
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="/dist/assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/dist/assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="/dist/assets/fonts/style.css">
        <link rel="stylesheet" href="/dist/assets/css/main.css">
        <link rel="stylesheet" href="/dist/assets/css/main-responsive.css">
        <link rel="stylesheet" href="/dist/assets/plugins/iCheck/skins/all.css">
        <link rel="stylesheet" href="/dist/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
        <link rel="stylesheet" href="/dist/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
        <link rel="stylesheet" href="/dist/assets/css/theme_light.css" type="text/css" id="skin_color">
        <link rel="stylesheet" href="/dist/assets/css/print.css" type="text/css" media="print"/>
        <!--[if IE 7]>
        <link rel="stylesheet" href="/dist/assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
        <![endif]-->
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="stylesheet" href="/dist/assets/plugins/select2/select2.css">
        <link rel="stylesheet" href="/dist/assets/plugins/datepicker/css/datepicker.css">
        <link rel="stylesheet" href="/dist/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
        <link rel="stylesheet" href="/dist/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
        <link rel="stylesheet" href="/dist/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
        <link rel="stylesheet" href="/dist/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
        <link rel="stylesheet" href="/dist/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
        <link rel="stylesheet" href="/dist/assets/plugins/summernote/build/summernote.css">
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="page-header">
                    <h1>{$title}</h1>
                </div>
            </div>
            <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: TEXT FIELDS PANEL -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i>
                            Text Fields
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                </a>
                                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-refresh" href="#">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-expand" href="#">
                                    <i class="fa fa-resize-full"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-close" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            {ci helper='form' function='form_open' param1='info/add' param2='class="form-horizontal"'}
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">Title</label>
                                    <div class="col-sm-9">
                                        <input name="title" maxlength="50" type="text" placeholder="Text Field" id="form-field-1" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="form-field-22" class="col-sm-2 control-label">Short Text</label>
                                    <div class="col-sm-9">
                                        <textarea name="short_desc" maxlength="140" placeholder="Default Text" id="form-field-22" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="col-sm-2 control-label">Description<br><br></p>
                                    <div class="col-sm-9">
                                        <textarea name="desc" id="form-field-25"  class="ckeditor form-control" cols="10" rows="10"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="col-sm-2 control-label">notification Area<br><br>
                                        <div class="col-sm-9">
                                            <label class="radio-inline">
                                                <input type="radio" value="1" name="is_notice" class="grey">Show
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" value="0" name="is_notice" class="grey">Hide
                                            </label>
                                        </div>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <p class="col-sm-2 control-label">Visibility<br><br>
                                        <div class="col-sm-9">
                                            <label class="radio-inline">
                                                <input type="radio" value="1" name="visible" class="grey">Show
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" value="0" name="visible" class="grey">Hide
                                            </label>
                                        </div>
                                    </p>
                                </div>

                                <div class="form-group">
                                    <p class="col-sm-2 control-label"><br>
                                        <div class="col-sm-9">
                                            <button type="submit" class="btn btn-success">Save</button>
                                            <a class="btn btn-danger" href="javascript:window.history.back();" role="button">Back</a>
                                        </div>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <div class="footer clearfix">
            <div class="footer-inner"></div>
            <div class="footer-items">
                <span class="go-top"><i class="clip-chevron-up"></i></span>
            </div>
        </div>
        <!-- end: FOOTER -->
        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
        <script src="/dist/assets/plugins/respond.min.js"></script>
        <script src="/dist/assets/plugins/excanvas.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <![endif]-->
        <!--[if gte IE 9]><!-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <!--<![endif]-->
        <script src="/dist/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="/dist/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/dist/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="/dist/assets/plugins/blockUI/jquery.blockUI.js"></script>
        <script src="/dist/assets/plugins/iCheck/jquery.icheck.min.js"></script>
        <script src="/dist/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
        <script src="/dist/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
        <script src="/dist/assets/plugins/less/less-1.5.0.min.js"></script>
        <script src="/dist/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
        <script src="/dist/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
        <script src="/dist/assets/js/main.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="/dist/assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="/dist/assets/plugins/autosize/jquery.autosize.min.js"></script>
        <script src="/dist/assets/plugins/select2/select2.min.js"></script>
        <script src="/dist/assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
        <script src="/dist/assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
        <script src="/dist/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="/dist/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="/dist/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
        <script src="/dist/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="/dist/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script src="/dist/assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
        <script src="/dist/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
        <script src="/dist/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
        <script src="/dist/assets/plugins/summernote/build/summernote.min.js"></script>
        <script src="/dist/assets/plugins/ckeditor/ckeditor.js"></script>
        <script src="/dist/assets/plugins/ckeditor/adapters/jquery.js"></script>
        <script src="/dist/assets/js/form-elements.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
            jQuery(document).ready(function () {
                Main.init();
                FormElements.init();
            });
        </script>
    </body>
    <!-- end: BODY -->
</html>