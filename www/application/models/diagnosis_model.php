<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Diagnosis_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->_table = "users";
    }
    
    /**
     * Есть ли диагноз у пользователя
     * @param int $user_id id пользователя
     * @return boolean
     */
    public function isDiagnosis($user_id) {
        $query = $this->db->query('SELECT `id` FROM `users` '
            . ' WHERE diagnosis IS NOT NULL AND `id` = '.$this->db->escape($user_id));
        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Получения диагноза пользователя
     * @param int $user_id id пользователя
     * @return str
     */
    public function diagnosis($user_id) {
        $query = $this->db->query('SELECT `diagnosis` FROM `users` WHERE `id` = '.$this->db->escape($user_id));
        if ($query->num_rows() > 0) {
            $row = $query->row(); 
            return $row->diagnosis;
        } 
    }

    /**
     * Обработка данных и запись с формы
     * формирования диагноза
     * @param int $user_id id пользователя 
     * @param array $data массив данных с формы
     */
    public function recDiagnosis($user_id, $data) {
        $str = '';
        foreach ($data as $value) {
            $arr = explode('|', $value);
            foreach ($arr as $val) {
                if(!empty($val) AND $val != 'undefined') {
                    $str .= " $val ";
                }
            }
            $str .= "<br>";
        }
        $this->db->where('id', $user_id);
        $this->db->update('users', array('diagnosis'=>$str));
    }
}