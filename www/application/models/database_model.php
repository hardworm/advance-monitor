<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Database_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function getCountUser() {
        return $this->db->count_all('users');
    }
    
    public function getUsers($page = 0) {
        $arr   = array();
        $page  = $page + 0;
        $query = $this->db->query("SELECT u.id, `first_name`, `last_name`, `DOB`, `email`, `is_male`, diagnosis, count(r.id) AS alarm FROM `users` AS `u`
        LEFT JOIN `report` AS `r` ON u.id=r.users_id
        LEFT JOIN `booking_reservation` AS `br`ON u.id=br.users_id
        GROUP BY u.id LIMIT $page,20 ");
        foreach ($query->result_array() as $row) {
            $arr[$row['id']] = $row;
            $booking = $this->db->query("SELECT cat.category_id, `category_name`, count(reservation_id) as count, sum(reservation_confirmed) AS `confirmed` 
            FROM `booking_categories` AS `cat` 
            LEFT JOIN `booking_calendars` AS `cal` ON cat.category_id=cal.category_id 
            LEFT JOIN `booking_reservation` AS `res` ON cal.calendar_id=res.calendar_id 
            WHERE users_id= ".$this->db->escape($row['id'])." GROUP BY cat.category_id ");
            foreach ($booking->result() as $val) {
                $arr[$row['id']][$val->category_name] = $val->count; 
                $arr[$row['id']][$val->category_name."_confirmed"] = $val->confirmed;
            }
            
            $alarm = $this->db->query('select count(*) as count from `report` WHERE `is_read` = 0 AND users_id='.$row['id']);
            $arr[$row['id']]['alarm_is_not_read'] = $alarm->row()->count;
            
            //TODO: переделать
            $QS = $this->db->query('select count(*) as count from `return_visit` WHERE users_id='.$row['id']);
            $arr[$row['id']]['QS'] = $QS->row()->count;
            
            $QSn = $this->db->query('select count(*) as count from `return_visit` WHERE `is_read` = 0 AND users_id='.$row['id']);
            $arr[$row['id']]['QS_is_not_read'] = $QSn->row()->count;
            
        }
        return $arr;
    }
    
    public function getAlarms($user_id, $page = 0) {
        $page  = $page + 0;
        $user_id = $user_id +0;
        $arr   = array();
        $this->db->order_by('date', 'desc'); 
        $query = $this->db->get_where('report', array('users_id' => $user_id), 20, $page);
        foreach ($query->result_array() as $row) {
            $moon = $this->getMoon(date("Y-m-d", strtotime($row['date'])));
            $arr[$row['id']] = $row;
            $arr[$row['id']]['kp'] = $this->getGS(date("Y-m-d", strtotime($row['date'])));
            $arr[$row['id']]['illumination'] = $moon['illumination'];
            $arr[$row['id']]['age'] = $moon['age'];
        }
        return $arr;
    }
    
    public function getQSs($user_id, $page = 0) {
        $page  = $page + 0;
        $user_id = $user_id +0;
        $this->db->order_by('date', 'desc'); 
        $query = $this->db->get_where('return_visit', array('users_id' => $user_id), 20, $page);
        return $query->result_array();
    }
    
    public function countGetQSs($user_id) {
        $this->db->where('users_id', $user_id)->from('return_visit');
        return $this->db->count_all_results();
    }
    
    public function getQS($qs_id) {
        $query = $this->db->get_where('return_visit', array('id' => $qs_id));
        return $query->row_array();
    }
    
    public function setQSRead($qs_id) {
        $this->db->where('id', $qs_id);
        $this->db->update('return_visit', array('is_read'=>1)); 
    }
    
    public function getBooking($user_id) {
        $user_id = $user_id +0;
        $arr   = array();
        $query = $this->db->query("SELECT br.*, u.*, bs.*, bc.calendar_title FROM `booking_reservation` AS `br`
            LEFT JOIN `users` AS `u` ON br.users_id=u.id
            LEFT JOIN `booking_calendars` AS `bc`ON br.calendar_id=bc.calendar_id
            LEFT JOIN `booking_slots` AS `bs` ON br.slot_id=bs.slot_id
            WHERE slot_date >= NOW() AND `users_id` =".$this->db->escape($user_id));
        return $query->result_array();
    }
    
    public function countGetAlarms($user_id) {
        $this->db->where('users_id', $user_id)->from('report');
        return $this->db->count_all_results();
    }
    
    private function getGS($date) {
        $query = $this->db->get_where('gs', array('date' => $date));
        if($query->num_rows() > 0) {
            return $query->row()->kp;
        } else {
            return 0;
        }
    }
    
    private function getMoon($date) {
        $query = $this->db->get_where('moon', array('date' => $date));
        if($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return 0;
        }
    }
}