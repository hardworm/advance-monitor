<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function setGS($kp) {
        $data = array(
            'date' => date("Y-m-d", time()),
            'kp'   => $kp,
        );
        if($this->db->insert('gs', $data)){
            return TRUE;
        } else {
            return;
        }
    }
    
    public function setMoon($arr) {
        if(is_array($arr) AND !empty($arr[0]['illumination']) AND !empty($arr[0]['age'])) {
            $data['date'] = date("Y-m-d",strtotime($arr[0]['iso_date']));
            $data['illumination'] = round((float)$arr[0]['illumination'] * 100);
            $data['age'] = floor((float)$arr[0]['age']);
            if($this->db->insert('moon', $data)){
                return TRUE;
            } else {
                return;
            }
        } else {
            return;
        }
    }
}