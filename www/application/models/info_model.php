<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->_table = "info";
    }
    
    public function getInfo() {
        $arr = array();
        $this->db->select('id, title, short_desc');
        $this->db->where('type_info_id', 1);
        $this->db->where('visible', 1);
        $this->db->order_by("date", "desc");
        $query = $this->db->get('info');

        return $query->result_array();
    }
    
    public function getNotice() {
        $this->db->select('id, title, short_desc');
        
        $this->db->where('is_notice', 1);
        $this->db->where('type_info_id', 1);
        $this->db->where('visible', 1);
        $this->db->order_by("date", "desc");
        $this->db->limit(1);
        $query = $this->db->get('info');
        
        return $query->row_array(); 
    }
    
    public function getMetods() {
        $arr = array();
        $this->db->select('id, title, short_desc');
        $this->db->where('type_info_id', 2);
        $this->db->where('visible', 1);
        $this->db->order_by("date", "desc");
        $query = $this->db->get('info');
        
        return $query->result_array();
    }
}