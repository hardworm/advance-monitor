<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Alarm_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    /**
     * Запись значений выбора пользователя в AlarmForm
     * @param int $report_id
     * @param array $data массив json c формы
     * @return boolean
     */
    public function recordAlarm($report_id, $data) {
        foreach ($data as $key => $value) {
//            file_put_contents('/tmp/json.txt', "$value\n", FILE_APPEND);
            $category = 0;
            preg_match('/[a-z_]+/u', $key, $category); //избавляемся от цифр в названии категории
            $arr  = explode('|', $value);

            if($arr[1] != 'undefined' AND is_array(explode(',', $arr[1]))) { //Обрабатка если данные прили из multiselect
//                file_put_contents('/tmp/json.txt', print_r($arr,true)."\nПервый блок\n", FILE_APPEND);
                foreach (explode(',', $arr[1]) as $val) {
                    if($val == 'undefined'){ //пропускаем если вдруг прилетела не то
                        continue;
                    }
                    $dataAlarm = array(
                        'report_id' => $report_id,
                        'category'  => $category[0],
                        'category2' => $arr[0] != 'undefined' ? $arr[0] : null,
                        'category3' => $val != 'undefined' ? $val : null,
                        'category4' => $arr[2] != 'undefined' ? $arr[2] : null,
                        'comment'   => $arr[3] != 'undefined' ? $arr[3] : null,
                    );
//                    file_put_contents('/tmp/alarm.txt', print_r($dataAlarm, true), FILE_APPEND); 
                    $this->db->insert('alarm', $dataAlarm);
                }
            } else { //Все остальные случаи
//                file_put_contents('/tmp/json.txt', "Второй блок\n", FILE_APPEND);
                $dataAlarm = array(
                    'report_id' => $report_id,
                    'category'  => $category[0],
                    'category2' => $arr[0] != 'undefined' ? $arr[0] : null,
                    'category3' => $arr[1] != 'undefined' ? $arr[1] : null,
                    'category4' => $arr[2] != 'undefined' ? $arr[2] : null,
                    'comment'   => $arr[3] != 'undefined' ? $arr[3] : null,
                );
//                file_put_contents('/tmp/alarm.txt', print_r($dataAlarm, true), FILE_APPEND); 
                $this->db->insert('alarm', $dataAlarm);
            }
        }
        return true;
    }
    
    /**
     * Функция для подсчета баллов и их записи
     * @param int $report_id id текущего отчета
     * @param array $data собранный данные с формы
     * @param array $config массив с баллами
     */
    public function calculateAndRecord($report_id, $data, $config) {
        $score['total_score'] = 0;
        
        foreach ($data as $key => $value) {
            $category = 0;
            $keyValue = array();
            
            preg_match('/[a-z_]+/u', $key, $category); //избавляемся от цифр в названии категории
            $arr      = explode('|', $value);

            if($arr[1] != 'undefined' AND is_array(explode(',', $arr[1]))){//Обрабатка если данные прили из multiselect
                foreach (explode(',', $arr[1]) as $val) {
                    if($val == 'undefined'){ //пропускаем если вдруг прилетела не то
                        continue;
                    }
                    $str = '$config->config';
                    if($category[0] != 'undefined' AND preg_match('/[a-z0-9_]{3,}/u', $category[0])){
                        $str .= "['$category[0]']";
                    }
                    if($arr[0] != 'undefined' AND preg_match('/[a-z0-9_]{3,}/u', $arr[0])){
                        $str .= "['$arr[0]']";
                    }
                    if($val != 'undefined' AND preg_match('/[a-z0-9_]{3,}/u', $val)){
                        $str .= "['$val']";
                    }
                    $str = '$keyValue = '.$str.";";
                    eval($str);

                    if(is_numeric($keyValue[0])){
                        $score['total_score'] = $score['total_score'] + $keyValue[0];
                        $score[$keyValue[1]] = @$score[$keyValue[1]] + $keyValue[0];
                    }
//                    file_put_contents('/tmp/arr.txt', print_r($keyValue, true), FILE_APPEND);
                }
            } else {
                $str = '$config->config';
                if($category[0] != 'undefined' AND preg_match('/[a-z0-9_]{3,}/u', $category[0])){
                    $str .= "['$category[0]']";
                }
                if($arr[0] != 'undefined' AND preg_match('/[a-z0-9_]{3,}/u', $arr[0])){
                    $str .= "['$arr[0]']";
                }
                if($arr[1] != 'undefined' AND preg_match('/[a-z0-9_]{3,}/u', $arr[1])){
                    $str .= "['$arr[1]']";
                }
                $str = '$keyValue = '.$str.";";
                eval($str);

                if(is_numeric($keyValue[0])){
                    $score['total_score'] = $score['total_score'] + $keyValue[0];
                    $score[$keyValue[1]] = @$score[$keyValue[1]] + $keyValue[0];
                }
//                file_put_contents('/tmp/arr.txt', print_r($keyValue, true), FILE_APPEND);
            }
            
//            file_put_contents('/tmp/score.txt', print_r($score, true), FILE_APPEND); 
        }
        $this->db->where('id', $report_id);
        $this->db->update('report', $score);
    }
    
}