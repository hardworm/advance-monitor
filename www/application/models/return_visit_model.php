<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Return_visit_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->_table = "return_visit";
    }
    
     /**
     * Функция для подсчета баллов и их записи
     * @param int $report_id id текущего отчета
     * @param array $data собранный данные с формы
     * @param array $config массив с баллами
     */
    public function calculateAndRecordFistVisit($report_id, $data, $config) {
        $score['total_score'] = 0;
        
        
//        file_put_contents('/tmp/report.txt', print_r($data, true), FILE_APPEND);
        foreach ($data as $key => $value) {
            $category = 0;
            $keyValue = array();
            
            
            preg_match('/[a-z_]+/u', $key, $category); //избавляемся от цифр в названии категории
            $arr      = explode('|', $value);

            if (is_array($arr)) {
                $str = '$config';
                if($category[0] != 'undefined' AND preg_match('/[a-z0-9_]{3,}/u', $category[0])){
                    $str .= "['$category[0]']";
                }
                if($arr[0] != 'undefined' AND preg_match('/[a-z0-9_]{2,}/u', $arr[0])){
                    $str .= "['$arr[0]']";
                }
                if($arr[1] != 'undefined' AND preg_match('/[a-z0-9_]{2,}/u', $arr[1])){
                    $str .= "['$arr[1]']";
                }
                $str = '$keyValue = '.$str.";";
//                file_put_contents('/tmp/debug.txt', "$str\n", FILE_APPEND);
                eval($str);

                if(is_numeric($keyValue[0])){
                    $score['total_score'] = $score['total_score'] + $keyValue[0];
                    $score[$keyValue[1]] = @$score[$keyValue[1]] + $keyValue[0];
                }
//                file_put_contents('/tmp/arr.txt', print_r($keyValue, true), FILE_APPEND);
            }
//            file_put_contents('/tmp/score.txt', print_r($score, true), FILE_APPEND); 
        }
        $this->db->where('id', $report_id);
        $this->db->update('return_visit', $score);
    }
     /**
     * Функция для подсчета баллов и их записи
     * @param int $report_id id текущего отчета
     * @param array $data собранный данные с формы
     * @param array $config массив с баллами
     */
    public function calculateAndRecordReturnVisit($report_id, $data, $config) {
        $score['total_score'] = 0;
        
        
//        file_put_contents('/tmp/report.txt', print_r($data, true), FILE_APPEND);
        foreach ($data as $key => $value) {
            $category = 0;
            $keyValue = array();
            $bal = 0;
            
            
            preg_match('/[a-z_]+/u', $key, $category); //избавляемся от цифр в названии категории
            $arr      = explode('|', $value);

            if (is_array($arr)) {
                $str = '$config';
                if($category[0] != 'undefined' AND preg_match('/[a-z0-9_]{3,}/u', $category[0])){
                    $str .= "['$category[0]']";
                }
                if($arr[0] != 'undefined' AND preg_match('/[a-z0-9_]{2,}/u', $arr[0])){
                    $str .= "['$arr[0]']";
                }
                if($arr[1] != 'undefined' AND preg_match('/[a-z0-9_]{2,}/u', $arr[1])){
                    $str .= "['$arr[1]']";
                }
                $bal = is_numeric($arr[2]) ? $arr[2]: $bal;
                $bal = is_numeric($arr[3]) ? $arr[3]: $bal;
                $str = '$keyValue = '.$str.";";
//                file_put_contents('/tmp/debug.txt', "$str\n", FILE_APPEND);
                eval($str);

                if(is_numeric($keyValue[0])){
                    $score['total_score'] = $score['total_score'] + $keyValue[0] + $bal;
                    $score[$keyValue[1]] = @$score[$keyValue[1]] + $keyValue[0] + $bal;
                }
//                file_put_contents('/tmp/arr.txt', print_r($keyValue, true), FILE_APPEND);
            }
//            file_put_contents('/tmp/score.txt', print_r($score, true), FILE_APPEND); 
        }
        $this->db->where('id', $report_id);
        $this->db->update('return_visit', $score);
    }
    
    public function getAllIndividualReport($user_id) {
        $arr = array();
        $query = $this->db->query("SELECT DATE_FORMAT(`date`, '%Y-%m-%d') AS `date`, `structural_defects`, `funtsionalnost_sphere`, "
            . "`cognitive`, `physiological_services`, `disease`, `total_score` "
            . '  FROM `return_visit`'
            . '  WHERE `users_id` = '.$this->db->escape($user_id) 
            . " ORDER BY `date` ASC " );
        
        foreach ($query->result() as $row) {
            $arr[] = $row;
        }
        return $arr;
    }
    
    public function getCountAllIndividualReport($user_id){
        $this->db->get_where('return_visit', array('users_id' => $user_id));
        return $this->db->count_all_results();
    }
    
    public function getLastReportUser($users_id) {
        $this->db->from('return_visit')->select('json_rv_vals')->where('users_id', $users_id)
            ->order_by('date','desc')->limit(1);
        $query = $this->db->get();
        $result = $query->row_array();
        $result = json_decode($result['json_rv_vals']);
        $arr = [];
        foreach ($result as $key => $value) {
            $arr[$key] = explode("|", $value);
        }
        return $arr;
    }
}