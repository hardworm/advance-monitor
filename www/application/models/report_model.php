<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->_table = "report";
    }
    
    /**
     * Индивидульный отчет за последнюю неделю
     * @param int $user_id id пользователя
     * @return array
     */
    public function getIndividualReport($user_id) {
        $arr = array();
        $query = $this->db->query("SELECT DATE_FORMAT(`date`, '%Y-%m-%d') AS `date`, `behavior`, `neurologic`, `vegetative`, `respiratory`, `alimentary`, `urinary` "
            . '  FROM `report`'
            . '  WHERE DATE(`date`) > NOW() - INTERVAL 7 DAY AND `users_id` = '.$this->db->escape($user_id) 
            . " ORDER BY `date` ASC " );
        
        foreach ($query->result() as $row) {
            $arr[] = array(
                $row->date,
                $row->behavior,
                $row->neurologic,
                $row->vegetative,
                $row->respiratory,
                $row->alimentary,
                $row->urinary,
            );
        }
        return $arr;
    }
    
    public function getWeaserIndividualReport($user_id) {
        $arr = array();
        $query = $this->db->query("SELECT DATE_FORMAT(`date`, '%Y-%m-%d') AS `date`, `weaser_score` "
            . '  FROM `report`'
            . '  WHERE DATE(`date`) > NOW() - INTERVAL 7 DAY AND `users_id` = '.$this->db->escape($user_id) 
            . " ORDER BY `date` ASC " );
        
        foreach ($query->result() as $row) {
            $arr[] = array(
                $row->date,
                $row->weaser_score,
            );
        }
        return $arr;
    }
    
    /**
     * Отчитывался ли сегодня пользователь
     * @param type $user_id
     * @return boolean true да, пользователь очитывался
     */
    public function isReportThisDay($user_id) {
        $query = $this->db->query('SELECT `id` FROM `report` WHERE DATE(`date`) = DATE(NOW()) AND `users_id` = '.$this->db->escape($user_id));
        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Функция для получения id 4 пользователей, у которых 
     * максимальное количество отчетов за последние 7 дней
     * @param int $user_id id пользователя который будет исключен
     * @return array массив id пользователей
     */
    private function get4User($user_id){
        $arr = array();
        $query = $this->db->query("SELECT `users_id` FROM `report` "
            . " WHERE DATE(`date`) > NOW() - INTERVAL 7 DAY AND `users_id` NOT IN ({$this->db->escape($user_id)}) "
            . " GROUP BY `users_id` "
            . " ORDER BY count('`id`') DESC "
            . " LIMIT 4 ");
        foreach ($query->result() as $row) {
            $arr[] = $row->users_id;
        }
        return $arr;
    }
    
    /**
     * Функция для получения суммарных баллов 4+1 пользователей 
     * за последние 7 дней у кого больше всего отчетов 
     * для построения общего графика
     * @param int $user_id id текущего пользователя
     * @return array массив массивов
     */
    public function getGeneralReport($user_id) {
        $arr = array();
        $arr2 = array();
        $fourUsers = $this->get4User($user_id);
        $users = implode(",", $fourUsers);
        //делаем массив  для позиционировая по user_id
        $key_users = array_flip($fourUsers);
        $key_users[$user_id] = 4; //текущий пользователь всегда последний
        
        $sql = "SELECT `users_id`, DATE_FORMAT(`date`, '%Y-%m-%d') AS `date`, `total_score` "
            . " FROM `report` "
            . " WHERE DATE(`date`) > NOW() - INTERVAL 7 DAY ";
        if(count($fourUsers) > 0) {
            $sql .= " AND `users_id` IN ($user_id,$users) ";
        } else {
            $sql .= " AND  `users_id` IN ($user_id) ";
        }
        $sql  .= " GROUP BY date, users_id ";
        $query = $this->db->query($sql);
        
        foreach ($query->result() as $row) {
            if(!isset($arr[$row->date])){
                $arr[$row->date] = array("$row->date",0,0,0,0,0); //забиваем нулями для размерности массива
            }
            $arr[$row->date][($key_users[$row->users_id]+1)] = $row->total_score; //заполняем
        }
        //избавляемся от структуры $key=>value
        foreach ($arr as $value) {
            $arr2[] = $value;
        }
        return $arr2;
    }
    
    public function calculateWeaser($report_id, $data) {
        $score['weaser_score'] = 0;
        foreach ($data as $key => $value) {
            if(is_numeric($value) AND $key != 'temp' AND $data->temp >= 0) {
                $score['weaser_score'] = $score['total_score'] + $value;
            }
            if(is_numeric($value) AND $key != 'temp' AND $data->temp < 0) {
                $score['weaser_score'] = $score['total_score'] - $value;
            }
        }
        $this->db->where('id', $report_id);
        $this->db->update('report', $score);
    }
    
    /**
     * Функция просмотра конкретного alarm
     * @param int $id
     * @return array
     */
    public function getReport($id) {
        $id = $id +0;
        $this->db->order_by('date', 'desc'); 
        $query = $this->db->get_where('report', array('id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    
    public function getAllIndividualReport($user_id) {
        $arr = array();
        $query = $this->db->query("SELECT DATE_FORMAT(`date`, '%Y-%m-%d') AS `date`, `behavior`, `neurologic`, "
            . "`vegetative`, `respiratory`, `alimentary`, `urinary`, total_score, weaser_score "
            . '  FROM `report`'
            . '  WHERE `users_id` = '.$this->db->escape($user_id) 
            . " ORDER BY `date` ASC " );
        
        foreach ($query->result() as $row) {
            $arr[] = $row;
        }
        return $arr;
    }
}