<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Booking_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Функция получения настроек из БД
     * @return array $key=>value
     */
    public function getSetting() {
        $arr = array();
        $query = $this->db->get('booking_config');
        foreach ($query->result() as $row){
           $arr[$row->config_name] = $row->config_value;
        }
        return $arr;
    }
    
    public function getVisibleFields() {
        $arr       = $this->getSetting();
        $list      = $arr['visible_fields'];
        $arrFields = Array();
        $arrFields = explode(",", $list);
        return $arrFields;
    }
    
    public function getMandatoryFields() {
        $arr       = $this->getSetting();
        $list      = $arr['mandatory_fields'];
        $arrFields = Array();
        $arrFields = explode(",", $list);
        return $arrFields;
	}

    public function getDefaultCategoryID() {
        $arr = array();
        $this->db->select('category_id');
        $this->db->where('category_order', 0);
        $this->db->where('category_active', 1);
        $query = $this->db->get('booking_categories');
        if ($query->num_rows() > 0) {
           $row = $query->row();
           return $row->category_id;
        } else {
            return;
        }
    }
    
    public function getCategoryID($category_id) {
        $this->db->select('category_id');
        $this->db->where('category_id', $category_id);
        $query = $this->db->get('booking_categories');
        if ($query->num_rows() > 0) {
           $row = $query->row();
           return $row->category_id;
        } else {
            return;
        }
    }
    
    public function getDefaultCalendarId($category_id) {
        $this->db->select('calendar_id');
        $this->db->where('calendar_order', 0);
        $this->db->where('calendar_active', 1);
        $this->db->where('category_id', $category_id);
        $query = $this->db->get('booking_calendars');
        if ($query->num_rows() > 0) {
           $row = $query->row();
           return $row->calendar_id;
        } else {
            return;
        }
    }
    
    public function getCalendar($rec=null, $calendar_id) {
        $query = $this->db->get_where('booking_calendars', array('calendar_id' => $calendar_id));
        if ($query->num_rows() > 0) {
           return $query->row();
        } else {
            return;
        }
    }
    
    public function getCategoriesList() {
		$arrayCategories = Array();
        $this->db->where('category_active', 1);
        $this->db->order_by('category_order', 'asc');
        $query = $this->db->get('booking_categories');
        
		foreach ($query->result() as $row) {
            $arrayCategories[$row->category_id] = array(
                'category_id'     => $row->category_id,
                'category_name'   => stripslashes($row->category_name),
                'category_active' => $row->category_active,
            );
        }
		return $arrayCategories;
	}
    
    public function getCalendarsList($category_id = 0) {
		$arrayCalendars = Array();
		if($category_id > 0) {
            $this->db->where('category_id', $category_id);
		}
        $this->db->where('calendar_active', 1);
        $this->db->order_by('calendar_order', 'asc');
        $query = $this->db->get('booking_calendars');
		
        foreach ($query->result() as $row) {
            $arrayCalendars[$row->calendar_id] = array(
                'calendar_title'  => stripslashes($row->calendar_title),
                'calendar_active' => $row->calendar_active,
                'calendar_order'  => $row->calendar_order,
            );
        }
		return $arrayCalendars;
	}
    
    public function getReservationForUser($user_id) {
        $query = $this->db->query("SELECT calendar_title, reservation_seats, reservation_confirmed, slot_date, slot_time_from, slot_time_to FROM `booking_reservation` AS `br`
        LEFT JOIN `booking_slots` AS `bs` ON br.slot_id = bs.slot_id
        LEFT JOIN `booking_calendars` AS `bc` ON br.calendar_id = bc.calendar_id
        WHERE  slot_date >= NOW() AND `users_id`=" .$this->db->escape($user_id));
        foreach ($query->result_array() as $row) {
            $arr[] = $row;
        }
        if (empty($arr)){
            return;
        } else {
            return $arr;
        }
        
    }
    
    public function getReservationFieldType() {
        $arr = array();
        $query = $this->db->get('booking_fields_types');
        
        foreach ($query->result() as $row){
           $arr[$row->reservation_field_name] = $row->reservation_field_type;
        }
		return $arr;
	}
    
    /**
     * Функция возращает массив переводов 
     * @return array key => value
     */
    public function getLabels() {
        $arr = array();
        $query = $this->db->get('booking_texts');
        
        foreach ($query->result() as $row){
           $arr[$row->text_label] = stripslashes($row->text_value);
        }
		return $arr;
	}
    
    /**
     * Функция для получения (возможно начала показа этого месяца)
     * @param int $calendar_id
     * @return string строка вида 2015,1,26
     */
    public function getFirstFilledMonth($calendar_id) {
		$returnvalue = date("Y,m,d");
        $arrDate     = explode(",", $returnvalue);
        $month       = (intval($arrDate[1]) - 1);
        $returnvalue = $arrDate[0] . "," . $month . "," . $arrDate[2];

        $this->db->where('slot_date >= NOW()');
        $this->db->where('calendar_id', $calendar_id);
        $this->db->where('slot_active', 1);
        $this->db->order_by('slot_date', 'asc');
        $this->db->limit(1);
        $query = $this->db->get('booking_slots');
		
		if($calendar_id!= 0 && $calendar_id != '' && $query->num_rows() > 0) {
			$row = $query->row();
			$arrDate = explode("-", $row->slot_date);
			$month = (intval($arrDate[1])-1);
			$returnvalue = $arrDate[0].",".$month.",".$arrDate[2];
		}
		return $returnvalue;
	}
    
    public function getMonthCalendar($month, $year, $weekday_format = "N") {
        $arrayMonth = Array();
        $date = mktime(0, 0, 0, $month, 1, $year);
        for ($n = 1; $n <= date('t', $date); $n++) {
            $arrayMonth[$n]              = Array();
            $arrayMonth[$n]["dayofweek"] = date($weekday_format, mktime(0, 0, 0, $month, $n, $year));
            $arrayMonth[$n]["daynum"]    = date('d', mktime(0, 0, 0, $month, $n, $year));
            $arrayMonth[$n]["monthnum"]  = date('m', mktime(0, 0, 0, $month, $n, $year));
            $arrayMonth[$n]["yearnum"]   = date('Y', mktime(0, 0, 0, $month, $n, $year));
            $arrayMonth[$n]["Ymd"] = $arrayMonth[$n]["yearnum"].$arrayMonth[$n]["monthnum"].$arrayMonth[$n]["daynum"];
        }
        return $arrayMonth;
    }
    
    public function getSlotsPerDay($year, $month, $arrayMonth, $calendar_id, $settingObj) {
//        if (strlen($month) == 1) {
//            $month = "0" . $month;
//        }
        $tot = array();
        
        foreach($arrayMonth as $daynum => $daydata) {
//            if (strlen($daynum) == 1) {
//                $daynum = "0" . $daynum;
//            }
            if (($year . "-" . $month . "-" . $daynum) == date('Y-m-d')) {
                $slotsQry = $this->db->query("SELECT SUM(s.slot_av) AS av_seats,s.* FROM booking_slots s "
                    . "WHERE s.slot_active=1 AND s.slot_date = '" . $year . "-" . $month . "-" . $daynum . "' "
                    . " AND REPLACE(s.slot_time_from,':','') >= DATE_FORMAT(NOW(),'%H%i%s') "
                    . " AND s.calendar_id=" . $this->db->escape($calendar_id) . " GROUP BY s.slot_id");
            } else {
                $slotsQry = $this->db->query("SELECT SUM(s.slot_av) AS av_seats,s.* FROM booking_slots s "
                    . " WHERE s.slot_active=1 AND s.slot_date = '" . $year . "-" . $month . "-" . $daynum . "' "
                    . " AND s.calendar_id=" . $this->db->escape($calendar_id) . " GROUP BY s.slot_id");
            }

            $tot[$daynum] = $slotsQry->num_rows();
            if ($tot[$daynum] == 0) {
                $tot[$daynum] = -1;
            } else {
                if ($settingObj['slots_unlimited'] != 1 && $settingObj['show_slots_seats'] == 0) {
                    foreach ($slotsQry->result_array() AS $slotRow) {
                        $reservationQry = $this->db->query("SELECT SUM(reservation_seats) as res FROM booking_reservation "
                            . " WHERE slot_id=" . $this->db->escape($slotRow["slot_id"]) 
                            . " AND reservation_cancelled = 0 GROUP BY slot_id");
                        if (($reservationQry->num_rows() > 0 && $reservationQry->row()->res == $slotRow["slot_av"]) || ($reservationQry->num_rows() > 0 && $settingObj['slots_unlimited'] == 0)) {
                            $tot[$daynum] = $tot[$daynum]-1;
                        }
                    }
                } else if ($setting['slots_unlimited'] == 2 && $setting['show_slots_seats'] == 1) {
                    $tot[$daynum]     = 0;
                    foreach ($slotsQry->result_array() AS $rowSlot) {
                        if ($rowSlot["av_seats"] == 0) {
                            $tot[$daynum]++;
                        } else {
                            $tot[$daynum] = $tot[$daynum] + $rowSlot["av_seats"];
                        }
                        $reservationQry = $this->db->query("SELECT SUM(reservation_seats) as res FROM booking_reservation WHERE slot_id=" . $this->db->escape($rowSlot["slot_id"]) . " AND reservation_cancelled = 0 GROUP BY slot_id");
                        if ($reservationQry->num_rows() > 0) {
                            $tot[$daynum] = $tot[$daynum] - $reservationQry->row()->res;
                        }
                    }
                }
            }
        }
        return $tot;
    }
    
    public function getSlotsPerDayList($year,$month,$day,$calendar_id,$setting) {
		
		$arraySlots=Array();
		if(strlen($month) == 1) {
			$month="0".$month;
		}
		if(strlen($day) == 1) {
			$day="0".$day;
		}
        $date = $year."-".$month."-".$day;
        
		if($date == date('Y-m-d')) {
			$slotsQry = $this->db->query("SELECT * FROM booking_slots WHERE slot_active=1 AND slot_date = ".$this->db->escape($date)." AND REPLACE(slot_time_from,':','') >= DATE_FORMAT(NOW(),'%H%i%s') AND calendar_id=".$this->db->escape($calendar_id)." ORDER BY slot_time_from");
		} else {
			$slotsQry = $this->db->query("SELECT * FROM booking_slots WHERE slot_active = 1 AND slot_date = ".$this->db->escape($date)." AND calendar_id= ".$this->db->escape($calendar_id)." ORDER BY slot_time_from");
		}
		
		foreach ($slotsQry->result_array() AS $slotRow) {
            $arraySlots[$slotRow["slot_id"]] = array(
                "slot_time_from"    => $slotRow["slot_time_from"],
                "slot_time_to"      => $slotRow["slot_time_to"],
                "slot_special_text" => stripslashes($slotRow["slot_special_text"]),
                "slot_special_mode" => $slotRow["slot_special_mode"],
                "slot_price"        => $slotRow["slot_price"],
                "slot_av"           => $slotRow["slot_av"],
            );

            if($setting['slots_unlimited'] == 0 && $setting['show_booked_slots'] == 0) {
				$reservationQry = $this->db->query("SELECT * FROM booking_reservation WHERE slot_id=".$this->db->escape($slotRow["slot_id"])." AND reservation_cancelled = 0");
				if($reservationQry->num_rows() == 0) {
					$arraySlots[$slotRow["slot_id"]]["booked"] = 0;
				}
			} else if($setting['slots_unlimited'] == 1) {
				$arraySlots[$slotRow["slot_id"]]["booked"] = 0;
			} else if($setting['slots_unlimited'] == 0 && $setting['show_booked_slots'] == 1) {
				$reservationQry = $this->db->query("SELECT * FROM booking_reservation WHERE slot_id=".$this->db->escape($slotRow["slot_id"])." AND reservation_cancelled = 0");
				if ($reservationQry->num_rows() > 0) {
                    $booked = 1;
                } else {
                    $booked = 0;
                }
				$arraySlots[$slotRow["slot_id"]]["booked"] = $booked;
			} else if($setting['slots_unlimited'] == 2) {
				$booked = 0;
				$reservationQry = $this->db->query("SELECT SUM(reservation_seats) as seats FROM booking_reservation WHERE slot_id=".$this->db->escape($slotRow["slot_id"])." AND reservation_cancelled = 0 GROUP BY slot_id");
				
				if($setting['show_booked_slots'] == 1 && $reservationQry->num_rows() > 0 && $reservationQry->row()->seats == $slotRow["slot_av"]) {
                    $booked=1;
                    $slot_av = 0;
                    $arraySlots[$slotRow["slot_id"]]["slot_av"] = $slot_av;
                    $arraySlots[$slotRow["slot_id"]]["slot_av_max"] = $slot_av;
                    $arraySlots[$slotRow["slot_id"]]["booked"] = $booked;
				} else {
					$booked=0;
					if($reservationQry->num_rows() > 0 && $reservationQry->row()->seats == $slotRow["slot_av"]) {
					} else if($reservationQry->num_rows() > 0) {
						$slot_av = $slotRow["slot_av"]-$reservationQry->row()->seats;
						$slot_av_max=$slotRow["slot_av_max"];
						if($slot_av_max>$slot_av) {
							$slot_av_max = $slot_av;	
						}
						$arraySlots[$slotRow["slot_id"]]["slot_av"] = $slot_av;
						$arraySlots[$slotRow["slot_id"]]["slot_av_max"] = $slot_av_max;
						$arraySlots[$slotRow["slot_id"]]["booked"] = $booked;
					} else {							
						$slot_av = $slotRow["slot_av"];
						$slot_av_max = $slotRow["slot_av_max"];
						if($slot_av_max > $slot_av) {
							$slot_av_max = $slot_av;	
						}		
						$arraySlots[$slotRow["slot_id"]]["slot_av"] = $slot_av;
						$arraySlots[$slotRow["slot_id"]]["slot_av_max"] = $slot_av_max;
						$arraySlots[$slotRow["slot_id"]]["booked"] = $booked;					
					}
				}
			}
		}
		return $arraySlots;
	}
    
    public function checkFutureSlots($year,$month,$day,$calendar_id) {
        $date = $year."-".$month."-".$day;
		$slotsQry = $this->db->query("SELECT * FROM booking_slots WHERE slot_date = ".$this->db->escape($date)." AND slot_active = 1 AND calendar_id=".$this->db->escape($calendar_id));
		$totRighe = 0;
		if($slotsQry->num_rows() > 0) {
			foreach ($slotsQry->result_array() AS $slotRow) {
				$reservationQry=$this->db->query("SELECT * FROM booking_reservation WHERE slot_id=".$this->db->escape($slotRow["slot_id"])." AND reservation_cancelled = 0");
				if($reservationQry->num_rows() <= 0) {
					$totRighe++;
				}
			}
			if ($totRighe > 0) {
                return true;
            } else {
                return false;
            }
        } else {
			return false;
		}
	}
    
    public function checkPastSlots($year,$month,$day,$calendar_id) {
        $date = $year."-".$month."-".$day;
		$slotsQry = $this->db->query("SELECT * FROM booking_slots WHERE slot_date = ".$this->db->escape($date)." AND slot_active = 1 AND calendar_id=".$this->db->escape($calendar_id));
		$totRighe = 0;
		if($slotsQry->num_rows() > 0) {
			foreach ($slotsQry->result_array() AS $slotRow) {
				$reservationQry = $this->db->query("SELECT * FROM booking_reservation WHERE slot_id=".$this->db->escape($slotRow["slot_id"])." AND reservation_cancelled = 0");
				if(($slotRow["slot_date"] == date('Y-m-d') && str_replace(":","",$slotRow["slot_time_from"])<date('His')) || $reservationQry->num_rows() > 0) {
				} else {
					$totRighe++;
				}
			}
			if($totRighe>0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
    public function insertReservation($setting, $POST, $user_id) {
		$listReservations="";
		for($i=0;$i<count($POST["reservation_slot"]);$i++) {
			$seats=1;
			if(isset($POST["reservation_seats_".$POST["reservation_slot"][$i]])) {
				$seats=$POST["reservation_seats_".$POST["reservation_slot"][$i]];
			}
			if($setting['slots_unlimited'] != 1) {
				$rsSlot = $this->db->query("SELECT * FROM booking_slots WHERE slot_id=".$this->db->escape($POST["reservation_slot"][$i]));
				$rowSlot = $rsSlot->row_array();
				$avSeats = $rowSlot["slot_av"];
				$ok = 0;
				$rsRes = $this->db->query("SELECT * FROM booking_reservation WHERE slot_id = ".$this->db->escape($POST["reservation_slot"][$i])." AND reservation_cancelled = 0");
				if($rsRes->num_rows()==0) {
					$ok = 1;
				} else {
					$totSeats = 0;
					foreach ($rsRes->result_array() as $rowRes) {
						$totSeats += $rowRes["reservation_seats"];
					}
					if(($totSeats+$seats)<=$avSeats) {
						$ok = 1;
					}
				}
			} else {
				$ok = 1;
			}
			if($ok == 1) {
                $data = array(
                    array(
                        'slot_id'             => $POST["reservation_slot"][$i],
                        'reservation_name'    => $POST["reservation_name"],
                        'reservation_surname' => $POST["reservation_surname"],
                        'reservation_email'   => $POST["reservation_email"],
                        'reservation_phone'   => $POST["reservation_phone"],
                        'reservation_message' => $POST["reservation_message"],
                        'reservation_seats'   => $seats,
                        'reservation_field1'  => $POST["reservation_field1"],
                        'reservation_field2'  => $POST["reservation_field2"],
                        'reservation_field3'  => $POST["reservation_field3"],
                        'reservation_field4'  => $POST["reservation_field4"],
                        'calendar_id'         => $POST["calendar_id"],
                        'users_id'            => $user_id,
                    ),
                );
                $this->db->insert_batch('booking_reservation', $data); 
							
				if($listReservations == "") {
					$listReservations.="".md5($this->db->insert_id())."";
				} else {
					$listReservations.=",".md5($this->db->insert_id())."";				
				}
			}
		}
		return $listReservations;
	}
    
    public function confirmReservations($listIds) {
		$arrayReservations = explode(",",$listIds);
		$listReservations = "";
		for($i=0;$i<count($arrayReservations);$i++) {
			if($listReservations=="") {
				$listReservations.="'".$arrayReservations[$i]."'";
			} else {
				$listReservations.=",'".$arrayReservations[$i]."'";
			}
		}
		$this->db->query("UPDATE booking_reservation SET reservation_confirmed = 1 WHERE MD5(reservation_id) IN (".$listReservations.")");
	}
    
    public function getSlot($id) {
        $query = $this->db->get_where('booking_slots', array('slot_id' => $id));
		return $query->row();
	}
    
    public function getMail($email_id) {
        $query = $this->db->get_where('booking_emails', array('email_id' => $email_id));
		return $query->row();
	}
    
    public function showUserReportReturVisit($user_id) {
        $sql  = " SELECT `reservation_id` AS `count` ";
        $sql .= " FROM `booking_reservation` AS `br` ";
        $sql .= " LEFT JOIN `booking_slots` AS `bs` ON br.slot_id = bs.slot_id ";
        $sql .= " LEFT JOIN `booking_calendars` AS `bc` ON br.calendar_id = bc.calendar_id ";
        $sql .= " WHERE `users_id`=" . $this->db->escape($user_id);
        $sql .= " AND (SELECT count(*)  FROM `booking_reservation` AS `br` LEFT JOIN `booking_slots` AS `bs` ON br.slot_id = bs.slot_id WHERE `users_id`=".$this->db->escape($user_id)." AND slot_date > NOW())  > 0 ";
        $sql .= " AND (SELECT count(*)  FROM `booking_reservation` AS `br` LEFT JOIN `booking_slots` AS `bs` ON br.slot_id = bs.slot_id WHERE `users_id`=".$this->db->escape($user_id)." AND slot_date < NOW()) > 0 ";
        $sql .= " AND (SELECT count(*)  FROM `booking_reservation` AS `br` LEFT JOIN `booking_slots` AS `bs` ON br.slot_id = bs.slot_id WHERE `users_id`=".$this->db->escape($user_id)." AND slot_date <= NOW() AND slot_date > NOW() - INTERVAL 7 DAY) = 0 ";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0) {
            return TRUE;
        } else {
            return;
        }
    }
    
    public function showUserReportFistVisit($user_id) {
        $sql  = " SELECT `id` FROM `return_visit`  ";
        $sql .= " WHERE `users_id`=" . $this->db->escape($user_id);
        $query = $this->db->query($sql);
        if($query->num_rows() > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
